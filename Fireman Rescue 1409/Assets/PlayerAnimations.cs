﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : MonoBehaviour {

    private Animator myanimator;
    private bool isRunning;
    private float finalRotation;
    public bool cantRunCauseObj;
    public bool haveObj;
    public bool armsstate; //true es que estan los brazos levantados, false es que no.
                            // Use this for initialization

    void Awake()
    {
        myanimator = this.GetComponent<Animator>();
        finalRotation = 2f;
    }

    void Start() {
        haveObj = false;
        armsstate = false;
        cantRunCauseObj = false;
    }

    void Update() {
        if (((Input.GetKey(KeyCode.W)) && Input.GetKey(KeyCode.LeftShift)) && armsstate == false && !cantRunCauseObj)
        {
            if (!haveObj) StartCoroutine("armsUP");
            myanimator.SetBool("isRuning", true);
            armsstate = true;
        }
        else if ((Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.W)) && armsstate == true)
        {
            if (!haveObj) StartCoroutine("armsDown");

            myanimator.SetBool("isRuning", false);
            armsstate = false;
        }
    }

    IEnumerator armsDown()
    {
        for(float i = 0; i<finalRotation; i += 0.1f)
        {
            transform.Rotate(+1,0,0);
        }
        yield return new WaitForSeconds(2f);
    }

    IEnumerator armsUP()
    {
        for (float i = 0; i < finalRotation; i += 0.1f)
        {
            transform.Rotate(-1,0,0);
        }
        yield return new WaitForSeconds(2f);
    }
}
