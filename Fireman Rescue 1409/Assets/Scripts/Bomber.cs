﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// Class donde controla el bomber ayudante
/// </summary>
public class Bomber : Huma
{
	protected bool acompanyar;
	public bool girar;
	public bool girar2;
    public BoxCollider t;

    public bool volcarContenidor;

    Bomber()
	{
		nom = "Bomber";
		acompanyar = false;
	}

    private void Start()
    {
        base.Start();
        BoxCollider[] cols = GameObject.Find("Contenidor").GetComponents<BoxCollider>();
        t = cols[0];
        t.enabled = false;
        volcarContenidor = false;
    }

    public void setAcompaya(bool acompanyar)
	{
		this.acompanyar = acompanyar;
        if (acompanyar)
        {
            this.gameObject.GetComponent<FollowerBomber>().enabled = true;
        }
	}

	public bool getAcompanya()
	{
		return acompanyar;
	}

	public void Moures(){
        if(!volcarContenidor)
        {
            t.enabled = true;
            this.GetComponent<Transform>().localEulerAngles = new Vector3(0f, 180f, 0f);
            this.GetComponent<Animator>().SetBool("caminar", true);
        }
        
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.name == "P1") {
			this.GetComponent<Transform>().localEulerAngles =new Vector3(0f, 270f, 0f);
		}
		if (other.gameObject.name == "P2") {
			this.GetComponent<Transform>().localEulerAngles =new Vector3(0f, 0f, 0f);
			this.GetComponent<Animator>().SetBool("volcar", true);
			this.GetComponent<Animator>().SetBool("caminar", false);
            volcarContenidor = true;
		}
	}
     
}
