﻿using UnityEngine;

public class FollowerBomber : MonoBehaviour {
    [SerializeField] GameObject pointbomber;
    [SerializeField]GameObject bomber;
    float maxdistance = 1.4f;
    private UnityEngine.AI.NavMeshAgent ComponenteDeNavegacion;

    // Use this for initialization
    void Start () {
        ComponenteDeNavegacion = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        //pointbomber = GameObject.Find("FollowPoint");
        //bomber = GameObject.Find("Helper");
    }
	
	// Update is called once per frame
	void Update () {
        ComponenteDeNavegacion.SetDestination(pointbomber.GetComponent<Transform>().position);
        Vector3 distance = bomber.GetComponent<Transform>().position - this.GetComponent<Transform>().position;
        if(maxdistance < distance.magnitude)
        {
            GetComponent<Animator>().SetBool("caminar",true);
        }else {
            GetComponent<Animator>().SetBool("caminar", false);
            GetComponent<Animator>().SetBool("volcar", false); //////añadido
        }
    }
}
