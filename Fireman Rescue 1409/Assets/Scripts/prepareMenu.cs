﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class prepareMenu : MonoBehaviour {
    /// <summary>
    /// Script para inicializar los menus de seleccion de niveles
    /// Muesta en los canvas los niveles desbloqueadis Ej: "Incendis urbans (2/5)"
    /// </summary>
    /// 
	// Use this for initialization
	void Start() {

        string scene = SceneManager.GetActiveScene().name;

        if(scene!="MainMenu")
        {
            foreach (UserData.level l in UserData.unlockedLevels)
            {
                if (l.scene == "incendis_urbans")  
                {
                    //Debug.Log(l.name);
                    if(l.lvl==1)
                    {
                        Text scoreText = GameObject.Find(l.name).transform.FindChild("Menu1Score1").GetComponent<Text>();
                        scoreText.text = "(" + l.score.ToString() + " / 5)";
                    }
                    else if(l.lvl == 2)
                    {
                        Text scoreText = GameObject.Find(l.name).transform.FindChild("Menu1Score2").GetComponent<Text>();
                        scoreText.text = "(" + l.score.ToString() + " / 5)";
                    }
                    else if (l.lvl == 3)
                    {
                        Text scoreText = GameObject.Find(l.name).transform.FindChild("Menu1Score3").GetComponent<Text>();
                        scoreText.text = "(" + l.score.ToString() + " / 5)";
                    }

                }
            }
        }
        else  //Reset proceso de los niveles
        {
            int c_incendisurbans = 0;
            int c_autopista = 0;

            foreach (UserData.level l in UserData.unlockedLevels)
            {
                if (l.scene == "incendis_urbans") c_incendisurbans++;
                if (l.scene == "autopista") c_autopista++;             
            }
       
            GameObject.Find("accidents_urbans").transform.FindChild("PlayerValue").GetComponent<Text>().text = "(" + c_incendisurbans.ToString() + " / 3)";
            GameObject.Find("autopista").transform.FindChild("PlayerValue").GetComponent<Text>().text = "(" + c_autopista.ToString() + " / 3)";
        }
        
	}

	
}
