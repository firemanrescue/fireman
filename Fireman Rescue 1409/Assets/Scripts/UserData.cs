﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleJSON;

public class UserData {

    //variables privadas
    private static bool logged;
    private static string username;

    //variables publicas
    public struct level
    {
        public string name;
        public string scene;
        public double score;
        public int lvl;         //new
    };
    public static List<level> unlockedLevels = new List<level>();       //Lista de niveles desbloqueados
   
    //Funciones
    public static void insertLevel(string name, string scene, double score, int level)
    {
        level l;
        l.name = name;
        l.scene = scene;
        l.score = score;
        l.lvl = level;          //new
        unlockedLevels.Add(l);
    }

    public static void loadUser()
    {
        string json = "Null";
        while (json == "Null")
        {
            try
            {
                JSONNode jsn = new JSONNode();
                string loadUserURL = "https://viodgamescom.000webhostapp.com/loadUser.php?user=" + username;
                         
                WWW phpRequest = new WWW(loadUserURL);
                while (!phpRequest.isDone) { }

                json = phpRequest.text;
                string[] vjson = json.ToString().Split("/"[0]);
                
                if (json != "Null")
                {
                    for(int i=0;i<vjson.Length-1;i++)
                    {
                        jsn = JSONNode.Parse(vjson[i]);
                        string name = jsn["nom"];
                        string scene = jsn["scene"];
                        double score = jsn["score"].AsDouble;
                        int level = jsn["level"].AsInt;
                        insertLevel(name, scene, score, level);
                        Debug.Log(name + "-" + scene + "-" + score + "-" + level);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
        }
    }

    //Setter y Getters
    public static void setUsername(string u)
    {
        username = u;
    }
    public static string getUsername()
    {
        return username;
    }
    public static void setLogged(bool l)
    {
        logged = l;
    }
    public static bool isLogged()
    {
        return logged;
    }

}
