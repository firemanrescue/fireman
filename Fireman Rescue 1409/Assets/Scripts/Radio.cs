﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class Radio: MultiMenu
{
	public Dictionary<string, AudioClip> clips;
	public AudioSource audio;
	Radio()
	{	
		nom = "Radio";

	}
	void Awake()
    {
		base.Awake();
		audio = GetComponent<AudioSource>();
		clips = new Dictionary<string, AudioClip>();
		loadClips();
	}

    public override void createMenu()
    {
        if(nMenu  == 2)
        {
            GameManager.onMenu = true;
            clearMenu();
            cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
            cam.gameObject.GetComponent<FirstPersonController>().enabled = false;
            panelM.gameObject.SetActive(true);
            menucanvas.enabled = true;
            menu.GetComponent<GridLayoutGroup>().constraintCount = 1;          
            for(int i = 0; i < GameManager.helpers_ar.Length; i++)
            {
                createBomberOption(i);
            }
        }
        else
        {
            base.createMenu();
        }
    }

    private void createBomberOption(int n)
    {
        GameObject b = (GameObject)Instantiate(InterfaceManager.menubutton);
        b.GetComponentInChildren<Text>().text = "Binomi " + (n + 1);
        b.GetComponent<Button>().onClick.AddListener(delegate {
            comunicarambBomber(n);
        });
        b.transform.SetParent(menu.transform, false);
    }
	private void loadClips ()
	{
		AudioClip acordonar = Resources.Load("FX/ZONA ACORDONADA") as AudioClip;
		AudioClip apagatV = Resources.Load("FX/APAGAT VEHICLE") as AudioClip;
		AudioClip apagatC = Resources.Load("FX/APAGAT CONTENIDOR") as AudioClip;
		AudioClip bomber1demanant = Resources.Load("FX/BOMBER 1 DEMANANT") as AudioClip;
		AudioClip endavantcomandament = Resources.Load("FX/ENDAVANT PER COMANDAMENT") as AudioClip;
		AudioClip rebutcomandament = Resources.Load("FX/REBUT PER COMANDAMENT") as AudioClip;
        AudioClip comandamentDemanantB1 = Resources.Load("FX/COMANDAMENT DEMANANT B1") as AudioClip;
        AudioClip comandamentDemanantB2 = Resources.Load("FX/COMANDAMENT DEMANANT B2") as AudioClip;
        AudioClip comandamentDemanantB3 = Resources.Load("FX/COMANDAMENT DEMANANT B3") as AudioClip;
        AudioClip comandamentDemanantB4 = Resources.Load("FX/COMANDAMENT DEMANANT B4") as AudioClip;
        AudioClip endavantperb1 = Resources.Load("FX/ENDAVANT PER B1") as AudioClip;
        AudioClip endavantperb2 = Resources.Load("FX/ENDAVANT PER B2") as AudioClip;
        AudioClip endavantperb3 = Resources.Load("FX/ENDAVANT PER B3") as AudioClip;
        AudioClip endavantperb4 = Resources.Load("FX/ENDAVANT PER B4") as AudioClip;
        AudioClip trobatvictima = Resources.Load("FX/VICTIMA 1RA PLANTA") as AudioClip;
        AudioClip trobatvictima2 = Resources.Load("FX/VICTIMA 2NA PLANTA") as AudioClip;
        AudioClip trobatvictima3 = Resources.Load("FX/VICTIMA 3RA PLANTA") as AudioClip;
        AudioClip trobatvictimaEscales = Resources.Load("FX/VICTIMA ESCALES") as AudioClip;
        AudioClip victimaambulancia = Resources.Load("FX/VICTIMA AMBULANCIA") as AudioClip;

        clips.Add ("acordonar", acordonar);
		clips.Add ("apagatvehicle", apagatV);
		clips.Add ("apagatcontenidor", apagatC);
		clips.Add ("bomber1demanant", bomber1demanant);
		clips.Add ("endavantcomandament", endavantcomandament);
		clips.Add ("rebutcomandament", rebutcomandament);
        clips.Add("comandamentdemanantb1", comandamentDemanantB1);
        clips.Add("comandamentdemanantb2", comandamentDemanantB2);
        clips.Add("comandamentdemanantb3", comandamentDemanantB3);
        clips.Add("comandamentdemanantb4", comandamentDemanantB4);
        clips.Add("endavantb1", endavantperb1);
        clips.Add("endavantb2", endavantperb2);
        clips.Add("endavantb3", endavantperb3);
        clips.Add("endavantb4", endavantperb4);
        clips.Add("trobatvictima", trobatvictima);
        clips.Add("trobatvictima2", trobatvictima2);
        clips.Add("trobatvictima3", trobatvictima3);
        clips.Add("trobatvictimaEscales", trobatvictimaEscales);
        clips.Add("victimaambulancia", victimaambulancia);

    }
    void comunicarambBomber(int n)
    {
        string firemanNumber = GameManager.helpers_ar[n].id.ToString();
        StartCoroutine(comunicaBomber(firemanNumber));
        GameManager.helpers_ar[n].createMenu();
        GameManager.helpers_ar[n].radioComm = true;
    }
    IEnumerator comunicaBomber(string firemanNumber)
    {
        audio.PlayOneShot(clips["comandamentdemanantb"+ firemanNumber]);
        yield return new WaitForSeconds(4.0f);
        audio.PlayOneShot(clips["endavantb" + firemanNumber]);
    }
    
}
