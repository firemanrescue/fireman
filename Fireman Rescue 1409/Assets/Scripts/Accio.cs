﻿using UnityEngine;
using System.Collections;

public class Accio
{
    private string dependencia;
    protected string nom, requisit;
    protected int actionValue;
    public int[] dependenciaValues;
    protected int node;

    /// <summary>
    /// Constructors
    /// </summary>
    public Accio() { }
    public Accio(string nom, string dep, int actionV, int[] depeV, int n, string req)
    {
        this.nom = nom;
        this.dependencia = dep;
        this.actionValue = actionV;
        this.dependenciaValues = depeV;
        node = n;
        requisit = req;
    }
    public Accio(string nom, int actionV, int n, string req) {
        this.nom = nom;
        this.dependencia = null;
        this.actionValue = actionV;
        dependenciaValues = new int[0];
        node = n;
        requisit = req;
    }
    public Accio(string nom)
    {
        this.nom = nom;
    }
    //////FIN CONSTRUCTORS

    public string getDependencia() {
        return dependencia;
    }

    public string getRequisit(){
        return requisit;
    }

    public string getNom() {
        return nom;
    }

    public int getActionValue(){
        return actionValue;
    }

    public void setActionValue(int _actionV){
        this.actionValue = _actionV;
    }

    public int[] getDependenciaValue(){
        return dependenciaValues;
    }

    public void setDependenciaValue(int _dependenciaV){
        this.actionValue = _dependenciaV;
    }

    public virtual bool getSelected() { return false; }
    public virtual void setSelected(bool s) { }
    public virtual bool isAvailable(int menu_actual) { return false; }
    public int getObjectiveNode() { return node; }
    public virtual bool hasDependency(string depN) { return false; }

}

