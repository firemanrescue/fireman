﻿using UnityEngine;
using System.Collections;

public class Explosio : MonoBehaviour {

	public float tempsexplosio;	
	private bool explotado;
	private AudioClip explota;
	private GameObject foc1;
    private ParticleSystem ps;
    public GameObject explosion;

    // Use this for initialization
    void Start () {
		foc1 = GameObject.Find("focContenidor");            //FocBo
		tempsexplosio = 0;
		explotado = false;
		explota = Resources.Load("FX/explosion2") as AudioClip;
	}
	
	// Update is called once per frame
	void Update () {
		if (tempsexplosio < 110) {          
			tempsexplosio += Time.deltaTime;
		} else if (explotado == false && !GameObject.Find("focContenidor").GetComponent<Foc>().getApagat()) {   //Controlar si se ha apagado que no explote
				explotado = true;
                explosion.SetActive(true);
                StartCoroutine(animExplosion());
                AudioSource.PlayClipAtPoint(explota, Camera.main.transform.position,1.0f);          //Prueba de volumen
                ps = foc1.GetComponent<ParticleSystem>();
                var pm = ps.main;
                var em = ps.emission;
                pm.startLifetime = 1;
                em.rateOverTime = 30;
                pm.maxParticles = 100;
                //foc1.GetComponent<ParticleSystem>().startLifetime = 1;
				//foc1.GetComponent<ParticleSystem>().emissionRate = 50;
				//foc1.GetComponent<ParticleSystem>().maxParticles = 200;

			}
        if(tempsexplosio>112)
        {
            explosion.SetActive(false);
        }
		
	}

    IEnumerator animExplosion()
    {
        yield return new WaitForSeconds(2f);
        explosion.SetActive(true);
    }

}
