﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using SimpleJSON;
using System.Collections;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    public static GameObject activeObject;
    private static float max_segons;
    private static float segons;
    private GameObject cam;
    private static float min_time;
    public static Objectiu[] ar_objectius;                  //Array de objetivos que se van realizando
    public static string levelname;
    public static SoundManager sManager;
    public static bool rebutMal;                    //Variable para controlar si recibe daño el jugador
    public static GameObject foc;
    public static bool onMenu = false;
    public static string scenename;

    public static List<int> nodes_visitats;
    public static Node i_node;
    private static Node node0;
    public static int total_score;
    internal static int max_score;

    public static Helper[] helpers_ar;                      //Array de ayudantes
    public static bool portaOberta;                         //Para controlar que se abra la puerta una vez en los helpers.
    public static bool primerERA;

    //public static GameObject[] victims;                         //Array de victimas
    public static bool victimcatched;
    public static bool victim2catched;
    public static bool victim2placed;           //Si aparece en les escales o en las habitaciones{true=en habs, false=en escales}

    //Semafors
    private GameObject[] semaforos;
    private GameObject[] walls, pasoNpc;
    //private bool isLoop = true;
    //private initialColor init;

    public void Start()
    {
        semaforos = GameObject.FindGameObjectsWithTag("semafor");
        walls = GameObject.FindGameObjectsWithTag("wallCoches");
        pasoNpc = GameObject.FindGameObjectsWithTag("wallVianantes");
        NavMesh.pathfindingIterationsPerFrame = 500;

        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(this);
        initNodes();
        DontDestroyOnLoad(GameObject.Find("MurosCoches"));
        DontDestroyOnLoad(GameObject.Find("MurosVianantes"));

        nodes_visitats = new List<int>();
        i_node = node0;

        levelname = SceneManager.GetActiveScene().name;
        total_score = 0;
        switch (levelname)
        {
            case "accident1":
                max_score = 900;
                break;
            case "accident2":
                max_score = 1200;
                break;
            case "accident3":
                max_score = 1400;
                victim2catched = false;
                break;
        }

        initializeHelpers();
        portaOberta = false;
        rebutMal = false;
        primerERA = false;

        sManager = SoundManager.getInstance();
        sManager.getAllSounds();
        segons = max_segons;
        InvokeRepeating("decreaseTimeRemaing", 0.0f, 2.0f);

        getSceneName();
        cam = GameObject.Find("FPSController");
        victimcatched = false;


        for (int i = 0; i < semaforos.Length; ++i){
            if (semaforos[i].gameObject.GetComponent<Semafor>().estat == Semafor.Estats.vermell)
            {
                semaforos[i].gameObject.GetComponent<Semafor>().CanviarAVermell();
                semaforos[i].gameObject.GetComponent<Semafor>().estat = Semafor.Estats.verd;
            }
            else
            {
                semaforos[i].gameObject.GetComponent<Semafor>().CanviarAVerd();
                semaforos[i].gameObject.GetComponent<Semafor>().estat = Semafor.Estats.groc;
            }
        }    
    }

    public void Update()
    {
        foreach (GameObject s in semaforos)
            switch (s.GetComponent<Semafor>().estat)
            {
                case Semafor.Estats.verd:
                    if (s.GetComponent<Semafor>().getTemps() > s.GetComponent<Semafor>().tempsVerd)
                    {
                        s.GetComponent<Semafor>().estat = Semafor.Estats.groc;
                        s.GetComponent<Semafor>().CanviarAVerd();
                        s.GetComponent<Semafor>().setTemps(0);
                    }
                    break;
                case Semafor.Estats.vermell:
                    Debug.Log(s.GetComponent<Semafor>().getTemps());
                    if (s.GetComponent<Semafor>().getTemps() > s.GetComponent<Semafor>().tempsVermell)
                    {
                        s.GetComponent<Semafor>().estat = Semafor.Estats.verd;
                        s.GetComponent<Semafor>().CanviarAVermell();
                        s.GetComponent<Semafor>().setTemps(0);
                    }
                    break;
                case Semafor.Estats.groc:
                    if (s.GetComponent<Semafor>().getTemps() > s.GetComponent<Semafor>().tempsGroc)
                    {
                        s.GetComponent<Semafor>().estat = Semafor.Estats.vermell;
                        s.GetComponent<Semafor>().CanviarAGroc();
                        s.GetComponent<Semafor>().setTemps(0);
                    }
                    break;

                default:
                    break;
            }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");
        }

        if (GameManager.GetSeconds() <= 0 && SceneManager.GetActiveScene().name != "resum")
        {
            cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
            cam.gameObject.GetComponent<FirstPersonController>().enabled = false;
            CancelInvoke("decreaseTimeRemaining");
            SceneManager.LoadScene("resum");
        }

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            Destroy(gameObject);
        }

    }

    public static float GetSeconds()
    {
        return segons;
    }
    public static void setUnactiveObject()
    {
        activeObject = null;
    }

    public static void SetActiveObject(GameObject g)
    {
        activeObject = g;
    }

    public static bool isAnyObjectActive()
    {
        return activeObject != null;
    }

    public void decreaseTimeRemaing()
    {
        segons--;
    }

    public void starTimer()
    {
        InvokeRepeating("decreaseTimeRemaing", 0.0f, 1.0f);
    }

    public static void setSegons(float s)
    {
        max_segons = s;
    }

    public static string getTime()
    {
        float modul = segons % 60;
        string minutos = (Mathf.Floor(segons / 60)).ToString();
        string seconds = modul.ToString();
        if (modul < 10)
        {
            seconds = "0" + seconds;
        }
        return minutos + " : " + seconds;
    }

    public static float getMaxSeconds()
    {
        return max_segons;
    }

    public static void setminTime(float mt)
    {
        min_time = mt;
    }

    public static float getminTime()
    {
        return min_time;
    }

    
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="requirement_name"></param>
    public static void completeObjective(int id, string requirement_name)
    {
        int cost;
        Node n;
        i_node.lookForChild(id, out n, out cost);
        int i = n.getObjective();

        MultiObjective multinode_objective = (MultiObjective)ar_objectius[i];

        multinode_objective.completeRequirement(requirement_name);

        if (multinode_objective.getType() == "MultiNode")
        {
            i_node = n;
            nodes_visitats.Add(id);
            total_score += cost;
        }
        else
        {
            if (ar_objectius[i].isCompleted())
            {
                i_node = n;
                nodes_visitats.Add(id);
                float temps_completat = max_segons - segons;
                ar_objectius[i].setSegonsCompletat(temps_completat);
                if (cost < 100) ar_objectius[i].setRegular(true);
                if (temps_completat > ar_objectius[i].getMaxTime())
                {
                    total_score += cost / 2;
                    ar_objectius[i].setRegular(true);
                }
                else total_score += cost;
            }
        }

        Debug.Log("Node: " + id + " - Comment: " + n.getComment() + " - cost: " + cost + "TotalScore: " + total_score);


    }
    /// <summary>
    /// Funcion para comprobar si un Nodo ha completado su objetivo
    /// </summary>
    /// <param name="id"></param>
    public static void completeObjective(int id)
    {
        int cost;
        Node n;
        i_node.lookForChild(id, out n, out cost);                   //función para recoger los datos del nodo.
                                                                    //i_node.printChildren(); /////// Para saber que nodos hijos tiene el nodo num id
        int i = n.getObjective();

        //Debug.Log("Node: "+id+" - Comment: "+n.getComment() + " - Cost: " + cost);

        ar_objectius[i].completeObjective();                        //Asigna a la lista de Objetivos, esta completado

        float temps_completat = max_segons - segons;
        i_node = n;
        nodes_visitats.Add(id);
        ar_objectius[i].setSegonsCompletat(temps_completat);
        if (cost < 100) ar_objectius[i].setRegular(true);
        if (temps_completat > ar_objectius[i].getMaxTime())
        {
            total_score += cost / 2;
            ar_objectius[i].setRegular(true);
        }
        else total_score += cost;

        Debug.Log("Node: " + id + " - Comment: " + n.getComment() + " - Cost: " + cost + "TotalScore: " + total_score);

    }
    /// <summary>
    /// Function completa el objetivo dependiendo booleano es true= Se suman el cost total, si false= se divide /2 el cost
    /// y se pone como objetivo regular
    /// </summary>
    /// <param name="id"></param>
    /// <param name="correcte"></param>
    public static void completeObjective(int id, bool correcte)
    {
        int cost;
        Node n;
        i_node.lookForChild(id, out n, out cost);                   //función para recoger los datos del nodo.
        int i = n.getObjective();

        ar_objectius[i].completeObjective();                        //Asigna a la lista de Objetivos, esta completado

        float temps_completat = max_segons - segons;
        i_node = n;
        nodes_visitats.Add(id);
        ar_objectius[i].setSegonsCompletat(temps_completat);
        if (correcte)
        {
            total_score += cost;
        }
        else
        {
            total_score += cost / 2;
            ar_objectius[i].setRegular(true);
        }

        Debug.Log("Node: " + id + " - Comment: " + n.getComment() + " - Cost: " + cost + "TotalScore: " + total_score);

    }

    public void getSceneName()
    {
        switch (levelname)
        {
            case "accident1": scenename = "incendis_urbans"; break;
            case "accident2": scenename = "incendis_urbans"; break;
            case "accident3": scenename = "incendis_urbans"; break;
        }
    }

    ///////////////////////////// NODES //////////////////////////////
    /// <summary>
    /// Carga variable tipo JSON con datos del archivo, crea e inicializa los nodes y da valores.
    /// </summary>
    public void initNodes()
    {

        //Controlar que nivel cargar
        string scene = SceneManager.GetActiveScene().name;
        string jsn = "";
        switch (scene)
        {
            case "accident1":
                jsn = Resources.Load("JSON_NodeLVL1").ToString();
                break;
            case "accident2":
                jsn = Resources.Load("JSON_NodeLVL2").ToString();
                break;
            case "accident3":
                jsn = Resources.Load("JSON_NodeLVL3").ToString();
                break;
        }


        JSONNode json = JSONNode.Parse(jsn);
        /*try
        {
            WWW itemsData = new WWW("https://viodgamescom.000webhostapp.com/GetLevel.php");
            while (!itemsData.isDone) { }
            string jsn = itemsData.text;
            json = JSONNode.Parse(jsn);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            return;
        }*/
        int n_nodes = json.Count;
        Node[] nodes = createNodes(n_nodes);
        prepareNodes(nodes, json);

    }

    /// <summary>
    /// Crea una array de nodes vacia
    /// </summary>
    /// <param name="n">numero de nodes</param>
    /// <returns></returns>
    Node[] createNodes(int n)
    {
        Node[] nodes = new Node[n];
        for (int i = 0; i < n; ++i)
        {
            nodes[i] = new Node(i);
        }
        return nodes;
    }

    /// <summary>
    /// Rellena el array de nodes, con cada node del json
    /// </summary>
    /// <param name="nodes">Array de nodes</param>
    /// <param name="json">Objeto json con los datos</param>
    void prepareNodes(Node[] nodes, JSONNode json)
    {
        int length = nodes.Length;
        node0 = new Node(0);
        //preparar node0
        int[] fills = getFillsArray(0, json);                   //Recoge los nodes hijos de node0
        int[] values = getValuesArray(0, json);                 //Recoge el coste de cada node hijo de node0
        for (int i = 0; i < fills.Length; ++i)
        {
            node0.AddChild(nodes[fills[i]], values[i]);         //Añade los hijo y los costes de cada a node0
        }

        //iterar per cada node restant
        for (int i = 1; i < length; ++i)
        {
            int objectiu = json[i][0]["objective"].AsInt;
            nodes[i].setObjectiu(objectiu);

            fills = getFillsArray(i, json);
            values = getValuesArray(i, json);
            int[] requisits = getRequisitsArray(i, json);
            if (requisits.Length > 0)
            {
                foreach (int requisit in requisits)
                {
                    nodes[i].AddRequisit(nodes[requisit]);
                }
            }

            string comments = json[i][0]["comment"];
            nodes[i].setComment(comments);

            if (fills.Length > 0)
            {
                for (int x = 0; x < fills.Length; ++x)
                {
                    nodes[i].AddChild(nodes[fills[x]], values[x]);  //Añade los hijo y los costes de cada a node0
                }
            }

            int action = json[i][0]["action"].AsInt;
            nodes[i].setAction(action);

        }//Fin FOR
    }

    /// <summary>
    /// Rellena el array hijo del node n con el coste de cada node hijo
    /// </summary>
    /// <param name="n"></param>
    /// <param name="json"></param>
    /// <returns></returns>
    int[] getValuesArray(int n, JSONNode json)
    {
        int size = json[n][0]["values"].Count;
        int[] values = new int[size];
        for (int i = 0; i < size; ++i)
        {
            values[i] = json[n][0]["values"][i].AsInt;
        }
        return values;
    }

    int[] getRequisitsArray(int n, JSONNode json)
    {
        int size = json[n][0]["requirement"].Count;

        int[] requisits = new int[size];
        for (int i = 0; i < size; ++i)
        {
            requisits[i] = json[n][0]["requirement"][i].AsInt;
        }
        return requisits;
    }


    /// <summary>
    /// Rellena array de fills con los nodos que son hijo de "n"
    /// </summary>
    /// <param name="n">id del nodo</param>
    /// <param name="json"> objeto JSON con los datos</param>
    /// <returns></returns>
    int[] getFillsArray(int n, JSONNode json)
    {
        int size = json[n][0]["childs"].Count;
        int[] fills = new int[size];
        for (int i = 0; i < size; ++i)
        {
            fills[i] = json[n][0]["childs"][i].AsInt;
        }
        return fills;
    }



    /// <summary>
    /// 
    /// </summary>
    void initializeHelpers()
    {
        helpers_ar = UnityEngine.Object.FindObjectsOfType<Helper>();
        //Debug.Log("Array Bombers :"+ helpers_ar.Length);

    }
}

