﻿using UnityEngine;
using UnityEngine.UI;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class Personatge : MonoBehaviour
    {
        private float distancia;
        private AudioSource[] asrc;
        public bool mascara = false;
        private bool caminar;
        public Camera mycam;
        private float walkmovement;
        private float runmovement;
        public GameObject bombers;
        public GameObject menu;
        public GameObject agua;
        public GameObject manguera;
        public GameObject lanza;
        public GameObject alicates;
        public GameObject ERA;
        private Image CanvasEra;
        public GameObject contenidor;
        public GameObject cotxe;
        public GameObject brossa;
        public GameObject victim;
        private GameObject caixaPrimera;
        //public GameObject aigua;
        public GameObject arms;     
        private GUISkin guiSkin;
        //private bool cop;
        public LayerMask lm;
        public GameObject selectObj;
		public static bool comunicant = false;
        private float timeMenu;
        public bool switchingOff;
        private bool crouching=false;
        private float initialCamPosY;
        bool actTime;
        bool safe;
        public static GameObject medicine;
        public static GameObject axe;
		public static GameObject radio;
        

        void Awake()
        {
            victim = GameObject.Find("Victim");
            lanza = GameObject.Find("Lanza");
            walkmovement = this.GetComponent<FirstPersonController>().m_WalkSpeed;
            runmovement = this.GetComponent<FirstPersonController>().m_RunSpeed;
            asrc = GetComponents<AudioSource>();
            contenidor = GameObject.Find("Contenidor");
            ERA = GameObject.Find("ERA");
            manguera = GameObject.Find("Manguera");
            cotxe = GameObject.Find("Cotxe");
            brossa = GameObject.Find("Brossa");
            caixaPrimera = GameObject.FindGameObjectWithTag("caixa");
            menu = GameObject.Find("MenuObj");
            guiSkin = Resources.Load("GUISkin") as GUISkin;
            CanvasEra = GameObject.Find("EraImg").GetComponent<Image>();
            agua = GameObject.Find("Agua");
            selectObj = GameObject.Find("LeaveObject");
            selectObj.SetActive(false);
            //cop = false;
            safe = true;
            medicine = GameObject.Find("Botiquinmano");
            axe = GameObject.Find("Destralmano");
			radio = GameObject.Find ("Radio");
            alicates = GameObject.Find("Alicates");
            arms = GameObject.Find("brazos03");
            initialCamPosY = this.transform.position.y;

        }
    // Use this for initialization
    void Start()
        {
            lanza.SetActive(SingletonGameObjectsHand.manguera);
            medicine.SetActive(SingletonGameObjectsHand.medicine);
            axe.SetActive(SingletonGameObjectsHand.axe);
			radio.SetActive (SingletonGameObjectsHand.radio);
            alicates.SetActive(SingletonGameObjectsHand.alicates);

            // Distancia del xorro de la manguera
            agua.GetComponent<ParticleSystem>().startLifetime = 1.2f;

        }

        void FixedUpdate()
        {
            checkDangers();
        }

        // Update is called once per frame
        void Update()
        {
            InputsPlayer();  //Entradas de teclado
            
            if (lanza.activeSelf){walkmovement = 2.0f; runmovement = 2.0f; asrc[0].pitch = 0.7f; }
            else { walkmovement = 3.0f; runmovement = 6.0f; asrc[0].pitch = 1f; }
            

            distancia = Vector3.Distance(this.transform.position, bombers.transform.position);

            if (actTime)
            {
                timeMenu -= Time.deltaTime;
            }
            if (timeMenu <= 0f)
            {
                selectObj.gameObject.SetActive(false);
                actTime = false;
            }

            if (safe)
            {
                if (SingletonCharacter.health < 500)
                {
                    SingletonCharacter.health = SingletonCharacter.health + 2.5f;
                }
            }
            /* // crouch with smoothing
              if(crouching){
                  if(mainCamera.transform.localPosition.y > crouchingHeight){
                      if(mainCamera.transform.localPosition.y - (crouchHeight* Time.deltaTime*8) < crouchingHeight){
                          mainCamera.transform.localPosition.y = crouchingHeight;
                      } else {
                          mainCamera.transform.localPosition.y -= crouchHeight* Time.deltaTime*8;
                         }

                  } else {
                  if(mainCamera.transform.localPosition.y<standingHeight){
                      if(mainCamera.transform.localPosition.y + (crouchHeight* Time.deltaTime*8) > standingHeight){
                          mainCamera.transform.localPosition.y = standingHeight;
                      } else {
                          mainCamera.transform.localPosition.y += crouchHeight* Time.deltaTime*8;
                      }
                      }
              }*/

        }///FIN UPDATE

        void InputsPlayer()
    {
            //CAMINAR
            if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.A)) || (Input.GetKey(KeyCode.S)) || (Input.GetKey(KeyCode.D)))
            {
                if (caminar == true && !GameManager.onMenu)
                {
                    asrc[0].Play();
                    caminar = false;
                }
            }
            else
            {
                asrc[0].Stop();
                caminar = true;
            }
            //TIRAR OBJETOS
            if (Input.GetKeyDown(KeyCode.G))                
            {
                selectObj.gameObject.SetActive(true);
                actTime = true;
                timeMenu = 3f;
            }
           
            //Cuando clicamos boton derecho del mouse
            if (Input.GetMouseButtonDown(1))
            {

                if (SingletonGameObjectsHand.manguera == true)
                {
                    Utilities.SetEmissionRate(agua.GetComponent<ParticleSystem>(), 5000f);
                    //arms.GetComponent<Animator>().SetBool("haveManguera", true);
                    //arms.GetComponent<Animator>().SetBool("usingManguera", true);
                    //arms.GetComponent<Animator>().SetBool("haveObject", true);
                    arms.GetComponent<Animator>().Play("IdleObject");
                    //arms.GetComponent<Animator>().Play("mangueraenabled");  ///
                    agua.GetComponent<AudioSource>().Play();
                    switchingOff = true;
                }

                if (SingletonGameObjectsHand.radio == true)
                {
                    radio.GetComponent<Objecte>().createMenu();
                }

            }
            else if (Input.GetMouseButtonUp(1))
            {
                if (SingletonGameObjectsHand.manguera == true)
                {
                    Utilities.SetEmissionRate(agua.GetComponent<ParticleSystem>(), 0f);
                    //arms.GetComponent<Animator>().SetBool("usingManguera", false);
                    //arms.GetComponent<Animator>().SetBool("haveManguera", true);
                    //arms.GetComponent<Animator>().SetBool("haveObject", true);
                    arms.GetComponent<Animator>().Play("IdleObject");

                    agua.GetComponent<AudioSource>().Stop();
                    switchingOff = false;
                }
                /* else
                 {
                     //arms.GetComponent<Animator>().SetBool("haveObject", true);
                     //arms.GetComponent<Animator>().SetBool("haveManguera", false);
                     arms.GetComponent<Animator>().Play("IdleObject");
                 }*/
            }
            //
            //PONERSE ERA
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (!GameManager.primerERA && (GameManager.getMaxSeconds()- GameManager.GetSeconds()) <  45.0f)      //Controlar si se pone ERA en momento correcto
                {
                    GameManager.primerERA = true;
                }

                if (ERA.GetComponent<Objecte>().catched == true && mascara == false)
                {
                    mascara = true;
                    CanvasEra.enabled = true;
                    asrc[0].pitch = 0.7f;
                    GameManager.sManager.setVolumes(0.1f, "Breathing");
                    asrc[1].Play();
                }
                else
                {
                    CanvasEra.enabled = false;
                    mascara = false;
                    asrc[0].pitch = 1f;
                    GameManager.sManager.restartVolumes();
                    asrc[1].Stop();
                }
            }//FIN PONERSE ERA

            if (Input.GetKeyDown(KeyCode.C))
            {
                InterfaceManager.x = InterfaceManager.x - 1;
                lanza.SetActive(false);
                manguera.GetComponentInParent<Objecte>().catched = false;
            }
            //Controlar cuando te agachas
            /*if (Input.GetKey(KeyCode.LeftControl))
            {
                mycam.transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
                walkmovement = 2.0f;
                runmovement = 4.0f;
            } else if(Input.GetKeyUp(KeyCode.LeftControl))
            {
                mycam.transform.position = new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z);
                walkmovement = 4.0f;
                runmovement = 8.0f;
            }*/
            
            // Lógica de crouch
            if (Input.GetKey(KeyCode.LeftControl))
            {
                if (!crouching) crouch();
            }
            if (!Input.GetKey(KeyCode.LeftControl))
            {
                if (crouching)
                {
                    stopCrouching();
                }
            }
        }

        void crouch()
        {
            //mycam.transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
            mycam.transform.position = Vector3.Lerp(mycam.transform.position, new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z), Time.deltaTime * 25f);
            this.GetComponent<FirstPersonController>().m_WalkSpeed = 1.5f;
            this.GetComponent<FirstPersonController>().m_RunSpeed = this.GetComponent<FirstPersonController>().m_WalkSpeed;
            arms.GetComponent<PlayerAnimations>().cantRunCauseObj = true;
            crouching = true;
        }
        void stopCrouching()
        {
            mycam.transform.position = new Vector3(transform.position.x, transform.position.y + 0.7f, transform.position.z);
            this.GetComponent<FirstPersonController>().m_WalkSpeed = 3.0f;
            this.GetComponent<FirstPersonController>().m_RunSpeed = 6.0f;
            arms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
            crouching = false;

        }

        void OnGUI()
        {
            GUI.skin = guiSkin;
            if (distancia > 40)
            {
                GUI.Label(new Rect(Screen.width / 4, Screen.height / 4, Screen.width, 80), "Torna a la zona de l'accident");
            }
        }

        void checkDangers()
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, 2f, lm);
            if (cols.Length > 0)
            {
                for (int i = 0; i < cols.Length; i++)
                {
                    if (cols[i].tag == "fire" || cols[i].tag == "humo")
                    {
                        if (cols[i].tag == "fire")
                        {
                            float particles = Utilities.GetEmissionRate(cols[i].GetComponent<ParticleSystem>());
                            if (particles > 10)
                            {
                                if (SingletonCharacter.health > 0)
                                {
                                    safe = false;
                                    --SingletonCharacter.health;
                                    GameManager.rebutMal = true;
                                }
                            }
                        }
                        else
                        {
                            if (cols[i].tag == "humo")
                            {
                                if (!mascara)
                                {
                                    if (SingletonCharacter.health > 0)
                                    {
                                        safe = false;
                                        --SingletonCharacter.health;
                                        GameManager.rebutMal = true;
                                    }
                                }
                                else safe = true;
                            }
                        }
                    }
                }
            }
            else safe = true;
        }

        /// <summary>
        /// Funcion para controlar cuando choca con la victima
        /// </summary>
        /// <param name="other"></param>
        void OnCollisionEnter(Collision other)
        {

            if (other.gameObject.name == "Victim")
            {
                Physics.IgnoreCollision(other.transform.GetComponent<Collider>(), GetComponent<Collider>());
            }

        }
    }
}
