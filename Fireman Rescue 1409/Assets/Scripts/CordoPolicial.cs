﻿using UnityEngine;

public class CordoPolicial : MonoBehaviour {
	public bool activar;
	// Use this for initialization

	void Start () {
		activar = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (activar == true){
			this.gameObject.GetComponent<MeshRenderer> ().enabled = true;
			this.gameObject.GetComponent<BoxCollider> ().enabled = true;
            if(GameObject.Find("MovableRoadBlocker") != null) { 
                GameObject.Find("MovableRoadBlocker").GetComponent<BarricadeObjectiveBehaviour>().trigger = true;
            }
            activar = false;
		}
	}

}
