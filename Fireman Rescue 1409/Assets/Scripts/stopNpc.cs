﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class stopNpc : MonoBehaviour
{
    [HideInInspector]public float currentNavSpeed, currentAnimSpeed;
    public List<GameObject> npc;

    void Start() {
        //currentNavSpeed = this.gameObject.GetComponent<NavMeshAgent>().speed;
        //currentAnimSpeed = this.gameObject.GetComponent<Animator>().speed;
    }

    public void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "npc") {
            other.GetComponent<NavMeshAgent>().speed = 0;
            other.GetComponent<Animator>().SetBool("walk", false);
            other.GetComponent<Animator>().SetBool("run", false);
            Debug.Log("Is a GameObject " + other.gameObject.name);
            npc.Add(other.gameObject);
        }



        /*if (other.gameObject.tag == "ZebraVerde") {
            this.GetComponent<NavMeshAgent>().speed = 0;
            this.GetComponent<Animator>().SetBool("walk", false);
            this.GetComponent<Animator>().SetBool("run", false);
        }*/


    }

         
}
