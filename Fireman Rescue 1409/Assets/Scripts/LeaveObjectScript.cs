﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class LeaveObjectScript : MonoBehaviour {

	public GameObject button, button2;

	public GameObject mangueraObject;
	public GameObject axeObject;
	public GameObject medicineObject;
    public GameObject eraObject; ///
	public GameObject radioObject;
    public GameObject alicateObject;
    private GameObject playerArms;
    private Animator playerArmsanimtr;
    public GameObject manguera;
    //private bool giro = false;
	public GameObject fpsCharacter;
    private float normalwalkV = 3f;
    private float slowedwalkV = 1.5f;

	bool[] deixat;
	bool[] objectCatched;

	Vector3 posOManguera;
	Vector3 posOAxe;
	Vector3 posOMedicine;
    Vector3 posOEra;
	Vector3 posChar;
    Vector3 posAlicate;

    int[] objectsWindows;

	public Sprite axe, medicine, shower, era;

	void Awake(){
		manguera = GameObject.Find ("Lanza");
        playerArms = GameObject.Find("brazos03");
        playerArmsanimtr = GameObject.Find("brazos03").GetComponent<Animator>();
        
        mangueraObject = GameObject.Find("Manguera");
        axeObject = GameObject.Find("Destral");
        medicineObject = GameObject.Find("Botiquin");

        eraObject = GameObject.Find("ERA");

        radioObject = GameObject.Find("Radio");
        alicateObject = GameObject.Find("Alicates");
        fpsCharacter = GameObject.Find("FPSController");
    }

	// Use this for initialization
	void Start () {
        radioObject.SetActive (false);
        alicateObject.SetActive(false);

        deixat = new bool [3];
		objectCatched = new bool [3];

		objectsWindows = new int[2];

		objectsWindows [0] = -1;
		objectsWindows [1] = -1;

		for (int i = 0; i < 3; ++i) {
			deixat [i] = objectCatched [i] = false;
		}

		posOManguera = new Vector3 (mangueraObject.transform.position.x, mangueraObject.transform.position.y, mangueraObject.transform.position.z);
		posOAxe = new Vector3 (axeObject.transform.position.x, axeObject.transform.position.y, axeObject.transform.position.z);
		posOMedicine = new Vector3 (medicineObject.transform.position.x, medicineObject.transform.position.y, medicineObject.transform.position.z);
        posOEra = new Vector3(eraObject.transform.position.x, eraObject.transform.position.y, eraObject.transform.position.z);
        posChar = new Vector3 (0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.L)) {
			leaveObject ();
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			clickButton ();

		} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			clickButton2 ();
		} else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			if (!SingletonGameObjectsHand.radio)
				clickButton3 ();
		}else if (Input.GetKeyDown(KeyCode.Alpha4)){
            if (!SingletonGameObjectsHand.alicates)
               clickButton4();
        }

		button.transform.GetComponent<UnityEngine.UI.Image> ().sprite = null;
		button2.transform.GetComponent<UnityEngine.UI.Image> ().sprite = null;

        //Controla cuando la maguera es cogida del camión
		if (mangueraObject.GetComponent<Manguera>().catched) {
			mangueraObject.transform.position = posOManguera;
			objectCatched[0] = true;
			if (deixat[0]) {
				mangueraObject.transform.rotation = Quaternion.FromToRotation (new Vector3 (mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, mangueraObject.transform.rotation.z), new Vector3 (mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, -267.238f));
				deixat[0] = false;
			}

			objectsWindows [0] = 1;
			button.transform.GetComponent<UnityEngine.UI.Image> ().sprite = shower;
		}

		if (axeObject.GetComponent<Destral>().catched)
        {
			axeObject.transform.position = posOAxe;
			objectCatched[1] = true;
			if (deixat[1]) {
				axeObject.transform.rotation = Quaternion.FromToRotation (new Vector3 (axeObject.transform.rotation.x, axeObject.transform.rotation.y, axeObject.transform.rotation.z), new Vector3 (mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, -267.238f));
				deixat[1] = false;
			}

			if (objectsWindows [0] != 1) {
				objectsWindows [0] = 2;
				button.transform.GetComponent<UnityEngine.UI.Image> ().sprite = axe;
			} else {
				objectsWindows [1] = 2;
				button2.transform.GetComponent<UnityEngine.UI.Image> ().sprite = axe;
			}
		}

		if (medicineObject.GetComponent<Botiquin>().catched) {
			medicineObject.transform.position = posOMedicine;
			objectCatched[2] = true;
			if (deixat[2]) {
				medicineObject.transform.rotation = Quaternion.FromToRotation (new Vector3 (medicineObject.transform.rotation.x, medicineObject.transform.rotation.y, medicineObject.transform.rotation.z), new Vector3 (mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, -267.238f));
				deixat[2] = false;
			}

			if ((objectsWindows [0] != 1) && objectsWindows [0] != 2) {   
				objectsWindows [0] = 3;
				button.transform.GetComponent<UnityEngine.UI.Image> ().sprite = medicine;
			} else {
				objectsWindows [1] = 3;
				button2.transform.GetComponent<UnityEngine.UI.Image> ().sprite = medicine;
			}
		}
        
        //controlar objeto ERA cuando es cogido y soltado
        if (eraObject.GetComponent<ERA>().catched)
        {
            eraObject.transform.position = posOEra;
            /*objectCatched[3] = true;
            if (deixat[3])
            {
                eraObject.transform.rotation = Quaternion.FromToRotation(new Vector3(eraObject.transform.rotation.x, eraObject.transform.rotation.y, eraObject.transform.rotation.z), new Vector3(mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, -267.238f));
                deixat[3] = false;
            }

            if (((objectsWindows[0] != 1) && objectsWindows[0] != 2) && objectsWindows[0] != 3)
            {
                objectsWindows[0] = 4;
                button.transform.GetComponent<UnityEngine.UI.Image>().sprite = era;
            }
            else
            {
                objectsWindows[1] = 4;
                button2.transform.GetComponent<UnityEngine.UI.Image>().sprite = era;
            }*/
        }
        


    }

	public void clickButton() {
		switch(objectsWindows[0]) {
		case 1:
			SingletonGameObjectsHand.manguera = true;
            //playerArmsanimtr.SetBool("haveManguera", true);
            setVelocitydown();
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = true;
            SingletonGameObjectsHand.axe = false;
			SingletonGameObjectsHand.medicine = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;
		case 2:
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
            SingletonGameObjectsHand.axe = true;
            setVelocityOk();
			SingletonGameObjectsHand.manguera = false;
			SingletonGameObjectsHand.medicine = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;
		case 3:

			SingletonGameObjectsHand.medicine = true;
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
            SingletonGameObjectsHand.axe = false;
			SingletonGameObjectsHand.manguera = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;
		}
        setArmsrdy();
	}

	public void clickButton2() {
		switch(objectsWindows[1]) {
		case 1:
			SingletonGameObjectsHand.manguera = true;
            setVelocitydown();
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = true;
            SingletonGameObjectsHand.axe = false;
			SingletonGameObjectsHand.medicine = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;
		case 2:

			SingletonGameObjectsHand.axe = true;
            setVelocityOk();
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
            SingletonGameObjectsHand.manguera = false;
			SingletonGameObjectsHand.medicine = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;
		case 3:

			SingletonGameObjectsHand.medicine = true;
            setVelocityOk();
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
            SingletonGameObjectsHand.axe = false;
			SingletonGameObjectsHand.manguera = false;
			SingletonGameObjectsHand.radio = false;
            SingletonGameObjectsHand.alicates = false;
            break;

		}
        setArmsrdy();
    }
    /// <summary>
    /// Function donde mostrará solo la radio
    /// </summary>
    public void clickButton3() {
		SingletonGameObjectsHand.radio = true;
        setVelocityOk();
        SingletonGameObjectsHand.axe = false;
		SingletonGameObjectsHand.manguera = false;
		SingletonGameObjectsHand.medicine = false;
        SingletonGameObjectsHand.alicates = false;

        setArmsrdy();
    }
    /// <summary>
    /// Funcion para mostrar solo los alicates
    /// </summary>
    public void clickButton4()
    {
        SingletonGameObjectsHand.alicates = true;
        setVelocityOk();
        SingletonGameObjectsHand.axe = false;
        SingletonGameObjectsHand.manguera = false;
        SingletonGameObjectsHand.medicine = false;
        SingletonGameObjectsHand.radio = false;

        setArmsrdy();
    }

    void leaveObject() {
        Vector3 positionObj = this.transform.position + 2 * transform.forward;
        quitArms();
        setVelocityOk();
        if (playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj) playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
        if (SingletonGameObjectsHand.radio || SingletonGameObjectsHand.alicates) {
            if (SingletonGameObjectsHand.radio) SingletonGameObjectsHand.radio = false;
            if (SingletonGameObjectsHand.alicates) SingletonGameObjectsHand.alicates = false;
		}
		if (SingletonGameObjectsHand.axe)
        {
            axeObject.transform.position = positionObj;
            axeObject.GetComponent<Destral> ().catched = false;
			axeObject.transform.rotation = Quaternion.FromToRotation (new Vector3 (axeObject.transform.rotation.x, axeObject.transform.rotation.y, axeObject.transform.rotation.z), new Vector3 (axeObject.transform.rotation.x, axeObject.transform.rotation.y, 267.238f));
            axeObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
            axeObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
            if (objectsWindows [0] == 2) {
				deixat [0] = true;
				objectCatched [0] = false;
				objectsWindows [0] = 0;
			} else if (objectsWindows [1] == 2) {
				deixat [1] = true;
				objectCatched [1] = false;
				objectsWindows [1] = 0;
			}

			InterfaceManager.x--;
			SingletonGameObjectsHand.axe = false;

		} else if (SingletonGameObjectsHand.manguera) {
            mangueraObject.transform.position = positionObj;
			mangueraObject.GetComponent<Manguera> ().catched = false;
         
                mangueraObject.transform.rotation = Quaternion.FromToRotation(new Vector3(mangueraObject.transform.rotation.x, mangueraObject.transform.rotation.y, mangueraObject.transform.rotation.z), new Vector3(267.238f, mangueraObject.transform.rotation.y, 267.238f));
                mangueraObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
                mangueraObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;

            if (objectsWindows [0] == 1) {
				deixat [0] = true;
				objectCatched [0] = false;
				objectsWindows [0] = 0;
			} else if (objectsWindows [1] == 1) {
				deixat [1] = true;
				objectCatched [1] = false;
				objectsWindows [1] = 0;
			}

			InterfaceManager.x--;
			SingletonGameObjectsHand.manguera = false;

		} else if (SingletonGameObjectsHand.medicine) {
            medicineObject.transform.position = positionObj;
            medicineObject.GetComponent<Botiquin>().catched = false;
			medicineObject.transform.rotation = Quaternion.FromToRotation (new Vector3 (medicineObject.transform.rotation.x, medicineObject.transform.rotation.y, medicineObject.transform.rotation.z), new Vector3 (medicineObject.transform.rotation.x, medicineObject.transform.rotation.y, 267.238f));
            medicineObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
            medicineObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
            if (objectsWindows [0] == 3) {
				deixat [0] = true;
				objectCatched [0] = false;
				objectsWindows [0] = 0;
			} else if (objectsWindows [1] == 3) {
				deixat [1] = true;
				objectCatched [1] = false;
				objectsWindows [1] = 0;
			}

			InterfaceManager.x--;
			SingletonGameObjectsHand.medicine = false;

		}
	}

    void setArmsrdy()
    {
        if (!playerArms.GetComponent<PlayerAnimations>().haveObj && !playerArms.GetComponent<PlayerAnimations>().armsstate)
        {
            playerArmsanimtr.SetBool("haveObject", true);
            playerArms.GetComponent<PlayerAnimations>().StartCoroutine("armsUP");
            playerArms.GetComponent<PlayerAnimations>().haveObj = true;
        }
    }

    void quitArms()
    {
        if (playerArms.GetComponent<PlayerAnimations>().haveObj)
        {
            playerArms.GetComponent<PlayerAnimations>().haveObj = false;
            playerArms.GetComponent<PlayerAnimations>().StartCoroutine("armsDown");
            playerArmsanimtr.SetBool("haveObject", true);
        }
    }

    void setVelocitydown()
    {
        fpsCharacter.GetComponent<FirstPersonController>().m_WalkSpeed = slowedwalkV;
        fpsCharacter.GetComponent<FirstPersonController>().m_RunSpeed = slowedwalkV*2;
    }

    void setVelocityOk()
    {
        fpsCharacter.GetComponent<FirstPersonController>().m_WalkSpeed = normalwalkV;
        fpsCharacter.GetComponent<FirstPersonController>().m_RunSpeed = normalwalkV*2;
    }
}
