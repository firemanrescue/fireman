﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
public class Huma : MultiMenu
{

    public Huma()
    {
        nom = "";
        nMenu = 0;
    }

    new public void OnMouseEnter()          //Función de la classe Objecte
    {
        Limit.SetActive(true);
        cursorP.SetActive(false);
        cursorC.SetActive(true);

    }

    new public void OnMouseExit()           //Función de la classe Objecte
    {
        Limit.SetActive(false);
        cursorP.SetActive(true);
        cursorC.SetActive(false);
    }

}
