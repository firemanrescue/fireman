﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class cochePorMesh : MonoBehaviour {

    public bool arrive;
    private NavMeshAgent car;
    
    // Use this for initialization
    void Start () {
        car = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        if (arrive){
            car.destination = GetRandomPointOnMesh();
            arrive = false;
        }
	}

    Vector3 GetRandomPointOnMesh() {
        float maxWalkDistance = 50f;
        Vector3 direction = Random.insideUnitSphere * maxWalkDistance;
        Vector3 destination;
        direction.y = 0;
        direction += transform.position;
        NavMeshHit hit;
        int carsMask = 1 << NavMesh.GetAreaFromName("Coches");
        Debug.Log(carsMask);
        NavMesh.SamplePosition(direction, out hit, maxWalkDistance, carsMask);
        destination = hit.position;
        return destination;
    }
}
