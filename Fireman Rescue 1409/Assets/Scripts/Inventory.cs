﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Inventory : MonoBehaviour {
	public GameObject canv;

	private float timeMenu;

	bool actTime;

	// Use this for initialization
	void Start () {
		timeMenu = 2.5f;
		actTime = false;
	}

	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown (KeyCode.E)) && (!actTime)) {
			canv.gameObject.SetActive(true);
			actTime = true;
			timeMenu = 2.5f;
		}

		if (actTime) {
			timeMenu -= Time.deltaTime;
		}

		if (timeMenu <= 0f) {
			canv.gameObject.SetActive (false);
			actTime = false;
		}

        if (GameManager.GetSeconds() <= 0)
        {
            //Application.LoadLevel("resum");
            SceneManager.LoadScene("resum");
        }
	}
}

