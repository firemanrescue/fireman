﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HideMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Camera cameraToLookAt;
    public bool isOver = false;

	public void OnPointerEnter(PointerEventData eventData)
	{
		isOver = true;
	}

    public void OnPointerExit(PointerEventData eventData)
    {
        
        isOver = false;
        Button[] ls = this.GetComponentsInChildren<Button>();
        foreach (Button o in ls)
            Destroy(o.gameObject);
        GameManager.setUnactiveObject();
        this.GetComponent<Canvas>().enabled = false;
    }

    void Update()
    {
        Vector3 v = cameraToLookAt.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(cameraToLookAt.transform.position - v);
        transform.Rotate(0, 180, 0);
    }
}