﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stopCar : MonoBehaviour
{

    private Vector3 directionRay;
    public bool left;
    public bool right;
    public bool forward;
    public bool back;
    private bool npcCrossing;
    private float position;
    private GameObject coche;
    // Use this for initialization
    void Start(){
        position = 0.4f;
        npcCrossing = false;
    }
    // Update is called once per frame
    void Update(){
        // if (npcCrossing){
        //col.gameObject.GetComponent<Cotxe2>().stopCar = true;
        //}
        //shootRay();

        //raycast();
        //RaycastHit raycastHit;
        /*if (raycast()){
            Collider raycastHitCollider = raycastHit.collider;
            Debug.Log(raycastHitCollider.gameObject.tag);
        }*/
    }

    void shootRay() {
        if (left) {
            RaycastHit raycastHit;
            if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), Vector3.left, out raycastHit, 10)){
                Collider raycastHitCollider = raycastHit.collider;
                if(raycastHitCollider.gameObject.tag == "npc"){
                    npcCrossing = true;
                    coche.GetComponent<Cotxe2>().stopCar = true;
                }
                Debug.Log(raycastHitCollider.gameObject.tag);
            }
            Debug.DrawRay(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), Vector3.left, Color.red);
            for (int i = 0; i < 18; i++) {
                if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z + position), Vector3.left, out raycastHit, 10)){
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc"){
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z - position), Vector3.left, out raycastHit, 10)){
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc"){
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                Debug.DrawRay(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z + position), Vector3.left, Color.red);
                Debug.DrawRay(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z - position), Vector3.left, Color.red);
                position += 0.2f;
            }
            if (!npcCrossing){
                coche.GetComponent<Cotxe2>().stopCar = false;
                CancelInvoke();
            }
            else {
                npcCrossing = false;
            }
            position = 0;
        }

        else if (right) {
            Debug.DrawRay(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), Vector3.right, Color.red);
            RaycastHit raycastHit;
            if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), Vector3.right, out raycastHit, 10)) {
                Collider raycastHitCollider = raycastHit.collider;
                if (raycastHitCollider.gameObject.tag == "npc"){
                    npcCrossing = true;
                    coche.GetComponent<Cotxe2>().stopCar = true;
                }
                Debug.Log(raycastHitCollider.gameObject.tag);
            }
            for (int i = 0; i < 18; i++) {
                if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z + position), Vector3.right, out raycastHit, 10)){
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc"){
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z - position), Vector3.right, out raycastHit, 10)){
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc") { 
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                Debug.DrawRay(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z + position), Vector3.right, Color.red);
                Debug.DrawRay(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z - position), Vector3.right, Color.red);
                position += 0.2f;
            }
            if (!npcCrossing) {
                coche.GetComponent<Cotxe2>().stopCar = false;
                CancelInvoke();
            }
            else {
                npcCrossing = false;
            }
            position = 0;
        }

        else if (forward) {
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5f), Vector3.forward, Color.red);
            RaycastHit raycastHit;
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5f), Vector3.forward, out raycastHit, 10)){
                Collider raycastHitCollider = raycastHit.collider;
                if (raycastHitCollider.gameObject.tag == "npc"){
                    npcCrossing = true;
                    coche.GetComponent<Cotxe2>().stopCar = true;
                }
                Debug.Log(raycastHitCollider.gameObject.tag);
            }
            for (int i = 0; i < 18; i++){
                if (Physics.Raycast(new Vector3(transform.position.x + position, transform.position.y, transform.position.z + 0.5f), Vector3.forward, out raycastHit, 10)){
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc"){
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                if (Physics.Raycast(new Vector3(transform.position.x - position, transform.position.y, transform.position.z + 0.5f), Vector3.forward, out raycastHit, 10)) {
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc"){
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                Debug.DrawRay(new Vector3(transform.position.x + position, transform.position.y, transform.position.z + 0.5f), Vector3.forward, Color.red);
                Debug.DrawRay(new Vector3(transform.position.x - position, transform.position.y, transform.position.z + 0.5f), Vector3.forward, Color.red);
                position += 0.2f;
            }
            if (!npcCrossing){
                coche.GetComponent<Cotxe2>().stopCar = false;
                CancelInvoke();
            }
            else{
                npcCrossing = false;
            }
            position = 0;
        }

        else if (back) {
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f), Vector3.back, Color.red);
            RaycastHit raycastHit;
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f), Vector3.back, out raycastHit, 10)){
                Collider raycastHitCollider = raycastHit.collider;
                if (raycastHitCollider.gameObject.tag == "npc") {
                    npcCrossing = true;
                    coche.GetComponent<Cotxe2>().stopCar = true;
                }
                Debug.Log(raycastHitCollider.gameObject.tag);
            }
            for (int i = 0; i < 18; i++) {
                if (Physics.Raycast(new Vector3(transform.position.x + position, transform.position.y, transform.position.z - 0.5f), Vector3.back, out raycastHit, 10))
                {
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc") {
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                if (Physics.Raycast(new Vector3(transform.position.x - position, transform.position.y, transform.position.z - 0.5f), Vector3.back, out raycastHit, 10))
                {
                    Collider raycastHitCollider = raycastHit.collider;
                    if (raycastHitCollider.gameObject.tag == "npc") {
                        npcCrossing = true;
                        coche.GetComponent<Cotxe2>().stopCar = true;
                        break;
                    }
                    Debug.Log(raycastHitCollider.gameObject.tag);
                }
                Debug.DrawRay(new Vector3(transform.position.x + position, transform.position.y, transform.position.z - 0.5f), Vector3.back, Color.red);
                Debug.DrawRay(new Vector3(transform.position.x - position, transform.position.y, transform.position.z - 0.5f), Vector3.back, Color.red);
                position += 0.2f;
            }
            if (!npcCrossing){
                coche.GetComponent<Cotxe2>().stopCar = false;
                CancelInvoke();
            }
            else{
                npcCrossing = false;
            }
            position = 0;
        }
    }

    void OnTriggerEnter(Collider col) {
        if (col.gameObject.tag == "cotxe1" || col.gameObject.tag == "cotxe2") {
            Debug.Log("Ray shooted");
            coche = col.gameObject;
            InvokeRepeating("shootRay", 0, 0.5f);
        }
    }
}


