﻿using UnityEngine;
using SimpleJSON;
using System;
using System.IO;
using UnityEngine.SceneManagement;

public class JSON_Reader : MonoBehaviour
{
    /// <summary>
    /// Objectes de l'escena.
    /// </summary>
    public Objecte[] array_objectes; //declarem un array pels objectes de la escena (destral, botiquin,...).
    /// <summary>
    /// Nom de l'escena carregada.
    /// </summary>
    string levelName;

    //Objeto JSON que recoge el json de la BBDD para luego tratarlo y recoger datos.
    JSONNode jsn = new JSONNode();

    // Use this for initialization
    void Awake()
    {
        levelName = SceneManager.GetActiveScene().name;
        Debug.Log("LoadLevel: "+levelName);
        carregarEscena();
    }

    /// <summary>
    /// Crida a tots els inicialitzadors per preparar Objectes,Objectius i Temps a partir de JSON.
    /// </summary>
    void carregarEscena()
    {
        DontDestroyOnLoad(this);

        initializeJSON(); //inicialitzem el JSON
        initializeObjectes(); //inicialitzem els objectes
        initializeObjectius(); //inicialitzem els objectius
        initializeTime();  //inicialitzem el temps
    }
    /// <summary>
    /// Inicialitza l'array d'accions de cada objecte en l'array d'objectes del nivell.
    /// </summary>
    void initializeObjectes()
    {
        //Inicialitzem les accions per cada objecte dintre del array objectes
        foreach (Objecte o in array_objectes)
        {
            o.accions = initializeAccionsList(o.name);
        }
    }
    
    /// <summary>
    /// Llegeix cada objectiu del nivell i els guarda a l'array ar_objectius de GameManager.
    /// </summary>
    void initializeObjectius()
    {
        int length = jsn[0]["objectius"].Count; // total objectius del nivell.
        GameManager.ar_objectius = new Objectiu[length];
        for (int i = 0; i < length; i++)
        {
            string str_n = jsn[0]["objectius"][i]["nom"];// nom de l'objectiu.
            string str_textc = jsn[0]["objectius"][i]["text_completat"]; // descripcio de l'objectiu.
            string str_texti = jsn[0]["objectius"][i]["text_incomplet"]; // descripcio de l'objectiu.
            string str_textr = jsn[0]["objectius"][i]["text_regular"]; // descripcio de l'objectiu.
            string str_expl = jsn[0]["objectius"][i]["explicacio"]; // descripcio de l'objectiu.
            int str_time = jsn[0]["objectius"][i]["max_time"].AsInt; // ordre en el qual s'ha de completar.
            string type = jsn[0]["objectius"][i]["type"]; // tipus de l'objectiu

            switch (type)
            {
                case "Objectiu":
                    {
                        GameManager.ar_objectius[i] = new Objectiu(str_n, str_textc, str_texti, str_textr, str_expl, str_time);
                    }
                    break;
                case "MultiNode":
                    {
                        MultiObjective multiNode_objective = new MultiObjective(str_n, str_textc, str_texti, str_textr, str_expl, str_time, type);
                        int requisits_length = jsn[0]["objectius"][i]["accio"].Count;
                        multiNode_objective.initializeRequirements(requisits_length);
                        for (int x = 0; x < requisits_length; x++)
                        {
                            string requisit_name = jsn[0]["objectius"][i]["accio"][x].ToString();
                            Utilities.netejarString(ref requisit_name);
                            multiNode_objective.createRequirement(requisit_name, x);
                        }
                        GameManager.ar_objectius[i] = multiNode_objective;
                    }
                    break;
                case "MultiObjective":
                    {
                        MultiObjective multiNode_objective = new MultiObjective(str_n, str_textc, str_texti, str_textr, str_expl, str_time, type);
                        int requisits_length = jsn[0]["objectius"][i]["accio"].Count;
                        multiNode_objective.initializeRequirements(requisits_length);
                        for (int x = 0; x < requisits_length; x++)
                        {
                            string requisit_name = jsn[0]["objectius"][i]["accio"][x].ToString();
                            Utilities.netejarString(ref requisit_name);
                            multiNode_objective.createRequirement(requisit_name, x);
                        }
                    }
                    break;
                case "FireObjective":
                    {
                        int total_fires = jsn[0]["objectius"][i]["focs"].Count;
                        int objective = jsn[0]["objectius"][i]["objective"].AsInt;
                        string[] fire_names = new string[total_fires];
                        for (int x = 0; x < total_fires; x++)
                        {
                            string fire = jsn[0]["objectius"][i]["focs"][x];
                            GameObject.Find(fire).GetComponent<Foc>().setObjective(objective);
                            fire_names[x] = fire;
                        }
                        GameManager.ar_objectius[i] = new FireObjective(str_n, str_textc, str_texti, str_textr, str_expl, str_time, fire_names);
                    }
                    break;
            }
        }
    }
    
    //

    /// <summary>
    /// Llegir temps del nivell. Transforma minuts en segons. Prepara temps total de joc i temps minim.
    /// </summary>
    void initializeTime()
    {
        float min = jsn[1]["temps"][0]["minuts"].AsFloat;
        float segons = (jsn[1]["temps"][0]["segons"].AsFloat) + (min * 60);
        float minTime = jsn[1]["temps"][0]["minim"].AsFloat;
        GameManager.setSegons(segons);
        GameManager.setminTime(minTime);
    }
    
    /// <summary>
    /// Connecta amb la Base de dades, agafant el json del nivell en concret que tingui el mateix nom que variable levelName i omple el objeto jsn.
    /// </summary>
    /// <param name="jsn"> Objeto global tipo JSON</param>
    void initializeJSON()
    {
        string json = "Null";
        while (json == "Null")
        {
            try
            {
                WWWForm wwwForm = new WWWForm();
                wwwForm.AddField("levelName", levelName);
                WWW itemsData = new WWW("https://viodgamescom.000webhostapp.com/GetLevel.php", wwwForm);
                while (!itemsData.isDone) { }
                json = itemsData.text;
                jsn = JSONNode.Parse(json);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
                if (File.Exists("Error.txt"))
                {
                    Debug.Log("Error.txt" + " already exists.");
                    return;
                }
                StreamWriter sr = File.CreateText("Error.txt");
                sr.WriteLine(ex.ToString());
                sr.Close();
            }
        }
    }

    /// <summary>
    /// Retorna un array d'accions de l'objecte en concret. name es nom de l'objecte.
    /// </summary>
    /// <param name="jsn">Objecte type JSON on es guarden les dades del nivell</param>
    /// <param name="name"></param>
    /// <returns></returns>
    Accio[] initializeAccionsList(string name){
        /*if(name == "Destral"){
            Debug.Log("Now");
<<<<<<< HEAD
        }
        int total_objectes = jsn[2]["objectes"].Count; //total objectes de nivell.
=======
        }*/
        //jsn[2] fa referencia al JSON_objectes.txt
        int total_objectes = jsn[2]["objectes"].Count; // total objetos del nivel || esta linea peta en el accident4  
        Debug.Log(jsn[2]["objectes"]);
        Accio[] accions = new Accio[0];
        for (int i = 0; i < total_objectes; i++) //anar mirant 1 per 1 objectes del JSON per trobar el que volem.
        {
            string jsonObjectName = jsn[2]["objectes"][i]["nom"].ToString();
            Utilities.netejarString(ref jsonObjectName);
            if (jsonObjectName.Equals(name)) //comparar nom objecte amb el nom de l'objecte de JSON al qual s'esta apuntant.
            {
                int length = jsn[2]["objectes"][i]["accions"].Count; //total d'accions d'aquest objecte per crear array.
                accions = new Accio[length];
                for (int x = 0; x < length; x++) //bucle per comprobar accio una per una.
                {
                    string str_n = jsn[2]["objectes"][i]["accions"][x].ToString();
                    Utilities.netejarString(ref str_n);
                    int int_actionV = jsn[2]["objectes"][i]["valorac"][x].AsInt;
                    int node = jsn[2]["objectes"][i]["nodes"][x].AsInt;
                    string req = jsn[2]["objectes"][i]["dependencies"][x];

                    if ((jsn[2]["objectes"][i]["valordep"]) != null) // Si accio te dependencies, se li asignen.
                    {
                        string str_d = "";
                        int str_c = jsn[2]["objectes"][i]["valordep"][x].Count;
                        Debug.Log("Probando: " + jsn[2]["objectes"][i]["valordep"][x]);
                        int[] dep_values;
                        if (str_c > 0)
                        {
                            dep_values = new int[str_c];
                            for (int j = 0; j < str_c; j++)
                            {
                                dep_values[j] = jsn[2]["objectes"][i]["valordep"][x][j].AsInt;
                            }
                        }
                        else
                        {
                            dep_values = new int[1];
                            dep_values[0] = jsn[2]["objectes"][i]["valordep"][x].AsInt;
                        }
                        Accio acc = new Accio(str_n, str_d, int_actionV, dep_values, node, req);
                        accions[x] = acc;
                    }
                    else // Si no te dependencies, s'asigna directament.
                    {
                        Accio acc = new Accio(str_n, int_actionV, node, req);
                        accions[x] = acc;
                    }
                }
                break;
            }
        }
        return accions;
    }

}
