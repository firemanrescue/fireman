﻿using UnityEngine;
using System.Collections;

public class Alertante : Huma {

    private Victim.Posicio posicionVic;
    public AudioClip[] clips;
    public AudioSource adsourc;
    public Llaves llave;

    Alertante()
    {
        nom = "Alertant";
        
    }
    // Use this for initialization
    protected override void Start () {
        base.Start();
        llave = new Llaves();
        llave.catched = false;
        posicionVic = GameObject.Find("Victim").GetComponent<Victim>().posicio;
       // Debug.Log("Pos VIc: " + posicionVic);
        clips = new AudioClip[5];
        clips[0] = (AudioClip)Resources.Load("Audio/Alertador_0");
        clips[1] = (AudioClip)Resources.Load("Audio/Alertador_1"); //Baño
        clips[2] = (AudioClip)Resources.Load("Audio/Alertador_2"); //Dormitorio
        clips[3] = (AudioClip)Resources.Load("Audio/Alertador_3"); //Comedor
        clips[4] = (AudioClip)Resources.Load("Audio/Alertador_4"); //Cocina

        //this.GetComponent<Animator>().SetBool("hablado", false);
        this.GetComponent<Animator>().Play("Waving");
        adsourc = this.GetComponent<AudioSource>();
    }

    public void alert()
    {
        switch (posicionVic)
        {
            case Victim.Posicio.BATHROOM:
                {
                    adsourc.clip = clips[1];
                    adsourc.Play();
                }
                break;
            /*case Victim.Posicio.BEDROOM:
                {
                    adsourc.clip = clips[2];
                    adsourc.Play();
                }
                break;*/
            case Victim.Posicio.LIVINGROOM:
                {
                    adsourc.clip = clips[3];
                    adsourc.Play();
                }
                break;
            case Victim.Posicio.KITCHEN:
                {
                    adsourc.clip = clips[4];
                    adsourc.Play();
                }
                break;
        }
    }
}
