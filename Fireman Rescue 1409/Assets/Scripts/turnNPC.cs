﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class turnNPC : MonoBehaviour {

    public List<Transform> pointsToTravel;
    public NavMeshAgent agent;
    public int pointToTravelTowards;
    private int angle;
    private GameObject[] points;
    private GameObject[] parking;
    private GameObject[] banc;
    private GameObject[] observationPoints;
    private int contadorActions;
    private int[] order;
    public int[] randomOrder;
    public bool[] randomActions;
    private bool choosedBank;
    private bool ocupatedBank;
    private bool goToAccident;
    private float initialNavSpeed;
    public float navSpeed;

    private GameObject[] randomPoints;
    private bool running = false;
    //public bool goRun;
    //public bool sitting;
    //public bool walk;
    private bool ruteCalculated;
    private int contadorRunning = 0;
    private int contadorWalk = 0;
    private int bankToSit = 0;
    private int destination;

    // Use this for initialization
    void Start () {
        //agent = GetComponent<NavMeshAgent>();
        pointsToTravel = new List<Transform>();
        // Assign points
        //Poner a todos los puntos por donde se mueven los npc la etiqueta punts
        points = GameObject.FindGameObjectsWithTag("punts");
        parking = GameObject.FindGameObjectsWithTag("parking");
        banc = GameObject.FindGameObjectsWithTag("banc");
        observationPoints = GameObject.FindGameObjectsWithTag("observacion");
        /*randomActions = new bool[] { goRun, sitting, walk };
        for(int i = 0; i < randomActions.Length; i++) {
            randomActions[i] = false;
        }*/
        //Si se añade una nueva accion añadir aqui un nuevo numero
        contadorActions = 0;
        order = new int[] { 0, 1, 2, 3 };
        randomOrder = new int[order.Length];
        bool isIn = false;
        for (int i = 0; i < randomOrder.Length; i++) {
            randomOrder[i] = -1;
        }
        while (contadorActions < randomOrder.Length) {
            int random = Random.Range(0, randomOrder.Length);
            for (int i = 0; i < randomOrder.Length; i++) {
                if (randomOrder[i] == order[random]) {
                    isIn = true;
                }
            }
            if (isIn) {
                isIn = false;
            }
            else {
                randomOrder[contadorActions] = order[random];
                contadorActions++;
            }
        }
        //Añadir un false si se hace una nueva accion
        randomActions = new bool[] {false, false, false, false};
        contadorActions = 0;
        randomActions[contadorActions] = true;
        foreach (GameObject point in points) {
            pointsToTravel.Add(point.transform);
        }
        int aux = points.Length / 2;
        randomPoints = new GameObject[aux];

        ruteCalculated = false;
        choosedBank = false;
        goToAccident = false;
        initialNavSpeed = agent.speed;
        navSpeed = agent.speed;
    }
	
	// Update is called once per frame
	void Update () {
        if (randomActions[randomOrder[0]]) {
            Debug.Log("Walking");
            if (!ruteCalculated) {
                agent.enabled = true;
                randomizePoints();
                contadorWalk = 0;
                GetComponent<Animator>().SetBool("walk", true);
                ruteCalculated = true;
                agent.speed = initialNavSpeed;
                navSpeed = agent.speed;
            }
            this.GetComponent<NavMeshAgent>().enabled = true;
            agent.destination = randomPoints[contadorWalk].transform.position;
            Vector3 tempVector = transform.position - randomPoints[contadorWalk].transform.position;
            if (Vector3.SqrMagnitude(tempVector) < 10.0f) {
                contadorWalk++;
                int aux = points.Length / 2;
                if (contadorWalk == aux) {
                    contadorWalk = 0;
                    randomActions[randomOrder[0]] = false;
                    GetComponent<Animator>().SetBool("walk", false);
                    ruteCalculated = false;
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    nextAction();
                }
                else {
                    agent.destination = randomPoints[contadorWalk].transform.position;
                }
            }
            /*if(contadorWalk )
            Vector3 tempVector = transform.position - pointsToTravel[pointToTravelTowards].transform.position;
            if (Vector3.SqrMagnitude(tempVector) < 10.0f) {
                pointToTravelTowards = Random.Range(0, pointsToTravel.Count);
                agent.destination = pointsToTravel[pointToTravelTowards].transform.position;
            }*/
        }

        if (randomActions[randomOrder[1]]) {
            Debug.Log("Running");
            GetComponent<Animator>().SetBool("streach", true);
            randomActions[randomOrder[1]] = false;
        }

        if (randomActions[randomOrder[2]]) {
            Debug.Log("Sitting");
            if (!choosedBank){
                ocupatedBank = true;
                for (int i = 0; i < banc.Length; i++) {
                    if (banc[i].GetComponent<freeBanc>().freePoint) {
                        ocupatedBank = false;
                        bankToSit = i;
                    }
                }
                choosedBank = true;
                if (!ocupatedBank) {
                    GetComponent<Animator>().SetBool("walk", true);
                }
            }

            if (!ocupatedBank) {
                agent.enabled = true;
                agent.destination = banc[bankToSit].transform.position;
                Vector3 tempVector = transform.position - banc[bankToSit].transform.position;
                banc[bankToSit].GetComponent<freeBanc>().freePoint = false;
                if (Vector3.SqrMagnitude(tempVector) <= 0.5f){
                    GetComponent<Animator>().SetBool("walk", false);
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    float auxAngle = banc[bankToSit].GetComponent<freeBanc>().angle;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, auxAngle, transform.eulerAngles.z);
                    GetComponent<Animator>().SetBool("sit", true);
                    randomActions[randomOrder[2]] = false;
                    Invoke("getUp", 20);
                }
            }

            else {
                randomActions[randomOrder[2]] = false;
                nextAction();
            }
        }

        if (randomActions[randomOrder[3]]) {
            Debug.Log("Cotilleo");
            bool pointsFree = false;
            for (int i = 0; i < observationPoints.Length; i++) {
                if (observationPoints[i].GetComponent<freeBanc>().freePoint) {
                    pointsFree = true;
                }
            }
            if (pointsFree) {
                destination = 0;
                while (!observationPoints[destination].GetComponent<freeBanc>().freePoint){
                    destination = Random.Range(0, observationPoints.Length);
                }
                agent.enabled = true;
                agent.destination = observationPoints[destination].transform.position;
                GetComponent<Animator>().SetBool("walk", true);
                observationPoints[destination].GetComponent<freeBanc>().freePoint = false;
                goToAccident = true;
            }
            else {
                nextAction();
            }
            randomActions[randomOrder[3]] = false;
        }

        if (goToAccident){
            Vector3 tempVector = transform.position - observationPoints[destination].transform.position;
            //Debug.Log(Vector3.SqrMagnitude(tempVector));
            if (Vector3.SqrMagnitude(tempVector) < 1f) {
                int random = Random.Range(0, 2);
                Debug.Log("Random: " + random);
                if(random == 0) { 
                    float auxAngle = observationPoints[destination].GetComponent<freeBanc>().angle;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, auxAngle, transform.eulerAngles.z);
                    GetComponent<Animator>().SetBool("walk", false);
                    agent.enabled = false;
                }
                else {
                    nextAction();
                    observationPoints[destination].GetComponent<freeBanc>().freePoint = true;
                }
                goToAccident = false;
            }
        }

        if (running){
            agent.enabled = true;
            Vector3 tempVector = transform.position - randomPoints[contadorRunning].transform.position;
            if (Vector3.SqrMagnitude(tempVector) < 10.0f){
                contadorRunning++;
                int aux = points.Length / 2;
                if (contadorRunning == aux){
                    contadorRunning = 0;
                    running = false;
                    GetComponent<Animator>().SetBool("run", false);
                    nextAction();
                    //Debug.Log("Final");
                }
                else{
                    agent.destination = randomPoints[contadorRunning].transform.position;
                }
            }
        }
    }

    public void acabada()  {
        //FOR FOOTING
        randomizePoints();
        agent.enabled = true;
        agent.destination = randomPoints[0].transform.position;
        agent.speed = 3.5f;
        navSpeed = 3.5f;
        running = true;
        GetComponent<Animator>().SetBool("streach", false);
        GetComponent<Animator>().SetBool("run", true);
    }

    private void randomizePoints(){
        int contador = 0;
        bool isIn = false;
        for(int i = 0; i < randomPoints.Length; i++){
            randomPoints[i] = null;
        }
        int aux = points.Length / 2;
        while (contador < aux) {
            int random = Random.Range(0, points.Length);
            for (int i = 0; i < randomPoints.Length; i++) {
                if(randomPoints[i] == points[random]){
                    isIn = true;
                }
            }
            if (isIn){
                isIn = false;
            }
            else{
                randomPoints[contador] = points[random];
                contador++;
            }
        }
    }

    private void getUp(){
        GetComponent<Animator>().SetBool("up", true);
        Invoke("resetThings", 1f);
        nextAction();
    }

    private void resetThings() {
        GetComponent<Animator>().SetBool("sit", false);
        GetComponent<Animator>().SetBool("up", false);
    }

    private void nextAction() {
        contadorActions++;
        if(contadorActions == randomActions.Length) {
            contadorActions = 0;
        }
        Debug.Log(contadorActions);
        randomActions[contadorActions] = true;
    }
}
