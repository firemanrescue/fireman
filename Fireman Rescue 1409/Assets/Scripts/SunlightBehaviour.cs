﻿using UnityEngine;

public class SunlightBehaviour : MonoBehaviour {
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Rotate(Vector3.up, Time.deltaTime / 32);
	}
}
