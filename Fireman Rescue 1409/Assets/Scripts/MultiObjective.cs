﻿public class MultiObjective : Objectiu {

    public requisit[] requisits;
    public struct requisit
    {
        public bool completed;
        public string name;
    }

    public MultiObjective(string _nom, string text_c, string text_i, string text_r, string text_expl, int time, string type) :base(_nom, text_c, text_i, text_r, text_expl, time)
    {
        this.type = type;
    }

    public bool isRequirementCompleted(string name)
    {
        for (int i = 0; i < requisits.Length; i++)
        {
            if (requisits[i].name == name)
            {
                if (requisits[i].completed)
                    return true;
            }
        }
        return false;
    }
    public void completeRequirement(string name)
    {
        for (int i = 0; i < requisits.Length; i++)
        {
            if (requisits[i].name == name) requisits[i].completed = true;
        }
    }

    public override bool isCompleted()
    {
        foreach(requisit r in requisits)
        {
            if (!r.completed) return false;
        }
        return true;
    }
    public void createRequirement(string name, int pos)
    {
        requisit r;
        r.name = name;
        r.completed = false;
        requisits[pos] = r;
    }
    public void initializeRequirements(int length)
    {
        requisits = new requisit[length];
    }
    public bool hasOneCompleted(){

        foreach (requisit r in requisits)
        {
            if (r.completed) return true;
        }
        return false;
    }
}
