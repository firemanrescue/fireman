﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Objecte : MonoBehaviour {
    enum dependencies { NINGUNA, NOMANGUERA, MANGUERA, NODESTRAL, DESTRAL, NOBOTIQUIN, BOTIQUIN, NOCOMUNICAT, COMUNICANT, NOCOMUNICANT,
                        NOCOMUNICAEXT, NOALICATES, ALICATES, MENU00, MENU01, MENU02, LLAVES, APAGAT, BOMBER_ACOMP, NO_COMPLETAT, VICTIM };
    public Canvas menucanvas;       //TestCanvas
	public GameObject panelM;
    public GameObject victim;
    public GameObject victim2;
    public GameObject cam;
	public Camera camara;
	public GameObject Limit;
	public bool catched = false;
    //private new Renderer renderer;
    public Color basColor;
	public string nom;
	public List<Accio> options = new List<Accio>();  //acciones a mostrar en ese momento. Las cuales tienes acceso
	public Accio[] accions;   //Las acciones que puedes hacer
	public GameObject menu;
    public InterfaceManager interfaceManager;

    public GameObject cursorP, cursorH, cursorC;


    public void Awake()
    {
        panelM = GameObject.Find("MenuPanel");      //Panel CAnvas dentro del GO TestCanvas
        cursorP = GameObject.Find("Point");         //Cursor tipo punto dentro GO Canvas
        cursorH = GameObject.Find("HandCursor");
        cursorC = GameObject.Find("ChatCursor");
        victim = GameObject.Find("Victim");
        victim2 = GameObject.Find("Victim2");
        interfaceManager = GameObject.Find("InterfaceManager").GetComponent<InterfaceManager>();
    }

    protected virtual void Start()
    {
		cam = GameObject.Find ("FPSController");
        camara = GameObject.Find("FPSController").GetComponentInChildren<Camera>();
		panelM.gameObject.SetActive (false);
		menucanvas = GameObject.Find("TestCanv").GetComponent<Canvas>(); //Donde se muestran las opciones de la radio
		//renderer = this.GetComponent<MeshRenderer> ();
		//basColor = this.renderer.material.color;
		menu = GameObject.Find ("MenuObj");

        cursorH.SetActive(false);
        cursorC.SetActive(false);
    }

	void Update(){
		//if (catched) {
  //          renderer.material.shader = Shader.Find ("Transparent/Diffuse");
		//	basColor.a = 0.2f;
  //          //renderer.material.color = basColor;
		//}
		//if (!catched) {
		//	//renderer.material.shader = Shader.Find ("Standard");
		//	basColor.a = 0f;
  //          //renderer.material.color = basColor;
		//}
	}

	public void OnMouseDown(){
        if (camara.GetComponent<areaDetect>().detect && menucanvas.enabled == false){
            if (name == "Contenidor"){
                if ((GameObject.Find("focContenidor").GetComponent<Foc>().getApagat()) && (!GameObject.Find("Bomber").GetComponentInParent<Bomber>().getAcompanya())){
                    interfaceManager.pintaVolcar = true;                   
                }
            }

            createMenu();

		} else camara.GetComponent<areaDetect> ().detect = false;
	}

	public void OnMouseEnter(){
        Limit.SetActive(true);
        cursorP.SetActive(false);
        cursorH.SetActive(true);

    }

	public void OnMouseExit(){
		Limit.SetActive (false);
        cursorP.SetActive(true);
        cursorH.SetActive(false);
        

    }

    protected void clearMenu(){
        var children = new List<GameObject>();
        foreach (Transform child in menu.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

    protected virtual void checkOptions(){
        options.Clear();
        Debug.Log("Objecto usando : "+gameObject.name + " accion : "+ accions.Length);  ///peta ????

        for (int i = 0; i < accions.Length; i++){
            if (accions[i].dependenciaValues.Length > 0){
                int total_dep = accions[i].dependenciaValues.Length;
                int completed_deps = 0;

                for (int x = 0; x < total_dep; x++){
                    switch (accions[i].dependenciaValues[x]){
                        case (int)dependencies.NOMANGUERA:
                            {
                                if (!this.catched) completed_deps++;
                            }
                            break;
                        case (int)dependencies.MANGUERA:
                            {
                                if (this.catched) completed_deps++;
                            }
                            break;
                        case (int)dependencies.MENU00:
                            {
                                if (this.name == "Policia")
                                    if (this.GetComponent<Huma>().getNmenu() == 0) completed_deps++;
                                if (this.name == "Bomber")
                                    if (this.GetComponent<Huma>().getNmenu() == 0) completed_deps++;
                                if (this.name == "Alertant")
                                    if (this.GetComponent<Huma>().getNmenu() == 0) completed_deps++;
                                if (this.name == "Radio")
                                    if (this.GetComponent<MultiMenu>().getNmenu() == 0) completed_deps++;

                            }
                            break;
                        case (int)dependencies.MENU01:
                            {
                                if (this.name == "Policia")
                                    if (this.GetComponent<Huma>().getNmenu() == 1) completed_deps++;
                                if (this.name == "Bomber")
                                    if (this.GetComponent<Huma>().getNmenu() == 1) completed_deps++;
                                if (this.name == "Alertant")
                                    if (this.GetComponent<Huma>().getNmenu() == 1) completed_deps++;
                                if (this.name == "Victim")
                                    if (this.GetComponent<Huma>().getNmenu() == 1) completed_deps++;
                                if (this.name == "Victim2")
                                    if (this.GetComponent<Huma>().getNmenu() == 1) completed_deps++;
                                if (this.name == "Radio")
                                    if (this.GetComponent<MultiMenu>().getNmenu() == 1) completed_deps++;
                            }
                            break;
                        case (int)dependencies.MENU02:
                            {
                                if (this.name == "Alertant")
                                    if (this.GetComponent<Huma>().getNmenu() == 2) completed_deps++;
                                if (this.name == "Radio")
                                    if (this.GetComponent<MultiMenu>().getNmenu() == 2) completed_deps++;
                            }
                            break;

                        case (int)dependencies.NOBOTIQUIN:
                            {
                                if (this.name == "Victim")
                                {
                                    if (this.GetComponent<Huma>().getNmenu() == 0 && this.GetComponent<Victim>().arrosegar == false)
                                        if (!this.catched) completed_deps++;
                                }
                                else if (this.name == "Victim2")
                                {
                                    if (this.GetComponent<Huma>().getNmenu() == 0 && this.GetComponent<Victim>().arrosegar == false)
                                        if (!this.catched) completed_deps++;
                                }
                                else
                                {
                                    if (!this.catched) completed_deps++;
                                }
                            }
                            break;
                        case (int)dependencies.BOTIQUIN:
                            {
                                if (GameObject.Find("Botiquin").GetComponent<Objecte>().catched && SingletonGameObjectsHand.medicine)
                                {
                                    if (this.name == "Victim")
                                        if (this.GetComponent<Huma>().getNmenu() == 0) completed_deps++;
                                    if (this.name == "Victim2")
                                        if (this.GetComponent<Huma>().getNmenu() == 0) completed_deps++;
                                }
                            }
                            break;
                        case (int)dependencies.APAGAT:
                            {
                                if (GameObject.Find("focContenidor").GetComponent<Foc>().getApagat()) completed_deps++;
                            }
                            break;
                        case (int)dependencies.BOMBER_ACOMP:
                            {
                                if (GameObject.Find("Bomber").GetComponentInParent<Bomber>().getAcompanya()) completed_deps++;
                            }
                            break;

                        case (int)dependencies.DESTRAL:
                            {
                                if (GameObject.Find("Destral").GetComponent<Objecte>().catched && SingletonGameObjectsHand.axe)
                                {
                                    completed_deps++;
                                }
                            }
                            break;
                        case (int)dependencies.NODESTRAL:
                            {
                                if (!this.catched)
                                {
                                    completed_deps++;
                                }
                            }
                            break;
                        case (int)dependencies.NINGUNA:
                            {
                                if (options.Count != 0) completed_deps++;
                            }
                            break;
                        case (int)dependencies.ALICATES:
                            {
                                if (SingletonGameObjectsHand.alicates) completed_deps++;
                            }
                            break;
                        case (int)dependencies.NOALICATES:
                            {
                                if (!SingletonGameObjectsHand.alicates) completed_deps++;
                            }
                            break;
                        
                        case (int)dependencies.NO_COMPLETAT:
                            {
                                int obj_node = accions[i].getObjectiveNode();

                                if (GameManager.i_node.hasChild(obj_node))
                                {
                                    if (GameManager.i_node.getChild(obj_node).isAvailable())
                                    {
                                        if (accions[i].getNom() == "Comunicar extinció dels focs")
                                        {
                                            MultiObjective temporal_objective = (MultiObjective)GameManager.ar_objectius[GameManager.i_node.getChild(obj_node).getObjective()];
                                            if (!temporal_objective.isRequirementCompleted("comunicar_meitat"))
                                            {
                                                completed_deps++;
                                            }
                                        }
                                        if (accions[i].getNom() == "Comunicar situació inicial")
                                        {
                                            MultiObjective temporal_objective = (MultiObjective)GameManager.ar_objectius[GameManager.i_node.getChild(obj_node).getObjective()];
                                            if (!temporal_objective.isRequirementCompleted("comunicar_inici"))
                                            {
                                                completed_deps++;
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                        case (int)dependencies.LLAVES:
                            {
                                if (GameObject.Find("Alertant").GetComponent<Alertante>().llave.catched)
                                {
                                    completed_deps++;
                                }
                            }
                            break;
                        case (int)dependencies.VICTIM:
                            {
                                /*
                                if (victim.GetComponent<Victim>().arrosegar)
                                {
                                    completed_deps++;
                                }
                                else if(victim2.GetComponent<Victim>().arrosegar)
                                {
                                    completed_deps++;
                                }*/
                                completed_deps++;
                            }
                            break;
                        
                    }
                }
                if (completed_deps == total_dep) options.Add(accions[i]);
            }
            else
            {
                options.Add(accions[i]);
            }
        }
    }

    public virtual void createMenu(){ 
        GameManager.onMenu = true;
        checkOptions();
        if (menu.activeSelf == true)
        {
            if (options.Count > 0)
            {
                cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
                cam.gameObject.GetComponent<FirstPersonController>().enabled = false;
                panelM.gameObject.SetActive(true);
                menucanvas.enabled = true;
                menu.GetComponent<GridLayoutGroup>().constraintCount = 1;
                clearMenu();

                for (int i = 0; i < options.Count; i++)
                {
                    createOption(i);
                }
            }
        }
    }

    protected virtual void createOption(int i)
    {
        GameObject b = (GameObject)Instantiate(InterfaceManager.menubutton);
        b.GetComponentInChildren<Text>().text = options[i].getNom();
        int name = options[i].getActionValue();
        int obj_id = options[i].getObjectiveNode();
        b.GetComponent<Button>().onClick.AddListener(delegate {
            interfaceManager.CapturaBoto(name, cam, nom, menucanvas, obj_id);
        });
        b.transform.SetParent(menu.transform, false);
    }

}