﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class Briefing : MonoBehaviour
{
    int state;
	public Transform position;
	public Transform position1;
    public Transform eixGir;
    public Image FadeImg, anActuallyBetterWayOfDoingFadeImagesImage;
	public static float fadeSpeed = 10f;
	public static float unfadeSpeed = 3f;
	public static float smoothtime;
	public static bool fadeIn = false;
	private bool curva = false;
	private bool rotating = false;
    public bool FadedIn = false;
	public GameObject _botiquin;
	public GameObject _manega;
	public GameObject _destral;
    public GameObject _Era;//
	public Camera _mycam;
	public GameObject _player;
	AudioSource[] myAudios;
    public GameObject CamBriefing;

	public AudioClip comandamentDemanant;
	public AudioClip endavatComandament;
	public AudioClip explicacioInicial;
    public AudioClip explicacioInicial2;
    public AudioClip explicacioInicial3;
    public AudioClip rebutComandament;
	public AudioClip portaBombers;


	private bool auxiliarrotacions = true;
	// Use this for initialization
	void Start()
	{
		myAudios = GetComponents<AudioSource>();
		comandamentDemanant = Resources.Load("FX/saalaDemanantCorrecte") as AudioClip;
		endavatComandament = Resources.Load("FX/endavantPerComandament") as AudioClip;
		explicacioInicial = Resources.Load("FX/SORTIDA CONTENIDOR") as AudioClip;
        explicacioInicial2 = Resources.Load("FX/incendiHabitatge") as AudioClip;
        explicacioInicial3 = Resources.Load("FX/incendiHabitatge") as AudioClip;    //Demanar Inici Comunicaciò Bomber nivell3
        rebutComandament = Resources.Load("FX/rebutPerComandament") as AudioClip;
		portaBombers = Resources.Load("FX/porta") as AudioClip;
		StartCoroutine(audioPrincipi());
		smoothtime = 1f;
		this.GetComponentInChildren<Camera>().enabled = true;
		_mycam.enabled = false;
		_botiquin = GameObject.Find("Botiquin");
		_manega = GameObject.Find("Manguera");
		_destral = GameObject.Find("Destral");
        _Era = GameObject.Find("ERA");//
		_player = GameObject.Find("FPSController");
		_player.GetComponent<Personatge>().enabled = false;
		_player.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(true);
        state = 0;
        anActuallyBetterWayOfDoingFadeImagesImage = GameObject.Find("GoodFade").GetComponent<Image>();
        Color c = anActuallyBetterWayOfDoingFadeImagesImage.color;
        c.a = 255;
        anActuallyBetterWayOfDoingFadeImagesImage.color = c;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && state <= 3)  state = 3;

        switch (state)
        {
            case 0:
                {
                    if (!FadedIn)
                    {
                        anActuallyBetterWayOfDoingFadeImagesImage.CrossFadeAlpha(0f, 3, false);
                        FadedIn = true;
                    }
                    transform.position = Vector3.MoveTowards(transform.position, position1.position, 10f * Time.deltaTime);
                    if (transform.position == position1.position) state = 1;
                }
                break;
            case 1:
                {
                    if (!rotating)
                    {
                        rotating = true;
                        StartCoroutine(RotateOverTime(transform.localEulerAngles.y, transform.localEulerAngles.y + (90), smoothtime));
                    }
                    
                }
                break;
            case 2:
                {
                    transform.position = Vector3.MoveTowards(transform.position, position.position, 10f * Time.deltaTime);
                    if (this.transform.position == position.position) state = 3;
                }
                break;
            case 3:
                {
                    FadeToBlack();
                    anActuallyBetterWayOfDoingFadeImagesImage.gameObject.SetActive(false);
                    if (FadeImg.color == Color.black) state = 4;
                }
                break;
            case 4:
                {
                    transform.position = position.position;
                    transform.rotation = position.rotation;
                    this.GetComponentInChildren<Camera>().enabled = false;
                    CamBriefing.GetComponentInChildren<AudioListener>().enabled = false;    // Desactiva el audio listener
                    _botiquin.transform.parent = null;
                    _manega.transform.parent = null;
                    _destral.transform.parent = null;
                    _Era.transform.parent = null; //
                    _mycam.enabled = true;
                    _mycam.GetComponent<AudioListener>().enabled = true;
                    this.GetComponent<AudioSource>().enabled = false;
                    myAudios[1].clip = portaBombers;
                    myAudios[1].priority = 0;
                    myAudios[1].volume = 1;
                    myAudios[1].loop = false;
                    myAudios[1].Play();

                    InterfaceManager.movercoche = true;
                    fadeIn = true;
                    StopAllCoroutines();
                    state = 5;
                } break;
            case 5:
                {
                    FadeToClear();
                    if (FadeImg.color.a <= 0.1)
                    {
                        FadeImg.color = Color.clear;
                        StopAllCoroutines();
                        _player.GetComponent<Personatge>().enabled = true;
                        CamBriefing.SetActive(false);
                        if(GameObject.Find("Camera1") != null) {
                            GameObject.Find("Camera1").SetActive(false);
                        }
                        if (GameObject.Find("Camera2") != null) {
                            GameObject.Find("Camera2").SetActive(false);
                        }
                        this.GetComponent<Briefing>().enabled = false;
                    }
                }
                break;
        }
	}

	void FadeToClear()
	{
		// Lerp the colour of the image between itself and transparent.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.clear, unfadeSpeed * Time.deltaTime);

	}


	void FadeToBlack()
	{
		// Lerp the colour of the image between itself and black.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.black, fadeSpeed * Time.deltaTime);
	}

	IEnumerator RotateOverTime(float currentRotation, float desiredRotation, float overTime)
	{
        /*auxiliarrotacions = false;
		float i = 0.0f;
		while (i <= 1.1f)
		{
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, Mathf.Lerp(currentRotation, desiredRotation, i), transform.localEulerAngles.z);
			i += Time.deltaTime / overTime;
			yield return null;
		}
		rotating = false; // no longer rotating
        state = 2;
        */

        auxiliarrotacions = false;
        float i = 0.0f;
        while (i <= 1.0f)
        {
            transform.RotateAround(eixGir.position, Vector3.up, 90 * Time.deltaTime);
            i += Time.deltaTime / overTime;
            yield return null;
        }
        rotating = false;
        state = 2;

	}

	IEnumerator  audioPrincipi()
	{
		myAudios[0].PlayOneShot(comandamentDemanant);
		yield return new WaitForSeconds(3.0f);
		myAudios[0].PlayOneShot(endavatComandament);
		yield return new WaitForSeconds(3.0f);
        //Controlar nivel
        string levelname = SceneManager.GetActiveScene().name;
        if(levelname=="accident1")
        {
            myAudios[0].PlayOneShot(explicacioInicial);
            yield return new WaitForSeconds(8.5f);
        }
        else if(levelname == "accident2")
        {
            myAudios[0].PlayOneShot(explicacioInicial2);
            yield return new WaitForSeconds(13.0f);
        }
        else if (levelname == "accident3")
        {
            myAudios[0].PlayOneShot(explicacioInicial3);
            yield return new WaitForSeconds(13.0f);
        }

       
		myAudios[0].PlayOneShot(rebutComandament);
		yield return new WaitForSeconds(3.0f);
	}


}