﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class BloodSplatter : MonoBehaviour
{

    public Texture blood;

    public Color guiColor;
    public Color guiColorAlpha;

    // Use this for initialization
    void Start()
    {
        guiColor = Color.white;
        guiColorAlpha = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        guiColor.a = ((500 - SingletonCharacter.health) / 500) - 0.1f;
    }

    void OnGUI()
    {
        GUI.color = guiColor;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blood);
    }
}