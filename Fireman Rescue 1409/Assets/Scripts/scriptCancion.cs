﻿using UnityEngine;
using System.Collections;

public class scriptCancion : MonoBehaviour {
    /// <summary>
    /// Script for control de audio of the scene (ONLY ONe CLIP->PLAY & STOP)
    /// </summary>
    /// 
    //Vars statics
    static AudioSource ADS;
    static scriptCancion instanceRef;
    /// <summary>
    /// get audiosource component
    /// </summary>
	// Use this for initialization
	void Awake () {       
        if (instanceRef == null)
        {
            ADS = this.GetComponent<AudioSource>();
            instanceRef = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
	
	public static void Play()
    {
        ADS.Play();
    }
    public static void Stop()
    {
        if(ADS!= null)
        ADS.Stop();
    }
}
