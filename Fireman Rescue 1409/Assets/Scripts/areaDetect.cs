﻿using UnityEngine;
using System.Collections;

public class areaDetect : MonoBehaviour {
	public bool areaOb;
    static public Collider[] areaAr;
    public LayerMask myLayerMask;
    public LayerMask mySecondLayerMask;
    public bool detect = false;
	public GameObject menu;
	
    // Use this for initialization
	void Start () {
		menu = GameObject.Find ("MenuObj");
	}

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit = new RaycastHit();
        areaOb = Physics.SphereCast(transform.position, 0.2f, transform.forward, out hit, 2, myLayerMask.value);
        //areaAr = Physics.OverlapSphere(transform.position, 1f, mySecondLayerMask);
        if (areaOb)
        {
            detect = true;
            menu.SetActive(true);
        }
        else
        {
            detect = false;
        }
    }
}
