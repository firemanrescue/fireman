﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections.Generic;

public class Victim : Huma {

    public enum Posicio { BATHROOM, LIVINGROOM, KITCHEN }
    public enum Posicio2 { PLANTA1, PLANTA2, PLANTA3 }
    //Posiciones Victima1
    public Transform posBath;
    public Transform posKitch;
    public Transform posLiving;
    //Posiciones Victima2 escalas
    public Transform pos1;
    public Transform pos2;
    public Transform pos3;
    //Posiciones Victima2 habitaciones
    public Transform posHab1;
    public Transform posHab2;
    public Transform posHab3;

    public GameObject coll;
    private GameObject player;
    private UnityEngine.AI.NavMeshAgent ComponenteDeNavegacion;
    public bool arrosegar = false;
    public bool arrosegaHelper = false;
    private bool freez = true;

    public  Posicio posicio;
    public  Posicio2 posicio2;

    private int finalPosition;
    private int posVictim2;
    public GameObject helper;

    void Awake()
    {
        player = GameObject.Find("FPSController");
        ComponenteDeNavegacion = this.GetComponent<UnityEngine.AI.NavMeshAgent>();

        finalPosition = Random.Range(0, 3);

        if (this.gameObject.name == "Victim")
        {
            victim = this.gameObject;
            switch (finalPosition)
            {
                case 0:
                    {
                        gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posBath.transform.position;
                        posicio = Posicio.BATHROOM;
                    }
                    break;
                case 1:
                    {
                        gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posLiving.transform.position;
                        posicio = Posicio.LIVINGROOM;

                    }
                    break;
                case 2:
                    {
                        gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posKitch.transform.position;
                        posicio = Posicio.KITCHEN;
                    }
                    break;
            }
        }
    }

    void Start()
    {
        base.Start();
        posVictim2 = Random.Range(0,2); //si es 0=>aparece victima en escalas, si es 1=>aparece victima en habitaciones
        
        if (this.gameObject.name == "Victim2")
        {
            victim2 = this.gameObject;
            if(posVictim2==0) //escalas
            {
                GameManager.victim2placed = false;
                switch (finalPosition)
                {
                    case 0:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = pos1.transform.position;
                            posicio2 = Posicio2.PLANTA1;
                        }
                        break;
                    case 1:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = pos2.transform.position;
                            posicio2 = Posicio2.PLANTA2;
                        }
                        break;
                    case 2:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = pos3.transform.position;
                            posicio2 = Posicio2.PLANTA3;
                        }
                        break;
                }
            }
            else if(posVictim2==1) //habitacio
            {
                GameManager.victim2placed = true;
                switch (finalPosition)
                {
                    case 0:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posHab1.transform.position;

                        }
                        break;
                    case 1:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posHab2.transform.position;

                        }
                        break;
                    case 2:
                        {
                            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posHab3.transform.position;

                        }
                        break;
                }
            }
            

        }


        //focHab.SetActive(true);
        /*
        //Añadimos solo los fuegos de la habitacion (Hardcode)
        n_fires = focHab.transform.GetComponentsInChildren<Foc>();
        foreach (Foc f in n_fires) fires.Add(f);
        ((FireObjective)GameManager.ar_objectius[8]).addFires(fires);
        */
        ////

    }
    /// <summary>
    /// Random position of Fires and victim
    /// </summary>
    /*
    void Start () {
        base.Start();
        victim = this.gameObject;
        finalPosition = Random.Range(0, 4);
        List<Foc> fires;
        Foc[] n_fires;
        fires = new List<Foc>();
        switch (finalPosition)
        {
            case 0:
                {
                    gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posBath.transform.position;
                    posicio = Posicio.BATHROOM;
                    focMenjador.SetActive(true);
                    focHab.SetActive(true);

                    n_fires = focMenjador.transform.GetComponentsInChildren<Foc>();
                    foreach(Foc f in n_fires) fires.Add(f);
                    n_fires = focHab.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    ((FireObjective)GameManager.ar_objectius[8]).addFires(fires);

                }
                break;

            case 1:
                {
                    gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posBed.transform.position;
                    posicio = Posicio.BEDROOM;
                    focMenjador.SetActive(true);
                    focCuina.SetActive(true);

                    n_fires = focMenjador.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    n_fires = focCuina.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    ((FireObjective)GameManager.ar_objectius[8]).addFires(fires);
                }
                break;
            case 2:
                {
                    gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posLiving.transform.position;
                    posicio = Posicio.LIVINGROOM;
                    focHab.SetActive(true);
                    focCuina.SetActive(true);

                    n_fires = focHab.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    n_fires = focCuina.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    ((FireObjective)GameManager.ar_objectius[8]).addFires(fires);
                }
                break;
            case 3:
                {
                    gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = posKitch.transform.position;
                    posicio = Posicio.KITCHEN;
                    focMenjador.SetActive(true);
                    focHab.SetActive(true);

                    n_fires = focMenjador.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    n_fires = focHab.transform.GetComponentsInChildren<Foc>();
                    foreach (Foc f in n_fires) fires.Add(f);
                    ((FireObjective)GameManager.ar_objectius[8]).addFires(fires);
                }
                break;
        }
    }
    */

    void FixedUpdate()
    {
        if (arrosegar)
        {
            SingletonGameObjectsHand.alicates = false;
            SingletonGameObjectsHand.axe = false;
            SingletonGameObjectsHand.manguera = false;
            SingletonGameObjectsHand.medicine = false;
            SingletonGameObjectsHand.radio = false;
            GetComponent<Animator>().SetBool("pulled", true);
            StartCoroutine("levantarVictima");

            //gameObject.layer = 10;
            player.GetComponent<FirstPersonController>().m_MouseLook.XSensitivity = 0.4f;//XSensitivity = 0.3f;
            player.GetComponent<FirstPersonController>().m_MouseLook.YSensitivity = 0.3f;
           /* for (int i = 0; i < 6; i++)
            {
                transform.GetChild(i).gameObject.layer = 10;
            }*/
            if (freez)
            {

                transform.Rotate(0, -90, 0);
                GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezePositionY;
                freez = false;
            }
            coll.SetActive(false);
            transform.rotation = Quaternion.Euler(GetComponent<Rigidbody>().transform.eulerAngles.x, cam.GetComponent<Rigidbody>().transform.eulerAngles.y,GetComponent<Rigidbody>().transform.eulerAngles.z);
            cam.GetComponent<FirstPersonController>().m_WalkSpeed = 2f;
            cam.GetComponent<FirstPersonController>().m_RunSpeed = 3f;          //velocidad carrera cuando estas arrastrando Victim
            
            //GetComponent<BoxCollider>().enabled = false;
           // if (areaDetect.areaAr.Length < 0.5f) {
                ComponenteDeNavegacion.SetDestination(cam.GetComponent<Rigidbody>().position/*transform.position = Vector3.MoveTowards(transform.position, cam.GetComponent<Rigidbody>().position, cam.GetComponent<Rigidbody>().velocity.magnitude / 50)*/);
           // }
            //else { ComponenteDeNavegacion.SetDestination(transform.position); }
        }

        //Cuando es arrastrado por el Helper
        if(arrosegaHelper)
        {
            if (GameManager.levelname != "accident3")
            {
                helper = GameObject.Find("Helper"); //Modificar para identificar que helper le toca, posible con ontriggerenter
            }
            GetComponent<Animator>().SetBool("pulled", true);
            if (freez)
            {
                transform.Rotate(0, -90, 0);
                GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezePositionY;
                freez = false;
            }
            //coll.SetActive(false);
            GetComponent<BoxCollider>().enabled = false;

            ComponenteDeNavegacion.SetDestination(helper.transform.position);
            /*
            if (GameManager.levelname == "accident3")
            {
                GameObject helper1 = GameObject.Find("Helper1");
                GameObject helper2 = GameObject.Find("Helper2");
                ComponenteDeNavegacion.SetDestination(helper1.transform.position);
            }
            else
            {
                ComponenteDeNavegacion.SetDestination(helper.transform.position);
            }
            */
        }

    }
    IEnumerator levantarVictima()
    {
        Animator anim = GetComponent<Animator>();
            anim.SetFloat("blend", anim.GetFloat("blend")+0.1f);
            yield return new WaitForSeconds(0.1f);
    }
    public void Leave()
    {
        player.GetComponent<FirstPersonController>().m_MouseLook.XSensitivity = 1f;
        player.GetComponent<FirstPersonController>().m_MouseLook.YSensitivity = 1f;
        cam.GetComponent<FirstPersonController>().m_WalkSpeed = 3f;
        cam.GetComponent<FirstPersonController>().m_RunSpeed = 6f;
        this.setNmenu(-1);
        
    }

}
