﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceLightRotationScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(transform.right, Time.deltaTime * 90f);
    }
}
