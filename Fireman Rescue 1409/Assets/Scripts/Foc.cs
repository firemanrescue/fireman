﻿using UnityEngine;

/// <summary>
/// Script for control the fires in the game
/// </summary>
public class Foc : MonoBehaviour
{
    private ParticleSystem ps;
    private ParticleSystem fum;
    [SerializeField]
    private bool apagat;
    private string objective_name;
    private int objective = -1;
   

    // Use this for initialization
    void Start()
    {
        ps = this.GetComponent<ParticleSystem>();
        if(transform.childCount >0)
        fum = transform.GetChild(0).GetComponent<ParticleSystem>();
    }

    void Update()
    {
		if (apagat && fum != null)
        {
            float particles = Utilities.GetEmissionRate(fum);
            Utilities.SetEmissionRate(fum, particles - 0.1f);
        }
    }

    public void setObjective(int o)
    {
        objective = o;
    }
    public void apagarFoc()
    {
        float particles = Utilities.GetEmissionRate(ps);
        Utilities.SetEmissionRate(ps, particles - 0.05f);
        if (particles <= 0 && !apagat)
        {
            //ps.Stop();
            Destroy(this.gameObject.GetComponent<BoxCollider>());
            //GetComponent<BoxCollider>().enabled = false;
            apagat = true;

            this.gameObject.SetActive(false);

            if (objective!= -1) completeFire();

        }
    }

    public void apagarFocRapid()
    {
        float particles = Utilities.GetEmissionRate(ps);
        Utilities.SetEmissionRate(ps, particles - 0.1f);
        if (particles <= 0 && !apagat)
        {
            //ps.Stop();
            GetComponent<BoxCollider>().enabled = false;
            apagat = true;
            
            if (objective != -1) completeFire();
        }
    }

    public bool getApagat()
    {
        return apagat;
    }

    private void completeFire()
    {
        int cost;
        Node n;
        GameManager.i_node.lookForChild(objective, out n, out cost);
        int i = n.getObjective();

        if (GameManager.ar_objectius[i].isCompleted())
        {
            GameManager.completeObjective(objective,true);
        }
    }
}
