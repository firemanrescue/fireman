﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlPersonatge : MonoBehaviour {

    public List<Transform> pointsToTravel;
    private NavMeshAgent agent;
    public int pointToTravelTowards;
    private GameObject[] points;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        pointsToTravel = new List<Transform>();
        // Assign points
        //Poner a todos los puntos por donde se mueven los npc la etiqueta punts
        points = GameObject.FindGameObjectsWithTag("punts");
        foreach(GameObject point in points){
            pointsToTravel.Add(point.transform);
        }
        
        pointToTravelTowards = Random.Range(0, pointsToTravel.Count);
        agent.destination = pointsToTravel[pointToTravelTowards].transform.position;
        GetComponent<Animator>().SetBool("walk", true);
    }
	
	// Update is called once per frame
	void Update () {

        /*Transform point;
        for (uint i = 0; i != pointsToTravel.Count; i++)
        {
            point = pointsToTravel[(int)i];
            if (FastApproximately(transform.position.x, point.position.x, 0.5f) && FastApproximately(transform.position.z, point.position.z, 0.5f)) 
            {
                pointToTravelTowards = Random.Range(0, pointsToTravel.Count);
            }
        }
        agent.destination = pointsToTravel[pointToTravelTowards].transform.position;*/

        Vector3 tempVector = transform.position - pointsToTravel[pointToTravelTowards].transform.position;
        if (Vector3.SqrMagnitude(tempVector) < 10.0f) {
            pointToTravelTowards = Random.Range(0, pointsToTravel.Count);
            agent.destination = pointsToTravel[pointToTravelTowards].transform.position;
        }

    }

    /*public static bool FastApproximately(float a, float b, float threshold) {
        return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
    }*/
}
