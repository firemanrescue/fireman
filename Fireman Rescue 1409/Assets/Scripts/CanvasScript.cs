﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Script para controlar el inventory interface al pulsar "e"
/// </summary>
public class CanvasScript : MonoBehaviour
{
    public GameObject era, w1, w2, extra;

    public GameObject goEra, goAxe, goMed, goSho;

    public Sprite none, eraSprite, axe, medicine, shower;

    public Sprite eraSpriteWB;

    int[] bag; //0.- ERA | 1.-/2.-Hands | 3.- Extra. Objects in hands go from 0 to 3, 1.- Axe | 2.- Medicine | 3.- Shower | 0.- Nothing

    // Use this for initialization
    void Start()
    {
        bag = new int[4];

        for (int i = 0; i < bag.Length; ++i)
        {
            bag[i] = 0;
        }


    }

    // Update is called once per frame
    void Update()
    {
        setTime();
        if (bag[0] == 0)
        {
            era.transform.GetComponent<Image>().sprite = none;
            if (goEra.GetComponent<ERA>().catched)
            {
                bag[0] = 1;
            }
        }
        else if (bag[0] == 1)
        {
            era.transform.GetComponent<Image>().sprite = eraSprite;
            if (!goEra.GetComponent<ERA>().catched)
            {
                bag[0] = 0;
            }
        }

        if (bag[1] == 1)
        {

            w1.transform.GetComponent<Image>().sprite = axe;

            if (!goAxe.GetComponent<Destral>().catched)
            {
                bag[1] = 0;
            }

        }
        else if (bag[1] == 2)
        {
            w1.transform.GetComponent<Image>().sprite = medicine;

            if (!goMed.GetComponent<Botiquin>().catched)
            {
                bag[1] = 0;
            }

        }
        else if (bag[1] == 3)
        {

            w1.transform.GetComponent<UnityEngine.UI.Image>().sprite = shower;

            if (!goSho.GetComponent<Manguera>().catched)
            {
                bag[1] = 0;
            }

        }
        else if (bag[1] == 0)
        {

            w1.transform.GetComponent<UnityEngine.UI.Image>().sprite = none;

            if ((goAxe.GetComponent<Destral>().catched) && (bag[2] != 1))
            {
                bag[1] = 1;
            }
            else if ((goMed.GetComponent<Botiquin>().catched) && (bag[2] != 2))
            {
                bag[1] = 2;
            }
            else if ((goSho.GetComponent<Manguera>().catched) && (bag[2] != 3))
            {
                bag[1] = 3;

            }
        }

        if (bag[2] == 1)
        {

            w2.transform.GetComponent<Image>().sprite = axe;

            if (!goAxe.GetComponent<Destral>().catched)
            {
                bag[2] = 0;
            }

        }
        else if (bag[2] == 2)
        {
            w2.transform.GetComponent<Image>().sprite = medicine;

            if (!goMed.GetComponent<Botiquin>().catched)
            {
                bag[2] = 0;
            }

        }
        else if (bag[2] == 3)
        {

            w2.transform.GetComponent<Image>().sprite = shower;

            if (!goSho.GetComponent<Manguera>().catched)
            {
                bag[2] = 0;
            }

        }
        else if (bag[2] == 0)
        {
            w2.transform.GetComponent<Image>().sprite = none;

            if ((goAxe.GetComponent<Destral>().catched) && (bag[1] != 1))
            {
                bag[2] = 1;
            }
            else if ((goMed.GetComponent<Botiquin>().catched) && (bag[1] != 2))
            {
                bag[2] = 2;
            }
            else if ((goSho.GetComponent<Manguera>().catched) && (bag[1] != 3))
            {
                bag[2] = 3;

            }
        }

        if (bag[3] == 0)
        {
            extra.transform.GetComponent<Image>().sprite = none;



        }
        else if (bag[3] == 1)
        {

        }
    }

    private void setTime()
    {

        this.GetComponentInChildren<Text>().text = GameManager.getTime();

    }
}

