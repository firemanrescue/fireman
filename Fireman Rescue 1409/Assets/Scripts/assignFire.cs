﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class assignFire : MonoBehaviour {

    public LayerMask layerMask;
    RaycastHit hit;
    public GameObject player;
    // Update is called once per frame
    void Update () {

        if (Physics.SphereCast(this.transform.position, 0.5f, transform.up, out hit, 100, layerMask))
        {
            if (player.GetComponent<Personatge>().switchingOff)
            {
                GameManager.foc = hit.collider.gameObject;
                GameManager.foc.GetComponent<Foc>().apagarFoc();
            }
        }
        else GameManager.foc = null;
	}
}
