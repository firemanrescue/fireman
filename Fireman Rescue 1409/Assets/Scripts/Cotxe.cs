﻿ using UnityEngine;
using System.Collections;

public class Cotxe : MonoBehaviour
{
	private Vector3[] disparosray;
	private Vector3 disparoray;
	private float distanciaray;
	private float stop;
	private GameObject persona;
	bool rotating = false;
	private bool continua;
	private float temps;
	private float temps2;
	private bool colisioncotxe;
	public float smoothTime; //rotate over 5 seconds
	public float velocity;
	private bool colisionando;
	private float auxRotacion;

	void Start()
	{	
		distanciaray = 5;
		disparosray = new Vector3[]{ Vector3.forward, -Vector3.right, -Vector3.forward,  Vector3.right };
		persona = GameObject.FindGameObjectWithTag("pj");
		smoothTime = 1.0f;
		continua = true;
		temps = 0;
		temps2= 0;
		colisioncotxe = false;
		velocity = 7.0f;
		colisionando = false;
	}
	void Update()
	{
		temps += Time.deltaTime;
		stop = Vector3.Distance (this.transform.position, persona.transform.position);
		if (stop > 4f && continua == true) {
			this.GetComponent<Rigidbody> ().MovePosition (transform.position + transform.forward * velocity * Time.deltaTime);
		} else if (stop > 4f && continua == false && temps > 4) {
			continua = true;
			colisioncotxe = true;
		} else if (stop < 4f) {
			continua = false;
			temps = 0;
		} 

		if (stop < 4f) {
			if (colisionando == true) {
				InterfaceManager.numcops--;
				colisionando = false;
			}
		}else{
			colisionando = true;
		}

		RaycastHit hit;
		Ray ray = new Ray (new Vector3 (transform.position.x, transform.position.y + 1, transform.position.z), disparoray);
		//Debug.DrawRay (new Vector3 (transform.position.x, transform.position.y + 1, transform.position.z), disparoray * distanciaray, Color.red);

		if (Physics.Raycast (ray, out hit, distanciaray)) {

			if (hit.collider.tag == "cotxe") {
				velocity = 0.0f;
			}
		} else if (velocity == 0.0f) {
			temps2 += Time.deltaTime;
			if (temps2 > 2) {
				velocity = 7.0f;
				temps2 = 0;
			}
		}else
			velocity = 7.0f;
	}


	void OnTriggerEnter(Collider col)
	{
		switch (col.gameObject.tag) {
		case "recta1":
			disparoray = disparosray [0];
			break;
		case "recta2":
			disparoray = disparosray [1];
			break;
		case "recta3":
			disparoray = disparosray [2];
			break;
		case "recta4":
			disparoray = disparosray [3];

			break;
		case "curva1":
			rotating = true;
		//	StartCoroutine(RotateOverTime(transform.localEulerAngles.y, transform.localEulerAngles.y + (-90), smoothTime));
			break;
		default:
			break;

		}
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "pj")
		{
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		} 
	}

	void OnCollisionExit(Collision other)
	{
		if (other.gameObject.tag == "pj")
		{
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
		}

	}

	void OnTriggerStay(Collider col)
	{
		if (rotating == true) {
			auxRotacion = this.transform.localEulerAngles.y;
			rotating = false;
		}
		if(col.gameObject.tag == "curva1"){
			//if (this.transform.localEulerAngles.y >= 1) {
				if(auxRotacion -89 <= this.transform.localEulerAngles.y)
					transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y - 1 * (Time.deltaTime * 40), transform.localEulerAngles.z);
			//} else {
			//	if (this.transform.localEulerAngles.y <= 1) {
				//	if (auxRotacion - 90 < this.transform.localEulerAngles.y) {
				//		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y - 2 * (Time.deltaTime * 20), transform.localEulerAngles.z);
				//	}
				//}
			//}
			
		
		}
		//transform.RotateAround (new Vector3 (this.transform.position.x +1, this.transform.position.y, this.transform.position.z), -Vector3.up, 50 * Time.deltaTime);
	}

	/*IEnumerator RotateOverTime(float currentRotation, float desiredRotation, float overTime)
	{
		float i = 0.0f;
		while (i <= 1.1f)
		{
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, Mathf.Lerp(currentRotation, desiredRotation, i) * (Time.deltaTime * 20), transform.localEulerAngles.z);
			i += Time.deltaTime / overTime;
			yield return null;
		}
		yield return new WaitForSeconds(overTime);
		rotating = false; // no longer rotating
	}*/
}