﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectHelper : MonoBehaviour {



    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Helper1")
        {
            this.GetComponent<Victim>().helper = other.gameObject;
        }
        else if (other.gameObject.name == "Helper2")
        {
            this.GetComponent<Victim>().helper = other.gameObject;
        }
    }

}
