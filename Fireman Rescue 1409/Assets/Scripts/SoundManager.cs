﻿using UnityEngine;
using System.Collections;

public class SoundManager {

    private static SoundManager instance = null;
    private SoundManager() { }
    private gameSound[] gameSounds;
    struct gameSound
    {
        public float volume;
        public AudioSource audio;
    }

    public static SoundManager getInstance()
    {
        if (instance == null) instance = new SoundManager();
        return instance;
    }

    public void getAllSounds()
    {
        AudioSource[] asr = Object.FindObjectsOfType<AudioSource>();
        gameSounds = new gameSound[asr.Length];
        for (int i=0; i<asr.Length;i++)
        {
            float v = asr[i].volume;
            gameSound gs;
            gs.volume = v;
            gs.audio = asr[i];
            gameSounds[i] = gs;
        }
    }

    public void setVolumes(float vol)
    {
        for(int i=0;i<gameSounds.Length;i++)
        {
            gameSounds[i].audio.volume = vol;
        }
    }
    public void setVolumes(float vol, string exception)
    {
        for (int i = 0; i < gameSounds.Length; i++)
        {
            if(gameSounds[i].audio.clip != null)
            {
                if (gameSounds[i].audio.clip.name != exception)
                    gameSounds[i].audio.volume = vol;
            }          
        }
    }

    public void restartVolumes()
    {
        for (int i = 0; i < gameSounds.Length; i++)
        {
            gameSounds[i].audio.volume = gameSounds[i].volume;
        }
    }


}
