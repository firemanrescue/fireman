﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
//using System.Data.Odbc;
//using System.Data;

/// <summary>
/// Script per controlar inputs en textboxs
/// </summary>
static public class Utilities
{

    /// <summary>
    /// Encriptar string desitjat.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    static public string Encrypt(string input)
    {
        using (SHA1Managed sha1 = new SHA1Managed())
        {
            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sb = new StringBuilder(hash.Length * 2);

            foreach (byte b in hash)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }
    }
    /// <summary>
    /// Comprovar si string es un correu electronic.
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    static public bool IsEmail(string email)
    {
        bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        return isEmail;
    }
    /// <summary>
    /// Comprovar si string conte lletres.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    static public bool containsLetters(string str)
    {
        int letterCount = Regex.Matches(str, @"[a-zA-Z]").Count;
        if (letterCount > 0) return true;
        else return false;
    }
    /// <summary>
    /// Comprovar si string conte nombres.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    static public bool containsNumbers(string str)
    {
        int numberCount = Regex.Matches(str, @"[0-9]").Count;
        if (numberCount > 0) return true;
        else return false;
    }
    /// <summary>
    /// Extreure simbols no desitjat d'un string.
    /// </summary>
    /// <param name="str"></param>
    public static void netejarString(ref string str)
    {
        var charsToRemove = new string[] { "@", ";", "\"" };
        foreach (var c in charsToRemove)
        {
            str = str.Replace(c, string.Empty);
        }
    }
    public static int getObjectivePosition(string objective_name)
    {
        for (int i = 0; i < GameManager.ar_objectius.Length; i++)
        {
            if (GameManager.ar_objectius[i].getNom() == objective_name)
            {
                return i;
            }
        }
        return 0;
    }
    public static string getTimeString(float segons)
    {
        float modul = segons % 60;
        string minutos = (Mathf.Floor(segons / 60)).ToString();
        string seconds = modul.ToString();
        if (modul < 10)
        {
            seconds = "0" + seconds;
        }
        return minutos + " : " + seconds;
    }
    public static float GetEmissionRate(this ParticleSystem particleSystem)
    {
        //return particleSystem.emission.rate.constantMax;
        return particleSystem.emission.rateOverTime.constantMax;
    }

    /// <summary>
    /// Change the rate emission of the particles
    /// </summary>
    /// <param name="particleSystem">Objeto particula</param>
    /// <param name="emissionRate">tasa de emison de particulas</param>
    public static void SetEmissionRate(ParticleSystem particleSystem, float emissionRate)
    {
        var emission = particleSystem.emission;
        //var rate = emission.rate;
        var rate = emission.rateOverTime;
        rate.constantMax = emissionRate;
        //emission.rate = rate;
        emission.rateOverTime = rate;
    }


}
