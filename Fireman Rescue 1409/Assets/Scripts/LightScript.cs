﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.RotateAround(this.gameObject.transform.position, Vector3.up, 800 * Time.deltaTime);
	}
}
