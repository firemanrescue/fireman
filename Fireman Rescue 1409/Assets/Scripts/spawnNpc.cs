﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class spawnNpc : MonoBehaviour {

    //Poner GameObject que sera el parent de los NPC
    public GameObject parent; 
    //Poner aqui la ruta de los prefabs de todos los npc
    private string[] people;
    //Número de persones que vols que l'spawner crei al començar
    public int initialPeople;
    //Número total de nps que volem instanciar durant el nivell
    public int maxNpc;
    //Tiempo que tarda en instanciar de nuevo
    public float timeToNext;
    private int random = 0;
    private Vector3 pos;

	// Use this for initialization
	void Start () {
        people = new string[] { "Prefabs/NPC/Andrew", "Prefabs/NPC/Charles", "Prefabs/NPC/Charlotte", "Prefabs/NPC/Edgar", "Prefabs/NPC/Eleanor",
            "Prefabs/NPC/Eva", "Prefabs/NPC/Gabriel", "Prefabs/NPC/George", "Prefabs/NPC/Kevin", "Prefabs/NPC/Leo", "Prefabs/NPC/Lisa", "Prefabs/NPC/Olivier",
            "Prefabs/NPC/Samantha", "Prefabs/NPC/Sandra", "Prefabs/NPC/Sarah", "Prefabs/NPC/Terence", "Prefabs/NPC/Thelma", "Prefabs/NPC/Tommy",
            "Prefabs/NPC/Wanda", "Prefabs/NPC/Warren"};
        Debug.Log("Initial People: " + initialPeople);
        //Vector3 create = GetRandomPointOnMesh();
        //Debug.Log(create);
        for (int i = 0; i < initialPeople; i++){
            int rand = Random.Range(0,people.Length);
            pos = GetRandomPointOnMesh();
            GameObject person = Instantiate(Resources.Load(people[rand]), pos, transform.rotation) as GameObject;
            person.transform.parent = parent.transform;
        }
        timeToNext = 2;
    }
	
	// Update is called once per frame
	void Update () {
        if (parent.transform.childCount < maxNpc){
            Invoke("generatePeople", timeToNext);
        }
        
	}

    void generatePeople(){
        int rand = Random.Range(0, people.Length);
        pos = GetRandomPointOnMesh();
        GameObject person = Instantiate(Resources.Load(people[rand]), pos, transform.rotation) as GameObject;
        person.transform.parent = parent.transform;
        CancelInvoke("generatePeople");
    }

    Vector3 GetRandomPointOnMesh(){
        float maxWalkDistance = 50f;
        Vector3 direction = Random.insideUnitSphere * maxWalkDistance;
        Vector3 destination;
        direction.y = 0;
        direction += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(direction, out hit, maxWalkDistance, 1);
        destination = hit.position;
        return destination;
    }
}
