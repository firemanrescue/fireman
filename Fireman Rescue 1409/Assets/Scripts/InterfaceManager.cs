﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour {

    [SerializeField] public static GameObject menubutton;
	public enum action { CANCELAR, AGAFAR, DEIXAR, PARLAR, BONDIA, MARXAR, ACORDONAR, VOLCAR,
	UTILITZARDESTRAL, UTILITZARBOTIQUI, DESCONNECTAR, ACOMPANYAM, QUEHAPASAT, DONACLAUS, COMUNICAR, 
	COMUNICAR_INICI, COMUNICAR_EXTFOCS, COMUNICAR_FI, UTILITZARCLAU, ARROSEGAR, PRIMERSAUXILIS, HELPER_APAGAR,
    HELPER_EP, HELPER_PA, HELPER_ALTRES, HELPER_ENRERE, HELPER_ACEPTAR, HELPER_CANCELAR, COMUNICA_BOMBERS, DEIXAR_VICTIM}

	public static GameObject cam;
	public static GameObject contenidor;
    public static GameObject cotxe;
   // public static GameObject bateria;
	public static GameObject tapaContenidor;
	public static GameObject caixaPrimera;
	public static GameObject capo;
	private GUISkin guiSkin;    //color rojo
    private GUISkin guiSkin2;   //color verde
    private static bool pintaObj;
    public  bool pintaVolcar;
    public static int numBomber;
    public static GameObject manguera;
    public static GameObject medicine;
    public static GameObject axe;
	public static GameObject radio;
    public static GameObject cordo1;
    public static GameObject alicates;
    public static GameObject cordo2;
    public static GameObject ERA;
    public static Quaternion pospolicia;
    public static GameObject victim;
    public static GameObject victim2;   //
    public static Animator armsAnim;
    private GameObject progressBar;       //Canvas
    private static float tempsPinta;
    public static int ordreClick;
    public static int x;                //para que sirve la x ?¿
	public static bool cop = false;
    private static float timeLeft = 11f;
    private static bool poliactiu;
    public static bool temps = false;
    private bool mostrarMsgBateria = true;
    private bool mostrarMsgClaus = true;
    private static float tiempo;
	public static bool pirate;
    public Dictionary<string, AudioClip> clips;
    public static GameObject bomber;
    
    private AudioSource myAudio;
    public static bool movercoche;
	public static int numcops;
	public static bool agachado;
    public static bool tempsSenseComunicar;
    public static bool jaComunicat;
    public static float controlTemps;
    public static GameObject playerArms;    //arms05

    void Awake()
    {
        tempsSenseComunicar = false;
        jaComunicat = false;
		myAudio = GetComponent<AudioSource> ();
        AudioClip bonDia = Resources.Load("FX/BuenosDiasPolicia") as AudioClip;
        AudioClip marxaPolicia = Resources.Load("FX/MeVoyPolicia") as AudioClip;
        AudioClip acordonarZona = Resources.Load("FX/AcordonarZonaPoli") as AudioClip;
        AudioClip rebutComandament = Resources.Load("FX/rebutPerComandamentWalkie") as AudioClip;
        AudioClip endavantBomber1 = Resources.Load("FX/endavantBomber1Walkie") as AudioClip;
        AudioClip comandamentDemanant = Resources.Load("FX/CAMANDAMENT DEMANANT A SALA") as AudioClip;
        AudioClip situacioControlada = Resources.Load("FX/situacioControlada") as AudioClip;
        AudioClip infoSituacio = Resources.Load("FX/informacioSituacio") as AudioClip;
        AudioClip endavantPerSala = Resources.Load("FX/ENDAVANT PER SALA") as AudioClip;
        AudioClip rebutSalaControl = Resources.Load("FX/REBUT PER SALA") as AudioClip;
        AudioClip rebutb1 = Resources.Load("FX/REBUT BOMBER 1") as AudioClip;
        AudioClip rebutb2 = Resources.Load("FX/REBUT BOMBER 2") as AudioClip;
        AudioClip rebutb3 = Resources.Load("FX/REBUT BOMBER 3") as AudioClip;
        AudioClip rebutb4 = Resources.Load("FX/REBUT BOMBER 4") as AudioClip;


        clips = new Dictionary<string, AudioClip>();
        clips.Add("bonDia", bonDia);
        clips.Add("marxaPolicia", marxaPolicia);
        clips.Add("acordonarZona", acordonarZona);
        clips.Add("rebutComandament", rebutComandament);
        clips.Add("endavantBomber1", endavantBomber1);
        clips.Add("comandamentDemanant", comandamentDemanant);
        clips.Add("situacioControlada", situacioControlada);
        clips.Add("infoSituacio", infoSituacio);
        clips.Add("endavantPerSala", endavantPerSala);
        clips.Add("rebutSalaControl", rebutSalaControl);
        clips.Add("rebutb1", rebutb1);
        clips.Add("rebutb2", rebutb2);
        clips.Add("rebutb3", rebutb3);
        clips.Add("rebutb4", rebutb4);

        
        menubutton = Resources.Load("Buttonmenu") as GameObject;
        cam = GameObject.Find("FPSController");
        cotxe = GameObject.Find("Bateria");
        ERA = GameObject.Find("ERA");
        tapaContenidor = GameObject.Find("tapaContenidor");
        caixaPrimera = GameObject.Find("Brossa");
        bomber = GameObject.Find("Bomber");
        medicine = GameObject.Find("Botiquinmano");
        axe = GameObject.Find("Destralmano");
        alicates = GameObject.Find("Alicates");
        radio = GameObject.Find("Radio");
        capo = GameObject.Find("Capo");
        manguera = GameObject.Find("Lanza");
        cordo1 = GameObject.FindGameObjectWithTag("cordo1");
        cordo2 = GameObject.FindGameObjectWithTag("cordo2");
        victim = GameObject.Find("Victim");
        victim2 = GameObject.Find("Victim2");
        armsAnim = GameObject.Find("brazos03").GetComponent<Animator>();
        pospolicia = GameObject.Find("Policia").transform.rotation;
        guiSkin = Resources.Load("GUISkin") as GUISkin;
        guiSkin2= Resources.Load("GUISkin2") as GUISkin;
        playerArms = GameObject.Find("brazos03");
        progressBar = GameObject.Find("ProgressBar"); ///
        progressBar.SetActive(false);
        
    }

    // Use this for initialization
    void Start () {
		agachado = false;
		movercoche = false;
        manguera.SetActive (false);
        pintaObj = false;
		ordreClick = 0;
		poliactiu = true;
		pirate = false;
        tempsPinta = 5000 * Time.deltaTime;
        tiempo = 0;
		numcops = 2;
	}
	
	// Update is called once per frame
	void Update () {
        manguera.SetActive(SingletonGameObjectsHand.manguera);
        medicine.SetActive(SingletonGameObjectsHand.medicine);
        axe.SetActive(SingletonGameObjectsHand.axe);
		radio.SetActive (SingletonGameObjectsHand.radio);
        alicates.SetActive(SingletonGameObjectsHand.alicates);

        //Sino comunica durant un temps, la radio avisa
        if (controlTemps > 30 && controlTemps < 40 && !jaComunicat && !tempsSenseComunicar) {
            AudioSource.PlayClipAtPoint(clips["bomberNoComunica"], Camera.main.transform.position);
            jaComunicat = true;
        }

        if (controlTemps > 45 && jaComunicat && !tempsSenseComunicar) {
            AudioSource.PlayClipAtPoint(clips["bomberNoComunica"], Camera.main.transform.position);
            jaComunicat = false;
        }

        if (poliactiu == false) {
			Destroy (GameObject.Find ("Policia"));
			pirate = false;
			poliactiu = true;
		}
		if (pirate == true){
			tiempo += Time.deltaTime;
		}

		if (tiempo > 30) {
			poliactiu = false;
		}

        if (temps == true){
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0){
                cordo1.GetComponent<CordoPolicial>().activar = true;
                cordo2.GetComponent<CordoPolicial>().activar = true;
                temps = false;
                GameObject gent = GameObject.Find("Gente4");
                gent.gameObject.GetComponentInChildren<BoxCollider>().enabled = false;  ///añadido inChildren
            }
        }
    }

    /// <summary>
    /// Funcion para controlar cada vez qie clicamos un boton de los menus
    /// </summary>
    /// <param name="valueAction"></param>
    /// <param name="cam"></param>
    /// <param name="nom"></param>
    /// <param name="menucanvas"></param>
    /// <param name="obj_id"></param>
	public void CapturaBoto(int valueAction, GameObject cam,string nom,Canvas menucanvas, int obj_id)
    {
        Debug.Log("Action: "+valueAction+ " Nom: "+ nom+" obj:"+obj_id);
        menucanvas.enabled = false;
        cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(true);
        cam.gameObject.GetComponent<FirstPersonController>().enabled = true;
        GameManager.onMenu = false;
		GameObject o = GameObject.Find(nom);
        //Cuando  agafem un objecte del camió
        if (valueAction == (int)action.AGAFAR && o.GetComponent<Objecte> ().catched == false)       
        {
            if ((nom == "Botiquin" || nom == "Destral" || nom == "Manguera") && x < 2 || nom == "ERA")
            {
                o.GetComponent<Objecte>().catched = true;
                o.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                o.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
                x++;
				if (nom == "ERA") { o.GetComponent<BoxCollider>().enabled = false; x--;}
                if (nom == "Manguera")
                {
                    SingletonGameObjectsHand.manguera = true;
					SingletonGameObjectsHand.axe = false;
					SingletonGameObjectsHand.radio = false;
					SingletonGameObjectsHand.medicine = false;
                    SingletonGameObjectsHand.alicates = false;
                }
            }
            else if (x >= 2) pintaObj = true;
		}
		if (valueAction == (int)action.DEIXAR) {
            o.GetComponentInChildren<Victim>().arrosegar = false;
            o.GetComponent<Victim>().Leave();
            o.GetComponent<Victim>().setNmenu(0);
            if(o.name == "Victim")
            {
                playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
                cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed*2;
                
            }
            //
            if (o.name == "Victim2")
            {
                playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
                cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed * 2;

            }
        }
        //VOLCAR CONTENIDOR
		if (valueAction == (int)action.VOLCAR)
        {
            bomber.GetComponentInChildren<Bomber>().Moures();
            
        }
        //UTILIZAR DESTRAL
		if (valueAction == (int)action.UTILITZARDESTRAL && SingletonGameObjectsHand.axe)
        {
            armsAnim.Play("usingpalanca");
            manguera.SetActive(false);
            o.GetComponentInChildren<BoxCollider>().enabled = false;
            if(o.GetComponent<Objecte>().nom == "Porta")
            {
                //animacion destral 30 secs
                progressBar.SetActive(true);
                StartCoroutine(DestralPorta(o,obj_id));
            }
            if(o.GetComponent<Objecte>().nom == "Capo")
            {
                //yield return new WaitForSeconds(2f);
                //Abrir Capo
                o.GetComponentInParent<Animation>().Play();
                o.GetComponent<BoxCollider>().enabled = false;
                o.GetComponent<Objecte>().cursorP.SetActive(true);  //
                o.GetComponent<Objecte>().cursorH.SetActive(false); //
                o.GetComponent<Objecte>().cursorC.SetActive(false); //
                //GameManager.completeObjective(obj_id, false);       //linea de prueba
            }
        }

		if (valueAction == (int)action.PARLAR)
        {
            o.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, (cam.transform.rotation.eulerAngles.y - 180), 0));
            o.GetComponent<Huma>().setNmenu(1);
            if (o.gameObject.name == "Alertant")
            {
                o.GetComponent<Alertante>().adsourc.clip = o.GetComponent<Alertante>().clips[0];
                o.GetComponent<Alertante>().adsourc.Play();
            } else o.GetComponent<Objecte> ().createMenu ();
        }
		if (valueAction == (int)action.ACOMPANYAM)
        {
            o.GetComponent<Bomber>().setAcompaya(true);
        }
		if (valueAction == (int)action.BONDIA && o.GetComponent<Policia>().getNmenu() == 1)
		{
            o.GetComponent<Animator>().SetBool("parlar", true);
            o.GetComponent<Policia>().setNmenu(0);
            AudioSource.PlayClipAtPoint(clips["bonDia"], Camera.main.transform.position);
          
        }
		if (valueAction == (int)action.ACORDONAR && o.GetComponent<Policia>().getNmenu() == 1) {
            AudioSource.PlayClipAtPoint(clips["acordonarZona"], Camera.main.transform.position);
            GameManager.completeObjective(obj_id);
            o.GetComponent<Policia>().setNmenu(0);
            temps = true;

			o.GetComponent<Animator>().SetBool("parlar", false);
			o.GetComponent<Animator>().SetBool("marxar", true);
			o.gameObject.transform.rotation = pospolicia;
			pirate = true;
            o.gameObject.GetComponent<BoxCollider>().enabled = false;
		}
		if (valueAction == (int)action.MARXAR && o.GetComponent<Policia>().getNmenu() == 1) {
            AudioSource.PlayClipAtPoint(clips["marxaPolicia"], Camera.main.transform.position);
            o.GetComponent<Policia>().setNmenu(0);

			o.GetComponent<Animator>().SetBool("parlar", false);
			o.GetComponent<Animator>().SetBool("marxar", true);
			o.gameObject.transform.rotation = pospolicia;
            o.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
		if (valueAction == (int)action.DESCONNECTAR && SingletonGameObjectsHand.alicates) {
			o.GetComponent<BoxCollider>().enabled = false;
            o.GetComponentInChildren<Bateria>().conectada = false;
            GameManager.completeObjective(obj_id);
        }
		if (valueAction == (int)action.COMUNICAR) {
            tempsSenseComunicar = true;
            StopAllCoroutines();
            myAudio.Stop();
            StartCoroutine(demanarSalaControl());
            o.GetComponent<Radio>().setNmenu(1);
			o.GetComponent<Radio> ().createMenu ();
            o.GetComponent<Radio>().setNmenu(0);
        }
		if (valueAction == (int)action.COMUNICAR_INICI) {
            tempsSenseComunicar = true;
            //activar audio missatge.
            StopAllCoroutines();
            myAudio.Stop();
            StartCoroutine(comunicarInici());
            GameManager.completeObjective(obj_id, "comunicar_inici");
        }
		if (valueAction == (int)action.COMUNICAR_EXTFOCS) {
            //activar audio missatge.
            // AudioSource.PlayClipAtPoint(rebutComandament, Camera.main.transform.position);
            StopAllCoroutines();
            myAudio.Stop();
            //StartCoroutine(extincioFocs());
            GameManager.completeObjective(obj_id, "comunicar_meitat");
        }
		if (valueAction == (int)action.COMUNICAR_FI) {
            //activar audio
            StopAllCoroutines();
            myAudio.Stop();
            StartCoroutine(final());
            //mirar cuando acabe el audio.
            SingletonGameObjectsHand.radio = false;
            cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
			cam.gameObject.GetComponent<FirstPersonController>().enabled = false;
        }
        if(valueAction == (int)action.QUEHAPASAT)
        {
            o.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, (cam.transform.rotation.eulerAngles.y - 180), 0));
            o.GetComponent<Alertante>().setNmenu(2);
            o.GetComponent<Alertante>().alert();
        }

        if (valueAction == (int)action.DONACLAUS)
        {
            o.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, (cam.transform.rotation.eulerAngles.y - 180), 0));
            o.GetComponent<Alertante>().setNmenu(0);
            o.GetComponent<Alertante>().llave.catched = true;
            //o.GetComponent<Animator>().SetBool("hablado",true);         //cambiar animacion Alertante
            o.GetComponent<Animator>().Play("Idle");
        }
        if(valueAction == (int)action.UTILITZARCLAU)
        {
            //o.SetActive(false);
            o.GetComponent<Animation>().Play();
            o.GetComponent<BoxCollider>().enabled = false;
            o.GetComponent<Objecte>().cursorP.SetActive(true);
            o.GetComponent<Objecte>().cursorH.SetActive(false);
            o.GetComponent<Objecte>().cursorC.SetActive(false);
            GameManager.portaOberta = true;
            GameManager.completeObjective(obj_id,true);
        }
        if (valueAction == (int)action.ARROSEGAR)
        {
            //Para controlar las victimas de Nivel3
            if(o.name=="Victim")
            {
                GameManager.victimcatched = true;
            }
            else if(o.name == "Victim2")
            {
                GameManager.victim2catched = true;
            }
            //
            o.GetComponent<Victim>().arrosegar = true;
            //o.GetComponent<BoxCollider>().enabled = false;  ///quitar colider
            o.GetComponent<Victim>().setNmenu(1);
            playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = true;   ///Error
            cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed;

        }
        if (valueAction == (int)action.PRIMERSAUXILIS)
        {
            Debug.Log("curat");
        }

        if (valueAction == (int)action.DEIXAR_VICTIM)
        {
            //Para controlar las victimas de Nivel3
            if (GameManager.victimcatched == true)
            {
                victim.GetComponentInChildren<Victim>().arrosegar = false;
                victim.GetComponent<Victim>().Leave();
                victim.GetComponent<BoxCollider>().enabled = false;
                playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
                //cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed;
                //Controlamos en que tiempo rescatamos la victima
                if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 240.0f)
                {
                    GameManager.completeObjective(14, true);
                }
                else if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) > 240.0f && (GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 480.0f)
                {
                    GameManager.completeObjective(14, false);
                }
                GameManager.victimcatched = false;
            }
            else if (GameManager.victim2catched == true)
            {
                victim2.GetComponentInChildren<Victim>().arrosegar = false;
                victim2.GetComponent<Victim>().Leave();
                victim2.GetComponent<BoxCollider>().enabled = false;
                playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
                //cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed;
                //Controlamos en que tiempo rescatamos la victima
                if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 240.0f)
                {
                    GameManager.completeObjective(15, true);
                }
                else if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) > 240.0f && (GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 480.0f)
                {
                    GameManager.completeObjective(15, false);
                }
                GameManager.victim2catched = false;
            }
            else
            {
                victim.GetComponentInChildren<Victim>().arrosegar = false;
                victim.GetComponent<Victim>().Leave();

                playerArms.GetComponent<PlayerAnimations>().cantRunCauseObj = false;
                cam.GetComponent<FirstPersonController>().m_RunSpeed = cam.GetComponent<FirstPersonController>().m_WalkSpeed;
                //Controlamos en que tiempo rescatamos la victima
                if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 240.0f)
                {
                    GameManager.completeObjective(obj_id, true);
                }
                else if ((GameManager.getMaxSeconds() - GameManager.GetSeconds()) > 240.0f && (GameManager.getMaxSeconds() - GameManager.GetSeconds()) < 480.0f)
                {
                    GameManager.completeObjective(obj_id, false);
                }
            }
            //

            
            
        }

        if (valueAction == (int)action.HELPER_APAGAR)
        {
            o.GetComponent<Helper>().setNmenu(1);
            o.GetComponent<Helper>().createMenu();
        }
        if (valueAction == (int)action.HELPER_EP)
        {
            o.GetComponent<Helper>().setNmenu(2);
            o.GetComponent<Helper>().createMenu();
        }
        if (valueAction == (int)action.HELPER_PA)
        {
            o.GetComponent<Helper>().setNmenu(3);
            o.GetComponent<Helper>().createMenu();
        }
        if (valueAction == (int)action.HELPER_ALTRES)
        {
            o.GetComponent<Helper>().setNmenu(4);
            o.GetComponent<Helper>().createMenu();
        }
        if (valueAction == (int)action.HELPER_ENRERE)
        {
            o.GetComponent<Helper>().setNmenu(0);
            o.GetComponent<Helper>().createMenu();
        }
        if (valueAction == (int)action.HELPER_ACEPTAR)
        {
            Helper helper = o.GetComponent<Helper>();
            helper.setNmenu(0);
            if (helper.radioComm) {
                Radio radioComp = radio.GetComponent<Radio>();
                radioComp.StopAllCoroutines();
                radioComp.audio.Stop();
                myAudio.Stop();
                myAudio.PlayOneShot(clips["rebutb" + helper.id]);
                helper.radioComm = false;
                radioComp.setNmenu(0);
            }
            foreach (Accio a in helper.helper_actions) a.setSelected(false);
            helper.StartCoroutine(helper.startWorking());
            helper.clearSideMenu();
        }
        if (valueAction == (int)action.HELPER_CANCELAR)
        {
            Helper helper = o.GetComponent<Helper>();
            foreach (HelperAction a in helper.helper_actions) a.setSelected(false);
            helper.clearSideMenu();
            helper.setNmenu(0);
            if (helper.radioComm)
            {
                Radio radioComp = radio.GetComponent<Radio>();
                radioComp.StopAllCoroutines();
                radioComp.audio.Stop();
                helper.radioComm = false;
                radioComp.setNmenu(0);
            }
        }

        if(valueAction == (int)action.COMUNICA_BOMBERS)
        {
            o.GetComponent<Radio>().setNmenu(2);
            o.GetComponent<Radio>().createMenu();
        }

    }

    //Courutina para progressbar abrir puerta
    IEnumerator DestralPorta(GameObject o,int obj_id)
    {
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().enabled=false;
        armsAnim.SetBool("isUsingPalanca", true);
        yield return new WaitForSeconds(30f);
        armsAnim.SetBool("isUsingPalanca", false);
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().enabled = true;
        progressBar.SetActive(false);
        //Abrir puerta
        o.GetComponent<Animation>().Play();
        o.GetComponent<BoxCollider>().enabled = false;
        o.GetComponent<Objecte>().cursorP.SetActive(true);
        o.GetComponent<Objecte>().cursorH.SetActive(false);
        o.GetComponent<Objecte>().cursorC.SetActive(false);
        GameManager.completeObjective(obj_id, false);
        
    }

    //Comunicar extincio focs
    /*
     IEnumerator extincioFocs()
    {
        myAudio.PlayOneShot(clips[""]);
        yield return new WaitForSeconds(5.0f);
        myAudio.PlayOneShot(clips["rebutSalaControl"]);
        yield return new WaitForSeconds(3.0f);
    }
         
    */
    IEnumerator final()
    {
        myAudio.PlayOneShot(clips["situacioControlada"]);
        yield return new WaitForSeconds(5.0f);
        myAudio.PlayOneShot(clips["rebutSalaControl"]);
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("resum");
        GameManager.completeObjective(8, "comunicar_final");
    }

    IEnumerator demanarSalaControl()
    {
        myAudio.PlayOneShot(clips["comandamentDemanant"]);
        yield return new WaitForSeconds(5.0f);
        myAudio.PlayOneShot(clips["endavantPerSala"]);
    }

    IEnumerator comunicarInici()
    {
        myAudio.PlayOneShot(clips["infoSituacio"]);
        yield return new WaitForSeconds(4.0f);
        myAudio.PlayOneShot(clips["rebutSalaControl"]);
    }

    /* IEnumerator audioEspera()
     {
         AudioSource.PlayClipAtPoint(rebutComandament, Camera.main.transform.position);
         yield return new WaitForSeconds(200);
     }*/

    //Si quieres mostrar mensaje en pantalla aqui
    void OnGUI()
    {
        GUI.skin = guiSkin;
        if (pintaObj)
        {
            GUI.Label(new Rect(Screen.width / 4, Screen.height / 4, Screen.width, Screen.height), "No pots carregar més objectes");
            //Quita el mensaje a los 2 segundos
            Invoke("nopintaTime", 2f);
        }
        if (pintaVolcar)
        {
            GUI.Label(new Rect(Screen.width / 4, Screen.height / 4, Screen.width, Screen.height), "Necesites ajuda per a volcar el contenidor");
            Invoke("nopintaTime", 2f);
        }
        if (GameObject.Find("Bateria").GetComponentInChildren<Bateria>().conectada == false && mostrarMsgBateria)
        {
            GUI.skin = guiSkin2;
            GUI.Label(new Rect(Screen.width / 4, Screen.height / 4, Screen.width, Screen.height), "Bateria desconectada");
            Invoke("nopintaTime", 2f);
        }

        if (SceneManager.GetActiveScene().name == "accident2" || SceneManager.GetActiveScene().name == "accident3") { 
            if (GameObject.Find("Alertant").GetComponent<Alertante>().llave.catched == true && mostrarMsgClaus){
                GUI.skin = guiSkin2;
                GUI.Label(new Rect(Screen.width / 4, Screen.height / 4, Screen.width, Screen.height), "Has rebut les claus");
                Invoke("nopintaTime", 2f);
            }
        }
    }


    //Resetar bool e pintado para que deje de pintar
    void nopintaTime() {
        pintaObj = false;
        pintaVolcar = false;
        if(GameObject.Find("Bateria").GetComponentInChildren<Bateria>().conectada == false)
        {
            mostrarMsgBateria = false;
        }
        if (SceneManager.GetActiveScene().name == "accident2" || SceneManager.GetActiveScene().name == "accident3")
        {
            if (GameObject.Find("Alertant").GetComponent<Alertante>().llave.catched == true){
                mostrarMsgClaus = false;
            }
        }

    }
}

