﻿using UnityEngine;
using System.Collections;

public class Cotxe2 : MonoBehaviour {
	private Transform davant;
	private Transform darrera;
	private Vector3[] disparosray;
    private string[] directions;
	private Vector3 disparoray;
	private float distanciaray;
	private float distanciabomberos;
	private float stop;
	private GameObject persona;
	public GameObject cochebomberos;
	bool rotating = false;
	private bool continua;
	private float temps;
	private float temps2;
	private bool colisioncotxe;
	public float smoothTime; //rotate over 5 seconds
	public float velocity;
	private bool colisionando;
    public bool stopCar;
    public bool avoidCollision;
    private bool dobleDirection;
    private bool straight;
    //Position in the array directions of the direction of the car
    public int carDirection;

	void Start(){ 
		distanciabomberos = 0;
		distanciaray = 5;
		disparosray = new Vector3[]{ Vector3.forward, -Vector3.right, -Vector3.forward,  Vector3.right };
		persona = GameObject.FindGameObjectWithTag("MainCamera");
        cochebomberos = GameObject.Find("DotacioBombers");
        smoothTime = 1.0f;
		continua = true;
		temps = 0;
		temps2= 0;
		colisioncotxe = false;
		velocity = 8.0f;
		colisionando = false;
        stopCar = false;
        avoidCollision = false;

		davant = this.gameObject.transform.GetChild (1);
		darrera = this.gameObject.transform.GetChild (2);

        directions = new string[] { "up", "right", "down", "left" };
	}
	void Update(){
        /*left) {
            RaycastHit raycastHit;
            if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), Vector3.left, out raycastHit, 10)){
                Collider raycastHitCollider = raycastHit.collider;
                if(raycastHitCollider.gameObject.tag == "npc"){
                    npcCrossing = true;
                    coche.GetComponent<Cotxe2>().stopCar = true;
                }
                Debug.Log(raycastHitCollider.gameObject.tag);
            }*/
        //Poner los coches en el layer coche
        int layerMask = 1 << 13;
        RaycastHit hit;
        //Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), disparoray);
        //
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), disparoray * distanciaray, Color.red);
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), disparoray, out hit, 10, layerMask)){
            Collider raycastHitCollider = hit.collider;
        }
       
        this.GetComponent<Rigidbody>().MovePosition(transform.position + transform.forward * velocity * Time.deltaTime);
        if (stopCar){
            davant.GetComponent<RotacioRodes>().enabled = false;
            darrera.GetComponent<RotacioRodes>().enabled = false;
            velocity = 0.0f;
        }
        else{
            davant.GetComponent<RotacioRodes>().enabled = true;
            darrera.GetComponent<RotacioRodes>().enabled = true;
            velocity = 7.0f;
        }

        if (InterfaceManager.movercoche == false) {
            distanciabomberos = Vector3.Distance(this.transform.position, cochebomberos.transform.position);
            if (distanciabomberos < 20)
            {
                davant.GetComponent<RotacioRodes>().enabled = false;
                darrera.GetComponent<RotacioRodes>().enabled = false;
                velocity = 0.0f;
            }
            else
            {
                davant.GetComponent<RotacioRodes>().enabled = true;
                darrera.GetComponent<RotacioRodes>().enabled = true;
                velocity = 7.0f;
            }
        }
        //temps += Time.deltaTime;
        /*if (InterfaceManager.movercoche == false) {
			distanciabomberos = Vector3.Distance (this.transform.position, cochebomberos.transform.position);
			if (distanciabomberos < 20) {
				davant.GetComponent<RotacioRodes> ().enabled = false;
				darrera.GetComponent<RotacioRodes> ().enabled = false;
				velocity = 0.0f;
			} else {
				davant.GetComponent<RotacioRodes> ().enabled = true;
				darrera.GetComponent<RotacioRodes> ().enabled = true;
				velocity = 7.0f;
			}
		}

		/*stop = Vector3.Distance (this.transform.position, persona.transform.position);
		if (stop > 4f && continua == true) {
			this.GetComponent<Rigidbody> ().MovePosition (transform.position + transform.forward * velocity * Time.deltaTime);
			if (velocity == 7.0f) {
				davant.GetComponent<RotacioRodes> ().enabled = true;
				darrera.GetComponent<RotacioRodes> ().enabled = true;
			}
		} else if (stop > 4f && continua == false && temps > 4) {
			continua = true;
			colisioncotxe = true;
		} else if (stop < 4f) {
			davant.GetComponent<RotacioRodes> ().enabled = false;
			darrera.GetComponent<RotacioRodes> ().enabled = false;
			continua = false;
			temps = 0;
		}

		if (stop < 4f) {
			if (colisionando == true) {
				InterfaceManager.numcops--;
				colisionando = false;
			}
		}else{
			colisionando = true;
		}*/



        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), disparoray, out hit, 10, layerMask)) {
			if (hit.collider.tag == "cotxe1") {
				velocity = 0.0f;
				davant.GetComponent<RotacioRodes> ().enabled = false;
				darrera.GetComponent<RotacioRodes> ().enabled = false;
			}
		} else if (velocity == 0.0f) {
			temps2 += Time.deltaTime;
			if (temps2 > 2) {
				velocity = 7.0f;
				temps2 = 0;
			}
		} else {
			velocity = 7.0f;
		}

    }

    void OnTriggerEnter(Collider col){
        switch (col.gameObject.tag) {
		case "recta32":
			disparoray = disparosray [0];
			break;
		case "recta42":
			disparoray = disparosray [1];
			break;
		case "recta12":
			disparoray = disparosray [2];
			break;
		case "recta22":
			disparoray = disparosray [3];
			break;
		case "curva2":
            dobleDirection = col.gameObject.GetComponent<dobleDirection>().twoDirection;
            straight = col.gameObject.GetComponent<dobleDirection>().dontTurn;
            rotating = true;
            int random = 1;
            if (col.gameObject.GetComponent<dobleDirection>().right){
               random = 1;
            }
            else{
               random = 2;
            }
            if (dobleDirection && !straight){
               random = Random.Range(1, 3);
            }
            else if(dobleDirection && straight){
                random = Random.Range(1, 4);
            }
            else if(!dobleDirection && straight){
                int aux = Random.Range(1, 3);
                if(aux == 1){
                    if (col.gameObject.GetComponent<dobleDirection>().right) {
                        random = 1;
                    }
                    else if (col.gameObject.GetComponent<dobleDirection>().left) {
                        random = 2;
                    }
                    else {
                        random = 3;
                    }
                }
                else{
                    random = 3;
                }
            }
            if (random == 1){
                //RotateCar(transform.localEulerAngles.y, transform.localEulerAngles.y + 90, smoothTime);
                StartCoroutine(RotateOverTime(transform.localEulerAngles.y, transform.localEulerAngles.y + (+90), smoothTime));
                if(carDirection < (directions.Length - 1)){
                    carDirection++;
                }
                else{
                    carDirection = 0;
                }
            }
            else if (random == 2) {
                float i = 0;
                float initialPosition = 0;
                float finalPosition = 0;
                switch (directions[carDirection]){
                    case "up":
                        initialPosition = transform.position.z;
                        finalPosition = transform.position.z + 4.5f;
                        while(i <= 1.1f){
                            i += Time.deltaTime / smoothTime;
                            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(initialPosition, finalPosition, i));
                        }
                        break;
                    case "down":
                        initialPosition = transform.position.z;
                        finalPosition = transform.position.z - 4.5f;
                        while (i <= 1.1f){
                            i += Time.deltaTime / smoothTime;
                            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(initialPosition, finalPosition, i));
                        }
                        break;
                    case "right":
                        initialPosition = transform.position.x;
                        finalPosition = transform.position.x + 4.5f;
                        while (i <= 1.1f){
                            i += Time.deltaTime / smoothTime;
                            transform.position = new Vector3(Mathf.Lerp(initialPosition, finalPosition, i), transform.position.y, transform.position.z);
                        }
                        break;

                    case "left":
                        initialPosition = transform.position.x;
                        finalPosition = transform.position.x - 4.5f;
                        while (i <= 1.1f){
                            i += Time.deltaTime / smoothTime;
                            transform.position = new Vector3(Mathf.Lerp(initialPosition, finalPosition, i), transform.position.y, transform.position.z);
                        }
                        break;
                    default:
                        break;
                }
                //StartCoroutine(GoStraight(transform.position.z, transform.position.z + 10f, smoothTime));
                //RotateCar(transform.localEulerAngles.y, transform.localEulerAngles.y - 90, smoothTime);
                StartCoroutine(RotateOverTime(transform.localEulerAngles.y, transform.localEulerAngles.y + (-90), smoothTime));
                if (carDirection > 0){
                    carDirection--;
                }
                else{
                    carDirection = directions.Length - 1;
                }
            }
			break;
        case "curvaPequeña":
            StartCoroutine(RotateOverTime(transform.localEulerAngles.y, 194, smoothTime));
            //RotateCar(transform.localEulerAngles.y, 194, smoothTime);
            /*switch (directions[carDirection]) {
                case "down":
                    StartCoroutine(RotateOverTime(transform.localEulerAngles.y, transform.localEulerAngles.y + 14, smoothTime));
                    break;
                default:
                    break;
            }*/
            break;
        default:
			break;
        } 
    }

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "pj"){
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		} 
	}

	void OnCollisionExit(Collision other){
		if (other.gameObject.tag == "pj"){
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
		}

	}

    /*void RotateCar(float originalAngle, float finalAngle, float overTime) {
        float partOfAngle = finalAngle / 10;
        float actualAngle = partOfAngle;
        float timer = 1.1f;
        while (transform.localEulerAngles.y < finalAngle) {
            Debug.Log(timer);
            if (timer > 20f) { 
                if(transform.localEulerAngles.y < finalAngle){
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y - actualAngle, transform.localEulerAngles.z);
                }
                else{
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + actualAngle, transform.localEulerAngles.z);
                }
                timer = 0;
            }
            else{
                timer += Time.deltaTime;
            }
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, finalAngle, transform.localEulerAngles.z);
    }*/

	IEnumerator RotateOverTime(float currentRotation, float desiredRotation, float overTime) { 
		float i = 0.0f;
		while (i <= 1.1f){
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, Mathf.Lerp(currentRotation, desiredRotation, i), transform.localEulerAngles.z);
			i += Time.deltaTime / overTime;
			yield return null;
		}
		yield return new WaitForSeconds(overTime);
		rotating = false; // no longer rotating
	}

    /*IEnumerator GoStraight(float initialPosition, float finalPosition, float overTime) {
        float i = 0;
        while(i <= 1.1f){
            i += Time.deltaTime / overTime;
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(initialPosition, finalPosition, i));
            yield return null;
        }
        yield return new WaitForSeconds(overTime);
    }*/
}