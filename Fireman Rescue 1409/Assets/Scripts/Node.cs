﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node
{
    public int id;
    private List<Node> fills;
    private List<int> costos;
    private List<Node> requisits;
    private int objectiu;
    int action;                 //No se usa
    string comment;

    public Node(int i)
    {
        id = i;
        fills = new List<Node>();
        costos = new List<int>();
        requisits = new List<Node>();
        comment = "";
    }

    public void setObjectiu(int o)
    {
        objectiu = o;
    }

    public Node AddChild(int k, int cost)
    {
        Node n = new Node(k);
        fills.Add(n);
        costos.Add(cost);
        return n;
    }

    public void AddChild(Node n, int cost)
    {
        fills.Add(n);
        costos.Add(cost);
    }

    public void AddChild(Node n)
    {
        fills.Add(n);
    }

    public void printChildren()
    {
        foreach (Node n in fills)
        {
            Debug.Log(n.id+" " + n.comment);
        }
    }

    public void AddRequisit(Node n)
    {
        requisits.Add(n);
    }

    /// <summary>
    /// Función para asignar datos al node nout y idbonj del nodo con id "k"
    /// </summary>
    /// <param name="k">Id del nodo</param>
    /// <param name="nout">Nodo encontrado</param>
    /// <param name="idobj">coste del nodo encontrado</param>
    public void lookForChild(int k, out Node nout, out int idobj)           
    {
        int idc = 0;
        Node aux = null;
        int auxi = 0;
        foreach (Node n in fills)
        {
            if (n.id == k)
            {
                aux = n;
                auxi = idc;
            }
            else idc++;
        }
        nout = aux;
        idobj = costos[auxi];
    }

    public int getObjective()
    {
        return objectiu;
    }

    public string getComment()
    {
        return comment;
    }

    public bool hasChild(int id)
    {
        foreach (Node n in fills)
        {
            if (n.id == id) return true;
        }
        return false;
    }

    public Node getChild(int id)
    {
        foreach (Node n in fills)
        {
            if (n.id == id) return n;
        }
        return null;
    }

    public bool isAvailable()
    {
        int total_requisits = requisits.Count;
        int contador_requisits = 0;

        foreach (Node n in requisits)
        {
            foreach (int i in GameManager.nodes_visitats)
            {
                if (n.id == i) contador_requisits++;
            }
        }
        if (contador_requisits == total_requisits) return true;
        else return false;
    }

    public int getObjectiveNode(string name)
    {
        foreach (Node n in fills)
        {
            if (GameManager.ar_objectius[n.objectiu].getNom() == name) return n.id;
        }
        return 0;
    }

    
    public void setAction(int a)
    {
        action = a;
    }
    
    public void setComment(string c)
    {
        comment = c;
    }

    /// <summary>
    /// Usat per Helpers per saber si poden començar l'accio o s'han d'esperar a la seva completacio.
    /// </summary>
    /// <returns></returns>
	public bool isAvailableToComplete()
    {
        foreach (int i in GameManager.nodes_visitats) if (i == id) return false;
        int total_requisits = requisits.Count;
        int requisits_count = 0;
        if (total_requisits > 0)
        {
            foreach (Node n in requisits)
            {
                foreach (int i in GameManager.nodes_visitats)
                {
                    if (n.id == i) requisits_count++;
                    if (i == id) return false;
                }
            }
        }

        if (requisits_count == total_requisits) return true;
        else return false;

    }
}