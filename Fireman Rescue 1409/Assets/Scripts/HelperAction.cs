﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class HelperAction : Accio {

	protected bool selected, active;
	protected int section, total_targets;
	protected string[] dependencies;
	protected Helper helper;
	protected GameObject[] targets;
	protected float maxDistance;
	public string clip;

	public HelperAction() : base() {
        dependencies = new string[0];
    }

	public bool isSelected()
	{
		return selected;
	}
	public int getSection()
	{
		return section;
	}
    public void setSection(int s)
    {
        section = s;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="n"></param>
    /// <param name="deps"></param>
    /// <param name="obj"></param>
    /// <param name="actionVal"></param>
    /// <param name="help"></param>
    /// <param name="targ"></param>
    /// <param name="req"></param>
    public HelperAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, string req) : base(n, actionVal, obj, req)
	{
		dependencies = deps;
		selected = false;
		helper = help;
		total_targets = targ.Length;

		targets = new GameObject[total_targets];
		for (int i = 0; i < total_targets; i++)
		{
			targets[i] = GameObject.Find(targ[i]);
		}
	}

	public void setNode(int n)
	{
		node = n;
	}

	protected IEnumerator comunicarRealitzacio()
	{
		if (clip != null) {
			helper.radio.audio.PlayOneShot (helper.radio.clips ["bomber1demanant"]);
			yield return new WaitForSeconds (3);
			helper.radio.audio.PlayOneShot (helper.radio.clips ["endavantcomandament"]);
			yield return new WaitForSeconds (3);
			helper.radio.audio.PlayOneShot (helper.radio.clips [clip]);
			yield return new WaitForSeconds (3);
			helper.radio.audio.PlayOneShot (helper.radio.clips ["rebutcomandament"]);
			yield return new WaitForSeconds (3);
		}
		yield return null;
	}

	public override bool isAvailable(int menu_actual)
	{
		if(section == menu_actual || (section == -1 && menu_actual != 0) || (section == -2))
		{
			if (meetsDependencies())
			{
				if (node != 0)
				{
					bool completed = false;
					foreach(int action_node in GameManager.nodes_visitats)
					{
						if (action_node == node) completed = true;
					}
					if (!active && !completed) return true;
				}
				else return true;
			}                       
		}    
		return false;
	}

	public int getNode()
	{
		return node;
	}
	public bool meetsDependencies()
	{
		int total_dependencies = dependencies.Length;
		int completed_dependencies = 0;

		foreach (string d in dependencies)
		{
			bool ok = false;

			foreach (HelperAction a in Helper.helper_actionsStatic)
			{
				if (d == a.nom)
				{
					int node = a.node;
					foreach (int completed_node in GameManager.nodes_visitats)
					{
						if (node == completed_node) ok = true;
					}
					if (a.isSelected()) ok = true;
				}               
			}			
			if (ok) completed_dependencies++;
		}
		if (completed_dependencies == total_dependencies) return true;
		else return false;
	}

	public override bool getSelected()
	{
		return selected;
	}

	public override void setSelected(bool s) { selected = s; }

	public override bool hasDependency(string depN)
	{
		foreach(string str in dependencies)
		{
			if (str == depN)
			{
				return true;
			}
		}
		return false;
	}

	public virtual IEnumerator startAction()
	{
        for (int i = 0; i < total_targets; i++)
        {
            if (this.nom == "portaDestral") {   //Controlar que una vez abierta la puerta no vaya denuevo
                if(GameManager.portaOberta){
                    break; 
                }
                else yield return helper.StartCoroutine(moveToActionSpot(i));
            }
            else yield return helper.StartCoroutine(moveToActionSpot(i));

            if (node != -1){
                int temp_obj;
                Node next_node;
                GameManager.i_node.lookForChild(node, out next_node, out temp_obj);

                if (next_node != null && next_node.isAvailableToComplete() )      //Aqui salta un error "NullReferenceException" //añadido:&& next_node!=null
                {
                    yield return helper.StartCoroutine(prepareForAction(i));
                    yield return helper.StartCoroutine(doAction(i));
                    if (next_node.getObjective() == -1) GameManager.nodes_visitats.Add(node);
                }
                else break;
            }
            else
            {
                yield return helper.StartCoroutine(prepareForAction(i));
                yield return helper.StartCoroutine(doAction(i));
            } 
		}
		yield return helper.StartCoroutine(comunicarRealitzacio());
	}

    public virtual IEnumerator doAction(int i)
    {
        throw new NotImplementedException();
    }
    public virtual IEnumerator doAction(int i, Node next_node) {
		throw new NotImplementedException();
	}

	public virtual IEnumerator prepareForAction(int i){
		throw new NotImplementedException();
	}

	public virtual IEnumerator moveToActionSpot(int i){
        Debug.Log("Target: " + targets[i].name);
		Vector3 pos = targets[i].transform.position;
		helper.agent.SetDestination(pos);
		helper.anim.SetBool("caminar", true);

        while (true)
		{
			if (!helper.agent.pathPending)
			{
				if (helper.agent.remainingDistance < maxDistance)
				{
					break;
				}
			}
			yield return new WaitForSeconds(0.1f);
		}
		helper.agent.Stop();
		helper.agent.ResetPath();
		helper.anim.SetBool("caminar", false);
        helper.anim.SetBool("volcar", false); /////////añadido pruebas
    }

	protected IEnumerator rotateToTarget(Transform target)
	{
		float elapsedTime = 0.0f;
		float time = 0.35f;

		Quaternion targetRotation = Quaternion.LookRotation(target.position - helper.transform.position);
		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime; // <- move elapsedTime increment here
			helper.transform.rotation = Quaternion.Slerp(helper.transform.rotation, targetRotation, (elapsedTime / time));
			yield return new WaitForEndOfFrame();
		}
	}

	protected IEnumerator rotate(Quaternion rotation)
	{
		float elapsedTime = 0.0f;
		float time = 0.35f;

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime; // <- move elapsedTime increment here
			helper.transform.rotation = Quaternion.Slerp(helper.transform.rotation, rotation, (elapsedTime / time));
			yield return new WaitForEndOfFrame();
		}
	}
}

public class HelperFireAction: HelperAction{

	Foc[] targetFire;

	public HelperFireAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, string req, string[] fire) : base(n, deps, obj, actionVal, help, targ, req)
	{
		targetFire = new Foc[fire.Length];
		for (int x = 0; x<fire.Length; x++) {
			targetFire [x] = GameObject.Find(fire[x]).GetComponent<Foc> ();
		}
		maxDistance = 1f;
	}

	/*  public override IEnumerator moveToActionSpot(int i)
    {
        if (nom.Equals("Brossa"))
        {
            Vector3 pos = new Vector3(3.68f, -37.783f, 18.87f);
            helper.agent.SetDestination(pos);
            helper.anim.SetBool("caminar", true);
            while (true)
            {
                if (!helper.agent.pathPending)
                {
                    if (helper.agent.remainingDistance <= 0.5f)
                    {
                        break;
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
            helper.agent.Stop();
            helper.agent.ResetPath();
            helper.anim.SetBool("caminar", false);
        }
        else
        {
            yield return helper.StartCoroutine(base.moveToActionSpot(i));
        }
        yield return new WaitForSeconds(0.1f);
    }*/

	public override IEnumerator doAction(int i)
	{
		helper.items[0].SetActive(true);
		while (!targetFire[i].getApagat())
		{
			yield return new WaitForSeconds(0.1f);
			targetFire[i].apagarFocRapid();
		}
		if (i == targetFire.Length-1)
		{
			helper.items[0].SetActive(false);
			while (true)
			{
				helper.anim.SetFloat("animation_blend", helper.anim.GetFloat("animation_blend") - 0.1f);
				if (helper.anim.GetFloat("animation_blend") <= 0.0)
				{
					helper.anim.SetFloat("animation_blend", 0.0f);
					break;
				}
				yield return new WaitForSeconds(0.02f);
			}
			helper.anim.Play("HumanoidIdle");
		}
	}

	public override IEnumerator prepareForAction(int i)
	{
		yield return helper.StartCoroutine(rotateToTarget(targetFire[i].gameObject.transform));

		if (nom.Equals("Brossa"))
		{
			Quaternion finalRotation = Quaternion.Euler(0, 20f, 0) * helper.transform.rotation;
			yield return helper.StartCoroutine(rotate(finalRotation));
			helper.anim.Play("crouch_idle");
		}
		else
		{
			Quaternion finalRotation = Quaternion.Euler(0, 35f, 0) * helper.transform.rotation;
			yield return helper.StartCoroutine(rotate(finalRotation));
			helper.anim.Play("idle_to_water");
		}
		while (true)
		{
			helper.anim.SetFloat("animation_blend", helper.anim.GetFloat("animation_blend") + 0.1f);        
			if (helper.anim.GetFloat("animation_blend") >= 0.8) break;
			yield return new WaitForSeconds(0.02f);
		}        
	}

}

public class HelperCommunicationAction : HelperAction
{
	float communicationTime;

	public HelperCommunicationAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, float time, string req) : base(n, deps, obj, actionVal, help, targ, req)
	{
		communicationTime = time;
		maxDistance = 2f;
	}

	public override IEnumerator doAction(int i)
	{
		yield return new WaitForSeconds(communicationTime);
		if (requisit != null) GameManager.completeObjective(node, requisit);
		else GameManager.completeObjective(node);
		if (targets[i].name == "Policia") {
			targets[i].GetComponent<Animator>().SetBool("marxar", true);
			InterfaceManager.pirate = true;
			InterfaceManager.temps = true;
		}
		while (true)
		{
			helper.anim.SetFloat("animation_blend", helper.anim.GetFloat("animation_blend") - 0.1f);
			if (helper.anim.GetFloat("animation_blend") <= 0.0)
			{
				helper.anim.SetFloat("animation_blend", 0.0f);
				break;
			}
			yield return new WaitForSeconds(0.02f);
		}
		helper.anim.Play("HumanoidIdle");
		if (requisit == "comunicar_final")
		{
			SceneManager.LoadScene("resum");
		}
	}

	public override IEnumerator prepareForAction(int i)
	{
		if(targets[i].name != "Helper") yield return helper.StartCoroutine(rotateToTarget(targets[i].transform));
		helper.anim.Play("talkingHumanoid");
		helper.anim.SetFloat("animation_blend", 1);
		yield return null;
	}

}

public class HelperOtherAction : HelperAction
{

	public HelperOtherAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, string req) : base(n, deps, obj, actionVal, help, targ, req)
	{
		maxDistance = 0.2f;
	}

	public override IEnumerator doAction(int i)
	{
		switch (nom)
		{
		case "Volcar Contenidor":
			{
				helper.anim.SetBool("volcar", true);
				yield return new WaitForSeconds(3f);
                    BoxCollider[] cols = GameObject.Find("Contenidor").GetComponents<BoxCollider>();
                    cols[0].enabled = false;
                    helper.anim.SetBool("volcar", false);
			}
			break;
		case "Obrir Capó":
			{
				    yield return new WaitForSeconds(1f);
                    GameObject.Find("Capo").GetComponent<Animation>().Play();
				    yield return new WaitForSeconds(3f);
			}
			break;
		case "Desconnectar Bateria":
			{
				GameManager.completeObjective(node);
				targets[i].GetComponent<BoxCollider>().enabled = false;
				yield return new WaitForSeconds(3f);
			}
			break;
        case "Alertant":
            {
                yield return new WaitForSeconds(0.5f);
                //helper.anim.Play("talkingHumanoid");
                helper.anim.SetBool("talking", true);
                yield return new WaitForSeconds(2f);
                GameObject llave = GameObject.Find("Alertant");
                llave.GetComponent<Alertante>().llave.catched = true;
                helper.anim.SetBool("talking", false);

                }
            break;
        case "Porta":   //Para cuando helper pide llaves a Alertant
            {
                if(!GameManager.portaOberta) //Si la porta no se ha abierto
                {
                    yield return new WaitForSeconds(0.5f);
                    helper.anim.SetBool("talking", true);
                    yield return new WaitForSeconds(1f);
                    helper.anim.SetBool("talking", false);
                    GameObject o = GameObject.Find("Porta");
                    o.GetComponent<Animation>().Play();
                    o.GetComponent<BoxCollider>().enabled = false;
                    o.GetComponent<Objecte>().cursorP.SetActive(true);
                    o.GetComponent<Objecte>().cursorH.SetActive(false);
                    o.GetComponent<Objecte>().cursorC.SetActive(false);
                        //GameManager.completeObjective(12, true);    //12 es el obj_id correspondiente a la Porta  *hardcode*
                        GameManager.portaOberta = true;
                }
            }
            break;
        case "portaDestral":            //Helper abre puerta con destral
            {
                if (!GameManager.portaOberta) 
                {
                    //animacion de golpear puerta
                    yield return new WaitForSeconds(30f);    //Segundos puerta
                    //parar animacion
                    GameObject o = GameObject.Find("Porta");
                    o.GetComponent<Animation>().Play();
                    o.GetComponent<BoxCollider>().enabled = false;
                    o.GetComponent<Objecte>().cursorP.SetActive(true);
                    o.GetComponent<Objecte>().cursorH.SetActive(false);
                    o.GetComponent<Objecte>().cursorC.SetActive(false);
                   // if (next_node.getObjective() == -1) GameManager.nodes_visitats.Add(node);
                    GameManager.completeObjective(12, false);  ////verificar si en vez de true deberia ser false
                    GameManager.portaOberta = true;
                }
            }
            break;
        case "trobarVictima1":
            {
                    Debug.Log("found");
                    //Comunicacio Radio
                    clip="trobatvictima";
                    comunicarRealitzacio();
                    //animacion agafar victima
                    yield return new WaitForSeconds(1f);
                    //parar animacion;
                    GameObject v = GameObject.Find("Victim");
                    v.GetComponent<Victim>().arrosegaHelper = true;
                    v.GetComponent<BoxCollider>().enabled = false;  ///quitar colider
                    v.GetComponent<Victim>().setNmenu(1);
                    v.transform.rotation = Quaternion.Euler(v.transform.eulerAngles.x,helper.transform.eulerAngles.y,v.transform.eulerAngles.z);
                    //v.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(helper.transform.position);
                    helper.agent.speed=0.4f;

             }
            break;
        case "rescatarVictima1":
            {
                //Comunicacio Radio
                clip = "victimaambulancia";
                comunicarRealitzacio();

                GameObject v = GameObject.Find("Victim");
                v.GetComponentInChildren<Victim>().arrosegaHelper = false;
                v.GetComponent<Victim>().setNmenu(0);
                helper.agent.speed = 1.2f;
                //comprobar estado salud en función del tiempo
                float tiempoact = GameManager.getMaxSeconds() - GameManager.GetSeconds();
                if (tiempoact < 240.0f)
                {
                    GameManager.completeObjective(14, true);
                }
                else if (tiempoact > 240.0f && tiempoact < 480.0f)
                {
                    GameManager.completeObjective(14, false);
                }
                    Debug.Log("saved");
            }
            break;
            case "trobarVictima2":
                {
                  
                    //Comunicacio Radio
                    clip = "trobatvictima2";
                    comunicarRealitzacio();
                    //animacion agafar victima
                    yield return new WaitForSeconds(1f);
                    //parar animacion;
                    GameObject v = GameObject.Find("Victim2");
                    v.GetComponent<Victim>().arrosegaHelper = true;
                    v.GetComponent<BoxCollider>().enabled = false;  ///quitar colider
                    v.GetComponent<Victim>().setNmenu(1);
                    v.transform.rotation = Quaternion.Euler(v.transform.eulerAngles.x, helper.transform.eulerAngles.y, v.transform.eulerAngles.z);
                    //v.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(helper.transform.position);
                    helper.agent.speed = 0.4f;

                }
            break;
            case "rescatarVictima2":
                {
                    //Comunicacio Radio
                    clip = "victimaambulancia";
                    comunicarRealitzacio();

                    GameObject v = GameObject.Find("Victim2");
                    v.GetComponentInChildren<Victim>().arrosegaHelper = false;
                    v.GetComponent<Victim>().setNmenu(0);
                    helper.agent.speed = 1.2f;
                    //comprobar estado salud en función del tiempo
                    float tiempoact = GameManager.getMaxSeconds() - GameManager.GetSeconds();
                    if (tiempoact < 240.0f)
                    {
                        GameManager.completeObjective(15, true);
                    }
                    else if (tiempoact > 240.0f && tiempoact < 480.0f)
                    {
                        GameManager.completeObjective(15, false);
                    }
                    GameManager.victim2placed = true;

                }
                break;
            //REVISAR ESCALAS
            case "anarescalas1":
                {
                    if(!GameManager.victim2placed)
                        yield return helper.StartCoroutine(anarEscalas1());
                }
                break;
            case "anarescalas2":
                {
                    if (!GameManager.victim2placed)
                        yield return helper.StartCoroutine(anarEscalas2());
                }
                break;
            case "anarescalas3":
                {
                    if (!GameManager.victim2placed)
                        yield return helper.StartCoroutine(anarEscalas3());

                    //Radio comunicació ha revisat escales
                }
                break;
        }
	}

	public override IEnumerator prepareForAction(int i)
	{
		if (nom.Equals("Volcar Contenidor"))
		{
			GameObject tapaContenidor = GameObject.Find("tapaContenidor");
			tapaContenidor.GetComponentInParent<BoxCollider>().enabled = true;
			yield return helper.StartCoroutine(rotateToTarget(tapaContenidor.transform));
		}
        if (nom.Equals("Porta"))
        {
            GameObject porta = GameObject.Find("Porta");
            //porta.GetComponentInParent<BoxCollider>().enabled = true;
            yield return helper.StartCoroutine(rotateToTarget(porta.transform));
        }
        if (nom.Equals("portaDestral"))
        {
            GameObject porta = GameObject.Find("Porta");
            //porta.GetComponentInParent<BoxCollider>().enabled = true;
            yield return helper.StartCoroutine(rotateToTarget(porta.transform));
        }
        if (nom.Equals("trobarVictima1"))
        {
            GameObject victim = GameObject.Find("Victim");
            //porta.GetComponentInParent<BoxCollider>().enabled = true;
            yield return helper.StartCoroutine(rotateToTarget(victim.transform));
        }
        if (nom.Equals("trobarVictima2"))
        {
            GameObject victim2 = GameObject.Find("Victim2");
            //porta.GetComponentInParent<BoxCollider>().enabled = true;
            yield return helper.StartCoroutine(rotateToTarget(victim2.transform));
        }

        yield return new WaitForSeconds(0.1f);
	}

    //METODOS
    private IEnumerator anarEscalas1()
    {
        if (!GameManager.victim2placed)
        {
            GameObject v = GameObject.Find("Victim2");
            if (v.GetComponent<Victim>().posicio2 == Victim.Posicio2.PLANTA1)
            {
                //Comunicacio Radio
                clip = "trobatvictimaEscales";
                comunicarRealitzacio();
                //Agafar Victima2
                v.GetComponent<Victim>().arrosegaHelper = true;
                v.GetComponent<BoxCollider>().enabled = false;
                v.GetComponent<Victim>().setNmenu(1);
                v.transform.rotation = Quaternion.Euler(v.transform.eulerAngles.x, helper.transform.eulerAngles.y, v.transform.eulerAngles.z);
                helper.agent.speed = 0.4f;
                ////Portar victima a la Doctora
                Vector3 pos = GameObject.Find("RescatVictimaPos").transform.position;
                helper.agent.SetDestination(pos);
                helper.anim.SetBool("caminar", true);
                while (true)
                {
                    if (!helper.agent.pathPending)
                    {
                        if (helper.agent.remainingDistance < maxDistance)
                        {
                            break;
                        }
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                helper.agent.Stop();
                helper.agent.ResetPath();
                helper.anim.SetBool("caminar", false);
                helper.anim.SetBool("volcar", false);
                //Entregar victima
                //Comunicacio Radio
                clip = "victimaambulancia";
                comunicarRealitzacio();
                v.GetComponentInChildren<Victim>().arrosegaHelper = false;
                v.GetComponent<Victim>().setNmenu(0);
                helper.agent.speed = 1.2f;
                //comprobar estado salud en función del tiempo
                float tiempoact = GameManager.getMaxSeconds() - GameManager.GetSeconds();
                if (tiempoact < 240.0f)
                {
                    GameManager.completeObjective(15, true);
                }
                else if (tiempoact > 240.0f && tiempoact < 480.0f)
                {
                    GameManager.completeObjective(15, false);
                }

            }//Fin if
        }//Fin if
        yield return new WaitForSeconds(0.01f);
    }
    private IEnumerator anarEscalas2()
    {
        if (!GameManager.victim2placed)
        {
            GameObject v = GameObject.Find("Victim2");
            if (v.GetComponent<Victim>().posicio2 == Victim.Posicio2.PLANTA2)
            {
                //Comunicacio Radio
                clip = "trobatvictimaEscales";
                comunicarRealitzacio();
                //Agafar Victima2
                v.GetComponent<Victim>().arrosegaHelper = true;
                v.GetComponent<BoxCollider>().enabled = false;
                v.GetComponent<Victim>().setNmenu(1);
                v.transform.rotation = Quaternion.Euler(v.transform.eulerAngles.x, helper.transform.eulerAngles.y, v.transform.eulerAngles.z);
                helper.agent.speed = 0.4f;
                ////Portar victima a la Doctora
                Vector3 pos = GameObject.Find("RescatVictimaPos").transform.position;
                helper.agent.SetDestination(pos);
                helper.anim.SetBool("caminar", true);
                while (true)
                {
                    if (!helper.agent.pathPending)
                    {
                        if (helper.agent.remainingDistance < maxDistance)
                        {
                            break;
                        }
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                helper.agent.Stop();
                helper.agent.ResetPath();
                helper.anim.SetBool("caminar", false);
                helper.anim.SetBool("volcar", false);
                //Entregar victima
                //Comunicacio Radio
                clip = "victimaambulancia";
                comunicarRealitzacio();
                v.GetComponentInChildren<Victim>().arrosegaHelper = false;
                v.GetComponent<Victim>().setNmenu(0);
                helper.agent.speed = 1.2f;
                //comprobar estado salud en función del tiempo
                float tiempoact = GameManager.getMaxSeconds() - GameManager.GetSeconds();
                if (tiempoact < 240.0f)
                {
                    GameManager.completeObjective(15, true);
                }
                else if (tiempoact > 240.0f && tiempoact < 480.0f)
                {
                    GameManager.completeObjective(15, false);
                }

            }//Fin if
        }//Fin if
        yield return new WaitForSeconds(0.01f);
    }
    private IEnumerator anarEscalas3()
    {
        if (!GameManager.victim2placed)
        {
            GameObject v = GameObject.Find("Victim2");
            if (v.GetComponent<Victim>().posicio2 == Victim.Posicio2.PLANTA3)
            {
                //Comunicacio Radio
                clip = "trobatvictimaEscales";
                comunicarRealitzacio();
                //Agafar Victima2
                v.GetComponent<Victim>().arrosegaHelper = true;
                v.GetComponent<BoxCollider>().enabled = false;
                v.GetComponent<Victim>().setNmenu(1);
                v.transform.rotation = Quaternion.Euler(v.transform.eulerAngles.x, helper.transform.eulerAngles.y, v.transform.eulerAngles.z);
                helper.agent.speed = 0.4f;
                ////Portar victima a la Doctora
                Vector3 pos = GameObject.Find("RescatVictimaPos").transform.position;
                helper.agent.SetDestination(pos);
                helper.anim.SetBool("caminar", true);
                while (true)
                {
                    if (!helper.agent.pathPending)
                    {
                        if (helper.agent.remainingDistance < maxDistance)
                        {
                            break;
                        }
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                helper.agent.Stop();
                helper.agent.ResetPath();
                helper.anim.SetBool("caminar", false);
                helper.anim.SetBool("volcar", false);
                //Entregar victima
                //Comunicacio Radio
                clip = "victimaambulancia";
                comunicarRealitzacio();
                v.GetComponentInChildren<Victim>().arrosegaHelper = false;
                v.GetComponent<Victim>().setNmenu(0);
                helper.agent.speed = 1.2f;
                //comprobar estado salud en función del tiempo
                float tiempoact = GameManager.getMaxSeconds() - GameManager.GetSeconds();
                if (tiempoact < 240.0f)
                {
                    GameManager.completeObjective(15, true);
                }
                else if (tiempoact > 240.0f && tiempoact < 480.0f)
                {
                    GameManager.completeObjective(15, false);
                }

            }//Fin if
        }//Fin if
        yield return new WaitForSeconds(0.01f);
    }
}

public class HelperPAAction : HelperAction
{

	public HelperPAAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, string req) : base(n, deps, obj, actionVal, help, targ, req)
	{
        maxDistance = 0.1f;
    }

	public override IEnumerator doAction(int i)
	{
		return null;
	}

	public override IEnumerator prepareForAction(int i)
	{
		return null;
	}

}

public class HelperEPAction : HelperAction
{

    public HelperEPAction(string n, string[] deps, int obj, int actionVal, Helper help, string[] targ, string req) : base(n, deps, obj, actionVal, help, targ, req)
    {
        maxDistance = 0.1f;
    }

    public override IEnumerator doAction(int i)
    {
        foreach (Helper h in GameManager.helpers_ar)
        {
            h.ERA.SetActive(true);
            h.ERA2.SetActive(true);
        }
        yield return 0;
    }

    public override IEnumerator prepareForAction(int i)
    {
        yield return new WaitForSeconds(0.1f);
    }

}

public class BaseAction : HelperAction
{
    HelperAction[] subactions;

    public BaseAction(string n, int actionVal, int total, Helper h) : base()
    {
        nom = n;
        actionValue = actionVal;
        subactions = new HelperAction[total];
        helper = h;
    }

    public void addAction(HelperAction action, int index)
    {
        subactions[index] = action;
    }
    public override IEnumerator startAction()
    {
        foreach (HelperAction a in subactions)
        {
            yield return helper.StartCoroutine(a.startAction());
        }
        yield return helper.StartCoroutine(comunicarRealitzacio());
    }

}
