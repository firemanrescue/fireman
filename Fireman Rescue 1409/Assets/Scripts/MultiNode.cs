﻿using UnityEngine;
using System.Collections;

public class MultiNode : Objectiu {

    public MultiNode(string _nom, string text_c, string text_i, string text_r, string text_expl, int time) :base(_nom, text_c, text_i, text_r, text_expl, time)
    {
        type = "MultiNode";
    }
}
