﻿using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Helper : Huma {
    public List<HelperAction> selected_actions;
    int[] temporary_indexs;
    public Animator anim;
    public UnityEngine.AI.NavMeshAgent agent;
    public GameObject[] items;
    private GameObject sideMenu;
	public Radio radio;
    public GameObject ERA;
    public GameObject ERA2;
    static public int total_fireman = 0;
    public int id;
    public bool radioComm;

    //static public HelperAction[] helper_actions;
    public HelperAction[] helper_actions;
    static public HelperAction[] helper_actionsStatic;  //Variable statica para trabajr con ella desde otros scripts

    override protected void Start()
    {
        base.Start();
        readJSON();
        selected_actions = new List<HelperAction>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        sideMenu = GameObject.Find("SideMenu");
        total_fireman++;
        id = total_fireman;
        radioComm = false;
    }
    protected override void checkOptions()
    {
        options.Clear();
        foreach(HelperAction a in helper_actions)
        {
            if (a.isAvailable(nMenu)) options.Add(a);
        }
    }

   /* protected void completeAction(int future_node_id)
    {
        Node future_node = GameManager.i_node.getChild(future_node_id);
        GameManager.nodes_visitats.Add(future_node_id);
        GameManager.i_node = future_node;
    }*/

    public int findAction(string d)
    {
        for(int i = 0; i< helper_actions.Length; ++i)
        {
            if (helper_actions[i].getNom() == d) return i;
        }
        return -1;
    }

    /// <summary>
    /// Crea menu como si fuese la radio
    /// </summary>
    public override void createMenu()
    {
        GameManager.onMenu = true;
        checkOptions();
        if (menu.activeSelf == true)
        {
            if (options.Count > 0)
            {
                cam.gameObject.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
                cam.gameObject.GetComponent<FirstPersonController>().enabled = false;
                panelM.gameObject.SetActive(true);
                menucanvas.enabled = true;
                clearMenu();

                if (nMenu != 0) menu.GetComponent<GridLayoutGroup>().constraintCount = 2;       
                else menu.GetComponent<GridLayoutGroup>().constraintCount = 1;

                for (int i = 0; i < options.Count; i++)
                {
                    createOption(i);
                }
                createSideMenu();
            }
        }
    }

    private void createSideMenu()
    {
        var children = new List<GameObject>();
        foreach (Transform child in sideMenu.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        foreach (HelperAction n in selected_actions)
        {
            createSideButton(n.getNom());
        }
    }

    public void clearSideMenu()
    {
        var children = new List<GameObject>();
        foreach (Transform child in sideMenu.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

    private void createSideButton(string name)
    {
        GameObject action_button = Instantiate(InterfaceManager.menubutton);
        action_button.GetComponentInChildren<Text>().text = name;

        action_button.GetComponent<Button>().onClick.AddListener(delegate {
            selectedAction(name);
        });

        action_button.transform.SetParent(sideMenu.transform, false);
    }

    private void selectedAction(string name)
    {
        int act = findAction(name);
        helper_actions[act].setSelected(false);
        for (int i = 0; i < selected_actions.Count; ++i)
        {
            if (selected_actions[i].hasDependency(helper_actions[act].getNom()))
            {
                int depAct = findAction(selected_actions[i].getNom());
                helper_actions[depAct].setSelected(false);
                selected_actions.RemoveAt(i);
                i--;
            }
            if (helper_actions[act].getNom() == selected_actions[i].getNom())
            {
                selected_actions.RemoveAt(i);
                i--;
            }
        }
        updateMenu();
    }

    private void updateSideMenu()
    {
        if (sideMenu.transform.childCount != selected_actions.Count) createSideMenu();
    }

    public void updateMenu()
    {
        Accio[] lastOptions = new Accio[options.Count];
        options.CopyTo(lastOptions);
        checkOptions();

        if (lastOptions.Length != options.Count)
        {
            clearMenu();
            for (int i = 0; i < options.Count; i++)
            {
                createOption(i);
            }
        }
        updateSideMenu();
    }

    public void selectedBox(int a, bool on)
    {
        int act = findAction(options[a].getNom());
        if (on)
        {
            helper_actions[act].setSelected(true);
            selected_actions.Add((HelperAction)helper_actions[act]);
        }
        else
        {
            helper_actions[act].setSelected(false);
            for (int i=0; i<selected_actions.Count; ++i)
            {
                if (selected_actions[i].hasDependency(helper_actions[act].getNom()))
                {
                    int depAct = findAction(selected_actions[i].getNom());
                    helper_actions[depAct].setSelected(false);
                    selected_actions.RemoveAt(i);
                    i--;
                }
                if (helper_actions[act].getNom() == selected_actions[i].getNom())
                {
                    selected_actions.RemoveAt(i);
                    i--;
                }
            }
        }
        updateMenu();
    }

    /// <summary>
    /// Function donde recoge la configuracion de los helpers
    /// </summary>
    private void readJSON()
    {
        string scene = SceneManager.GetActiveScene().name;
        string jsn = "";
        switch (scene)
        {
            case "accident1":
                jsn = Resources.Load("HelperJSON").ToString();
                break;
            case "accident2":
                jsn = Resources.Load("Helper2JSON").ToString();
                break;
            case "accident3":
                jsn = Resources.Load("Helper3JSON").ToString();
                break;
            case "accident4":
                jsn = Resources.Load("Helper4JSON").ToString();
                break;
        }

        JSONNode json = JSONNode.Parse(jsn);

        int total_actions = json[0].Count;
        helper_actions = new HelperAction[total_actions];
        helper_actionsStatic=new HelperAction[total_actions]; //Variable statica para trabajr con ella desde otros scripts

        for (int i = 0; i < total_actions; ++i)
        {
            helper_actions[i] = createAction(json, i, 0);
            helper_actionsStatic[i] = createAction(json, i, 0);
        }

    }
    private HelperAction createSubAction(JSONNode json, int i, int subAction)
    {
        HelperAction newAction;
        string name = json[0][i]["actions"][subAction]["name"];
        int n_dependencies = json[0][i]["actions"][subAction]["dependencies"].Count;
        string[] dependencies = new string[n_dependencies];
        if (n_dependencies > 0)
        {
            for (int o = 0; o < n_dependencies; ++o) dependencies[o] = json[0][i]["actions"][subAction]["dependencies"][o];
        }

        int objective = json[0][i]["actions"][subAction]["objective"].AsInt;
        int section = json[0][i]["actions"][subAction]["section"].AsInt;
        int actionVal = json[0][i]["actions"][subAction]["actionVal"].AsInt;
        int total_targets = json[0][i]["actions"][subAction]["target"].Count;

        string[] targets = new string[total_targets];
        for (int x = 0; x < total_targets; x++)
        {
            targets[x] = json[0][i]["actions"][subAction]["target"][x];
        }

        string requisit = json[0][i]["actions"][subAction]["requisit"];
        string type = json[0][i]["actions"][subAction]["type"];
        int node = json[0][i]["actions"][subAction]["node"].AsInt;

        string clip = json[0][i]["actions"][subAction]["clip"];
        switch (type)
        {
            case "HelperFireAction":
                {
                    int total_fires = json[0][i]["actions"][subAction]["fire"].Count;
                    string[] fires = new string[total_fires];
                    for (int x = 0; x < total_fires; x++)
                    {
                        fires[x] = json[0][i]["actions"][subAction]["fire"][x];
                    }
                    HelperFireAction action = new HelperFireAction(name, dependencies, node, actionVal, this, targets, requisit, fires);
                    action.clip = clip;
                     action.setSection(section);
                    newAction = action;
                }
                break;
            case "HelperCommunicationAction":
                {
                    float time = json[0][i]["actions"][subAction]["actionTime"].AsFloat;
                    HelperCommunicationAction action = new HelperCommunicationAction(name, dependencies, node, actionVal, this, targets, time, requisit);
                    action.clip = clip;
                     action.setSection(section);
                    newAction = action;
                }
                break;
            case "HelperPAAction":
                {
                    HelperPAAction action = new HelperPAAction(name, dependencies, node, actionVal, this, targets, requisit);
                    action.clip = clip;
                     action.setSection(section);
                    newAction = action;
                }
                break;
            case "HelperOtherAction":
                {
                    HelperOtherAction action = new HelperOtherAction(name, dependencies, node, actionVal, this, targets, requisit);
                    action.clip = clip;
                    action.setSection(section);
                    newAction = action;
                }
                break;
            case "HelperEPAction":
                {
                    HelperEPAction action = new HelperEPAction(name, dependencies, node, actionVal, this, targets, requisit);
                    action.clip = clip;
                   action.setSection(section);
                    newAction = action;
                }
                break;
            default:
                {
                    HelperAction action = new HelperAction(name, dependencies, node, actionVal, this, targets, requisit);
                    action.clip = clip;
                    action.setSection(section);
                    newAction = action;
                }
                break;
        }
        return newAction;
    }
    private HelperAction createAction(JSONNode json, int i, int subAction)
    {
        HelperAction newAction;
            
            string name = json[0][i]["name"];

            int n_dependencies = json[0][i]["dependencies"].Count;
            string[] dependencies = new string[n_dependencies];
            if (n_dependencies > 0)
            {
                for (int o = 0; o < n_dependencies; ++o) dependencies[o] = json[0][i]["dependencies"][o];
            }

            int objective = json[0][i]["objective"].AsInt;
            int section = json[0][i]["section"].AsInt;
            int actionVal = json[0][i]["actionVal"].AsInt;
            int total_targets = json[0][i]["target"].Count;
            string[] targets = new string[total_targets];

            for (int x = 0; x < total_targets; x++)
            {
                targets[x] = json[0][i]["target"][x];
            }

            string requisit = json[0][i]["requisit"];
            string type = json[0][i]["type"];
            int node = json[0][i]["node"].AsInt;
            string clip = json[0][i]["clip"];
            switch (type)
            {
                case "HelperFireAction":
                    {
                        int total_fires = json[0][i]["fire"].Count;
                        string[] fires = new string[total_fires];
                        for (int x = 0; x < total_fires; x++)
                        {
                            fires[x] = json[0][i]["fire"][x];
                        }
                        HelperFireAction action = new HelperFireAction(name, dependencies, node, actionVal, this, targets, requisit, fires);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
                case "HelperCommunicationAction":
                    {
                        float time = json[0][i]["actionTime"].AsFloat;
                        HelperCommunicationAction action = new HelperCommunicationAction(name, dependencies, node, actionVal, this, targets, time, requisit);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
                case "HelperPAAction":
                    {
                        HelperPAAction action = new HelperPAAction(name, dependencies, node, actionVal, this, targets, requisit);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
                case "HelperOtherAction":
                    {
                        HelperOtherAction action = new HelperOtherAction(name, dependencies, node, actionVal, this, targets, requisit);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
                case "HelperEPAction":
                    {
                        HelperEPAction action = new HelperEPAction(name, dependencies, node, actionVal, this, targets, requisit);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
                case "BaseAction":
                    {
                        int total_subAction = json[0][i]["actions"].Count;
                        BaseAction action = new BaseAction(name, actionVal, total_subAction, this);
                        action.clip = clip;
                        action.setSection(section);

                    for (int n = 0; n < total_subAction; n++)
                        {
                            HelperAction sub = createSubAction(json, i, n);
                            action.addAction(sub, n);
                        }
                        newAction = action;
                    }
                    break;
                default:
                    {
                        HelperAction action = new HelperAction(name, dependencies, node, actionVal, this, targets, requisit);
                        action.clip = clip;
                        action.setSection(section);
                        newAction = action;
                    }
                    break;
            }
        return newAction;
    }

    protected override void createOption(int i)
    {
        GameObject action_button = Instantiate(InterfaceManager.menubutton);
        action_button.GetComponentInChildren<Text>().text = options[i].getNom();

        if (options[i].getActionValue() >= 0)
        {
            int name = options[i].getActionValue();
            int objective = options[i].getObjectiveNode();
            string requisit = options[i].getRequisit();
            action_button.GetComponent<Button>().onClick.AddListener(delegate {
                interfaceManager.CapturaBoto(name, cam, nom, menucanvas, objective);
            });
        }
        else
        {
            GameObject selection_box = Instantiate(Resources.Load("CheckBox") as GameObject);
            int TempInt = i;
            if (options[i].getSelected()) selection_box.GetComponent<Toggle>().isOn = true;
            selection_box.GetComponent<Toggle>().onValueChanged.AddListener((value) => { selectedBox(TempInt, value); });
            selection_box.transform.SetParent(menu.transform, false);
        }
        action_button.transform.SetParent(menu.transform, false);
    }


    public IEnumerator startWorking()
    {
        foreach (HelperAction action in selected_actions)
        {
            yield return StartCoroutine(action.startAction());
            //GameManager.nodes_visitats.Add(action.getNode());
            foreach(HelperAction a in helper_actions)
            {
                if(a.getNom() == action.getNom()) a.setSelected(false);
            }
        }
        selected_actions.Clear();
    }
}
