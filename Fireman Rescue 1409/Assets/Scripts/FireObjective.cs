﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireObjective : Objectiu {
    Foc[] fires;

    public FireObjective() { }
    public FireObjective(string _nom, string text_c, string text_i, string text_r, string text_expl, int time, string[] fire_names): base(_nom, text_c, text_i, text_r, text_expl, time)
    {
        type = "FireObjective";

        int total_fires = fire_names.Length;
        fires = new Foc[total_fires];
        for(int i = 0; i<total_fires; i++){
            fires[i] = GameObject.Find(fire_names[i]).GetComponent<Foc>();
        }
    }

    public override bool isCompleted()
    {
        foreach(Foc fire in fires)
        {
            if (!fire.getApagat()) return false;
        }
        return true;
    }

    public void addFires(List<Foc> n_fires)
    {
        int total = n_fires.Count;
        fires = new Foc[total];
        //Debug.Log("total" + total);
        for (int i = 0; i < total; i++)
        {
            //Debug.Log("Fire" + i);
            fires[i] = n_fires[i];
            fires[i].setObjective(13);
        }
    }
}
