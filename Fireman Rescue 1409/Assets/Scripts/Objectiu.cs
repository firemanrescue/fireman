﻿using UnityEngine;
using System.Collections;

public class Objectiu
{

    private string nom;
    private string text_completat;
    private string text_incomplet;
    private string text_regular;
    private string explicacio;
    private float segons_completat;
    private int max_time;
    private bool regular, completed;
    protected string type;                  //Variable para identificar multinode

    public Objectiu() { }
    public Objectiu(string _nom, string text_c, string text_i, string text_r, string text_expl, int time)
    {
        nom = _nom;
        text_completat = text_c;
        text_incomplet = text_i;
        text_regular = text_r;
        explicacio = text_expl;
        max_time = time;
        regular = false;
        completed = false;
    }

    public string getNom()
    {
        return nom;
    }
    public void setTextCompletat(string text)
    {
        text_completat = text;
    }
    public void setTextIncomplet(string text)
    {
        text_incomplet = text;
    }
    public void setTextResum(string text)
    {
        text_regular = text;
    }
    public string getTextCompletat()
    {
        return text_completat;
    }
    public string getTextIncomplet()
    {
        return text_incomplet;
    }
    public string getTextRegular()
    {
        return text_regular;
    }
    public float getSegonsCompletat()
    {
        return segons_completat;
    }
    public void setSegonsCompletat(float segons)
    {
        segons_completat = segons;
    }

    public virtual bool isCompleted()
    {
        return completed;
    }

    public void setExplicacio(string xpl)
    {
        explicacio = xpl;
    }
    public string getExplicacio()
    {
        return explicacio;
    }

    public void completeObjective()
    {
        completed = true;
    }
    public string getType()
    {
        return type;
    }
    public int getMaxTime()
    {
        return max_time;
    }
    public void setMaxTime(int t)
    {
        max_time = t;
    }
    public bool getRegular()
    {
        return regular;
    }
    public void setRegular(bool r)
    {
        regular = r;
    }
}

