﻿using UnityEngine;
using System;

public static class Score{
    public static int casco;
    public static float cascos;
    private static int my_totalScore;

    /// <summary>
    /// Retorna puntuacio obtinguda per temps. Max 30 punts.
    /// </summary>
    /// <returns></returns>
    public static int calculate_timeScore()
    {
        float time_score;
        float player_time = 0;
        float min_time = 0;
        float premioScore = GameManager.max_score * 0.2f;                       //Si lo hace en tiempo mayor al min_time, le premiamos con un poco mas de Score
        /*int maxtimeScore = ((GameManager.max_score) / 5) * (-1);
        player_time = GameManager.getMaxSeconds() - GameManager.getSeconds();
        min_time = GameManager.getminTime();
        time_score = (maxtimeScore * player_time) / (min_time * 2);
        if (time_score < maxtimeScore) time_score = maxtimeScore;
        if (player_time <= min_time) time_score = 0;
        Debug.Log("TimeScore: "+time_score);
        return Mathf.RoundToInt(Mathf.Abs(time_score));
        */

        player_time = GameManager.getMaxSeconds() - GameManager.GetSeconds();
        if (player_time > GameManager.getminTime())
        {
            GameManager.max_score = GameManager.max_score + (int)premioScore;   //actualizamos el max_score, para aposterior calcular los cascos
            return Mathf.RoundToInt(premioScore);
        }
        else return 0;

    }
    /// <summary>
    /// Retorna puntuacio total aconseguida. Max 100.
    /// </summary>
    /// <returns></returns>
    public static void getTotalScore()
    {
        int time_score = calculate_timeScore();
        int total_score = time_score + GameManager.total_score;
        if (total_score < 0) total_score = 0;
        my_totalScore= total_score;
    }
    /// <summary>
    /// Retorna nombre de Cascos obtinguts. Max 5.
    /// </summary>
    /// <returns></returns>
    public static float calculateHelmets()
    {
        //float helmets=0f;
        casco = 0;
        getTotalScore();//
        float helmets = (5f * (float)my_totalScore) / GameManager.max_score;
        Debug.Log("Helmets: " + helmets + " Score: "+ my_totalScore);
        //Redondeos
        if (helmets > 0f && helmets <= 0.5f) cascos = 0.5f; //return 0.5f;
        else if (helmets >= 0.6f && helmets <= 1.0f) { casco = 1; cascos= 1.0f; }
        else if (helmets >= 1.1f && helmets <= 1.5f) { casco = 1; cascos= 1.5f; }
        else if (helmets >= 1.6f && helmets <= 2.0f) { casco = 2; cascos = 2.0f; }
        else if (helmets >= 2.1f && helmets <= 2.5f) { casco = 2; cascos = 2.5f; }
        else if (helmets >= 2.6f && helmets <= 3.0f) { casco = 3; cascos = 3.0f; }
        else if (helmets >= 3.1f && helmets <= 3.5f) { casco = 3; cascos = 3.5f; }
        else if (helmets >= 3.6f && helmets <= 4.0f) { casco = 4; cascos = 4.0f; }
        else if (helmets >= 4.1f && helmets <= 4.5f) { casco = 4; cascos = 4.5f; }
        else if (helmets >= 4.6f && helmets <= 5f) { casco = 5; cascos = 5.0f; }


            //return (float)System.Math.Round(helmets, 1,MidpointRounding.ToEven);            //devuelve helmet redondeado a 1 decimal

             return helmets;
        }

            /// <summary>
            /// Guardar puntuacio obtinguda en BBDD.
            /// </summary>
            public static void saveScore()
            {
                try
                {
                    //Preparar variables usuari, puntuacio, nom nivell.
                    float score = cascos;
                    string levelname = GameManager.levelname;
                    string username = UserData.getUsername();
                    string timestamp = DateTime.Now.ToString();
                    //string scenename = GameManager.scenename;

                    //updateScore(levelname, scenename, (double)score);       //falta añadir el nivel al que pertenece la puntuacion
                    string SaveScoreURL = "https://viodgamescom.000webhostapp.com/SaveScore.php";
                    WWWForm form = new WWWForm();
                    form.AddField("scr", score.ToString());
                    form.AddField("ts", timestamp);
                    form.AddField("lvlname", levelname);
                    form.AddField("user", username);
                    WWW phpRequest = new WWW(SaveScoreURL, form);
                }
                catch (Exception ex)
                {
                    //Error
                    Debug.Log(ex.ToString());
                }
            }

            /// <summary>
            /// Actualiza los Scores del nombre del nivel pasado 
            /// </summary>
            /// <param name="name">Nombre del nivel</param>
            /// <param name="scene">Nombre de la escena/mundo</param>
            /// <param name="score">puntuación nueva</param>
            /// <param name="level">nivel del mundo/escena</param>
            public static void updateScore(string name, string scene, double score, int level)
            {
                int totalLevels = UserData.unlockedLevels.Count;
                if (totalLevels > 0)
                {
                    for (int i = 0; i < totalLevels; i++)
                    {
                        if (UserData.unlockedLevels[i].name == name)                    //Exemple "accident1"
                        {
                            if (UserData.unlockedLevels[i].score < score)
                            {
                                UserData.unlockedLevels.RemoveAt(i);
                                UserData.insertLevel(name, scene, score, level);
                                return;
                            }
                        }
                    }
                }
                UserData.insertLevel(name, scene, score, level);
            }

        }