﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using DG.Tweening;

public class Resum : MonoBehaviour {

    private GameObject Content;
    public float points;
   //Cascos
	GameObject one;
	GameObject two;
	GameObject three;
	GameObject four;
	GameObject five;
    // Half Cascos 50%
	GameObject oneH;
	GameObject twoH;
	GameObject threeH;
	GameObject fourH;
	GameObject fiveH;
    //Canvas sin internet
    GameObject withowInternet;
    //sonido particulas
    AudioSource spark;

    Color ColorInici = new Color(0.2f,0.2f,0.2f,1f);
    Color ColorFinal = new Color(1f, 1f, 1f, 1f);
    Image halfHelm;

	int optionsSize;
    int INum;
    float scaleIncrement;
    int caso;
    string[] frases;

    GameObject party;

    // Use this for initialization
    void Start() {

        SingletonGameObjectsHand.manguera = false;
        SingletonGameObjectsHand.axe = false;
        SingletonGameObjectsHand.radio = false;
        SingletonGameObjectsHand.medicine = false;

        spark = GameObject.Find("Canvas").GetComponent<AudioSource>();          //Recoger sonido de la particula

        //Debug.Log("PrimerERA: " + GameManager.primerERA);
        if (!GameManager.rebutMal && GameManager.primerERA)
        {
            GameManager.completeObjective(9,true);            //Si no recibe daño y se equipa correctamente.... suma puntos
        }
        else if(!GameManager.rebutMal && !GameManager.primerERA)
        {
            GameManager.completeObjective(9, false);
        }

        Content = GameObject.Find("Text");
        InterfaceManager.x = 0;
        SingletonCharacter.health = 500f;

        points = Score.calculateHelmets();                                      //GetPoints
        Score.saveScore();

        //Debug.Log("points: " + points);          ////////

        //CASCOS
        one = GameObject.Find("1");
        two = GameObject.Find("2");
        three = GameObject.Find("3");
        four = GameObject.Find("4");
        five = GameObject.Find("5");
        oneH = GameObject.Find("05");
        twoH = GameObject.Find("15");
        threeH = GameObject.Find("25");
        fourH = GameObject.Find("35");
        fiveH = GameObject.Find("45");

        //Ajusta tamaño de textView a los objetivos
        optionsSize = GameManager.ar_objectius.Length;
        float espacio = GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta.y - Content.GetComponent<RectTransform>().sizeDelta.y;
        Content.GetComponent<RectTransform>().sizeDelta = new Vector2(Content.GetComponent<RectTransform>().sizeDelta.x, 30 * optionsSize);
        GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta.x, (30 * optionsSize) + espacio);

       //Desabilitar scrollbars
        if (optionsSize < 9)
        {
            GameObject.Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
            GameObject.Find("Scrollbar Vertical").SetActive(false);
        }
        /*int cellSize = (int)Panel_Objectius.GetComponent<RectTransform>().sizeDelta.y / optionsSize;
        Panel_Objectius.GetComponent<GridLayoutGroup>().cellSize = new Vector2(Panel_Objectius.GetComponent<GridLayoutGroup>().cellSize.x, cellSize-5);
        Panel_Objectius.GetComponent<GridLayoutGroup>().spacing = new Vector2(0,5);
        Panel_Temps.GetComponent<GridLayoutGroup>().cellSize = new Vector2(Panel_Temps.GetComponent<GridLayoutGroup>().cellSize.x, cellSize-5);
        Panel_Temps.GetComponent<GridLayoutGroup>().spacing = new Vector2(0, 5);*/

        //Bucle para recorerse la array de objetivos
        for (int i = 0; i < optionsSize; ++i)
        {
            string str_obj = "";
            string str_expl = "";
            //Objeto Options
            GameObject newGO = new GameObject("Options" + i);
            newGO.transform.SetParent(Content.transform);
            Text myText = newGO.AddComponent<Text>();
            myText.horizontalOverflow = HorizontalWrapMode.Overflow;
            myText.verticalOverflow = VerticalWrapMode.Overflow;
            //Objeto explicacion
            GameObject GOexpl = new GameObject("Explicacio" + i);
            GOexpl.transform.SetParent(newGO.transform);
            Text explText = GOexpl.AddComponent<Text>();
            //Objeto tiempo
            GameObject secondGO = new GameObject("Timer" + i);
            Text timeText = secondGO.AddComponent<Text>();
            secondGO.transform.SetParent(Content.transform);
            secondGO.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 0);
            secondGO.GetComponent<RectTransform>().localPosition = new Vector3(secondGO.GetComponent<RectTransform>().transform.position.x, secondGO.GetComponent<RectTransform>().transform.position.y, 0);
            timeText.horizontalOverflow = HorizontalWrapMode.Overflow;
            timeText.verticalOverflow = VerticalWrapMode.Overflow;

            //Rellenado de objetos con datos
            if (GameManager.ar_objectius[i].isCompleted())
            {
                if (!GameManager.ar_objectius[i].getRegular())                  //
                {
                    str_obj = string.Format(GameManager.ar_objectius[i].getTextCompletat(), System.Environment.NewLine);
                    myText.color = Color.green;
                    timeText.color = Color.green;
                }
                else
                {
                    str_obj = string.Format(GameManager.ar_objectius[i].getTextRegular(), System.Environment.NewLine);
                    myText.color = Color.yellow;
                    timeText.color = Color.yellow;
                }
                //if (GameManager.ar_objectius[i].getOrdre() != -1)
                //{
                timeText.font = Resources.Load<Font>("BOMBARD_");
                timeText.fontSize = 10;
                timeText.DOText(Utilities.getTimeString(GameManager.ar_objectius[i].getSegonsCompletat()), 6f * Time.deltaTime, true, ScrambleMode.None, null);
            }
            else
            {
                if(GameManager.ar_objectius[i].getType() == "MultiObjective" || GameManager.ar_objectius[i].getType() == "MultiNode")
                {
                    MultiObjective multi_objective = (MultiObjective)GameManager.ar_objectius[i];
                        if (multi_objective.hasOneCompleted())
                        {
                            str_obj = string.Format(GameManager.ar_objectius[i].getTextRegular(), System.Environment.NewLine);
                            myText.color = Color.yellow;
                            timeText.color = Color.yellow;
                        }
                        else
                        {
                            str_obj = string.Format(GameManager.ar_objectius[i].getTextIncomplet(), System.Environment.NewLine);
                            myText.color = Color.red;
                        }
                }
                else
                {
                    str_obj = string.Format(GameManager.ar_objectius[i].getTextIncomplet(), System.Environment.NewLine);
                    myText.color = Color.red;
                }

            }
            myText.font = Resources.Load<Font>("BOMBARD_");
            myText.fontSize = 10;
            str_expl = string.Format(GameManager.ar_objectius[i].getExplicacio(), System.Environment.NewLine);
            explText.fontSize = 8;
            explText.color = Color.black;
            explText.font = Resources.Load<Font>("BOMBARD_");
            newGO.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 0);
            newGO.GetComponent<RectTransform>().localPosition = new Vector3(newGO.GetComponent<RectTransform>().transform.position.x, newGO.GetComponent<RectTransform>().transform.position.y, 0);

            explText.horizontalOverflow = HorizontalWrapMode.Overflow;
            explText.verticalOverflow = VerticalWrapMode.Overflow;
            GOexpl.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
            GOexpl.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
            GOexpl.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 0f);
            GOexpl.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            GOexpl.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

            myText.DOText(str_obj, 50f * Time.deltaTime, true, ScrambleMode.None, null);
            explText.DOText(str_expl, 50f * Time.deltaTime, true, ScrambleMode.None, null);

        }
        //Restarting images

        one.GetComponent<Image>().DOColor(ColorInici, 1f);
        two.GetComponent<Image>().DOColor(ColorInici, 1f);
        three.GetComponent<Image>().DOColor(ColorInici, 1f);
        four.GetComponent<Image>().DOColor(ColorInici, 1f);
        five.GetComponent<Image>().DOColor(ColorInici, 1f);

        oneH.GetComponent<Image>().enabled = false;
        twoH.GetComponent<Image>().enabled = false;
        threeH.GetComponent<Image>().enabled = false;
        fourH.GetComponent<Image>().enabled = false;
        fiveH.GetComponent<Image>().enabled = false;

        InvokeRepeating("helmetOn", 0, 0.5f);

         switch (Score.casco)
        {
            case 1:
                Invoke("halfHelmet", 0.5f ); break;
            case 2:
                Invoke("halfHelmet", 1.5f); break;
            case 3:
                Invoke("halfHelmet", 2f); break;
            case 4:
                Invoke("halfHelmet", 2.5f); break;

        }

        Destroy(GameObject.Find("GameManager").gameObject);

        one.GetComponent<Image>().rectTransform.DOScale(0f, 0f);
        two.GetComponent<Image>().rectTransform.DOScale(0f, 0f);
        three.GetComponent<Image>().rectTransform.DOScale(0f, 0f);
        four.GetComponent<Image>().rectTransform.DOScale(0f, 0f);
        five.GetComponent<Image>().rectTransform.DOScale(0f, 0f);

    }

    void Update(){
        //Comprueba si hay internet y si no hay avisa
        if (Application.internetReachability == NetworkReachability.NotReachable){
            GameObject canvas = GameObject.Find("Canvas");
            canvas.SetActive(false);
            withowInternet = Instantiate(Resources.Load("Prefabs/SinInternet"), new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
        }
        //Awake

        /*for (int INum = 1; INum <= points; ++INum)
		{
			Invoke ("helmetOn", 1f);

		}*/
        else {
            if (scaleIncrement < 0.5f)
            {
                scaleIncrement += 0.1f;
            }
            else
            {
                scaleIncrement = 0.1f;
                ++caso;
            }

            switch (caso)
            {
                case 1:
                    one.GetComponent<Image>().rectTransform.DOScale(1f, 1f);
                    break;
                case 2:
                    two.GetComponent<Image>().rectTransform.DOScale(1f, 1f);
                    break;
                case 3:
                    three.GetComponent<Image>().rectTransform.DOScale(1f, 1f);
                    break;
                case 4:
                    four.GetComponent<Image>().rectTransform.DOScale(1f, 1f);
                    break;
                case 5:
                    five.GetComponent<Image>().rectTransform.DOScale(1f, 1f);
                    break;
                default:
                    break;
            }
        }
    }

    void helmetOn()
    {
        if (INum <= points)
        {
            switch (INum)
            {
                case 1:
                    one.GetComponent<Image>().DOColor(ColorFinal, 1f);
                    party = Instantiate(Resources.Load("Prefabs/Spark"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
                    party.transform.SetParent(one.transform);
                    party.transform.localPosition = new Vector3(-16.5f, 10f, -10f);
                    party.GetComponent<ParticleSystem>().Play();
                    spark.Play();
                    break;
                case 2:
                    two.GetComponent<Image>().DOColor(ColorFinal, 1f);
                    party = Instantiate(Resources.Load("Prefabs/Spark"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
                    party.transform.SetParent(two.transform);
                    party.transform.localPosition = new Vector3(-16.5f, 10f, -10f);
                    party.GetComponent<ParticleSystem>().Play();
                    spark.Play();
                    break;
                case 3:
                    three.GetComponent<Image>().DOColor(ColorFinal, 1f);
                    party = Instantiate(Resources.Load("Prefabs/Spark"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
                    party.transform.SetParent(three.transform);
                    party.transform.localPosition = new Vector3(-16.5f, 10f, -10f);
                    party.GetComponent<ParticleSystem>().Play();
                    spark.Play();
                    break;
                case 4:
                    four.GetComponent<Image>().DOColor(ColorFinal, 1f);
                    party = Instantiate(Resources.Load("Prefabs/Spark"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
                    party.transform.SetParent(four.transform);
                    party.transform.localPosition = new Vector3(-16.5f, 10f, -10f);
                    party.GetComponent<ParticleSystem>().Play();
                    spark.Play();
                    break;
                case 5:
                    five.GetComponent<Image>().DOColor(ColorFinal, 1f);
                    party = Instantiate(Resources.Load("Prefabs/Spark"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
                    party.transform.SetParent(five.transform);
                    party.transform.localPosition = new Vector3(-16.5f, 10f, -10f);
                    party.GetComponent<ParticleSystem>().Play();
                    spark.Play();
                    break;
                default:
                    break;
            }
            ++INum;
        }
    }

    void halfHelmet()
    {
        if (points == 0.5f)
        {
            halfHelm = oneH.GetComponent<Image>();
            halfHelm.enabled = true;
            halfHelm.DOColor(ColorFinal, 1f);
        }
        else if (points == 1.5f)
        {
            halfHelm = twoH.GetComponent<Image>();
            halfHelm.enabled = true;
            halfHelm.DOColor(ColorFinal, 1f);
        }
        else if (points == 2.5f)
        {
            halfHelm = threeH.GetComponent<Image>();
            halfHelm.enabled = true;
            halfHelm.DOColor(ColorFinal, 1f);
        }
        else if (points == 3.5f)
        {
            halfHelm = fourH.GetComponent<Image>();
            halfHelm.enabled = true;
            halfHelm.DOColor(ColorFinal, 1f);

        }
        else if (points == 4.5f)
        {
            halfHelm = fiveH.GetComponent<Image>();
            halfHelm.enabled = true;
            halfHelm.DOColor(ColorFinal, 1f);
        }
    }

    /*
     //Controlar que nivel cargar
        string scene = SceneManager.GetActiveScene().name;
        string jsn = "";
        switch (scene)
        {
            case "accident1":
                 jsn = Resources.Load("JSON_NodeLVL1").ToString();
                break;
            case "accident2":
                 jsn = Resources.Load("JSON_NodeLVL2").ToString();
                break;
        }
      */
}
