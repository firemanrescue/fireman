﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;

/// <summary>
/// Script for show tips in loadScreen
/// </summary>
public class LoadTips : MonoBehaviour {

    //variables privadas
    private string[] array_tips;
    private JSONNode jsonN = new JSONNode();
    private int postip;
    private string showtip;
    private int length;

    //variables publicas
    public TextAsset file;
    public Text tipTextforcanvas;

    // Use this for initialization
    void Start () {
        initializeJSON();
        initializeTips();
        showTip();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void initializeTips()
    {
        length = jsonN["consells"].Count;
        array_tips = new string[length];
        for(int i = 0; i < length; i++)
        {
            string tip = jsonN["consells"][i]["text"];
            array_tips[i] = tip;
        }

    }

    void initializeJSON()
    {
        try
        {
            jsonN = JSONNode.Parse(file.text);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    void showTip()
    {
        postip = UnityEngine.Random.Range(0, length);
        showtip = array_tips[postip];
        tipTextforcanvas.text = showtip;
    }
}
