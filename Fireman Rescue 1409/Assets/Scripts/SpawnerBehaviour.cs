﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBehaviour : MonoBehaviour {

    public List<GameObject> m_playerList;
    public int peopleToSpawn = 20;

	// Use this for initialization
	void Start () {
        StartCoroutine(Spawn(true));
        StartCoroutine(Spawn(false, 2));
    }
	
    /// <summary>
    /// Spawns either a mob of people or person in a relative time
    /// </summary>
    /// <param name="spawnMob"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public IEnumerator Spawn(bool spawnMob, float time = 0)
    {
        if (!spawnMob)
        {
            while (!spawnMob)
            {
                uint randomIterator = (uint)Random.Range(0, m_playerList.Count);
                Instantiate(m_playerList[(int)randomIterator], this.transform.position, Quaternion.identity, this.transform);
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                yield return new WaitForSeconds(time);
            }
        }
        else
        {
            for (uint i = 0; i != peopleToSpawn; i++)
            {
                uint randomIterator = (uint)Random.Range(0, m_playerList.Count);
                Instantiate(m_playerList[(int)randomIterator], this.transform.position, Quaternion.identity, this.transform);
            }
            yield break;
        }
        
    }

}
