﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;
using DG.Tweening;
using UnityEngine.EventSystems;

/// <summary>
/// Script for control all buttons and canvas of the GUI Menus
/// </summary>
public class Buttons : MonoBehaviour
{
    enum levels { LEVEL1, LEVEL2, LEVEL3 };
    AsyncOperation aO;                          //Charge level show particles
    public GameObject loadingScreenBG;          //BG of the load level
    public GameObject fireLoad;                 //Particles of the load level
    public GameObject exitButton;               //For activate/desactivate buttons exit
    public Text textoBriefing;                  //Component text of the mission to make
    public Text loadingText;                    //Only show "Loading..."
    public Text tipText;                        //to show the tip mission
    public Image FadeImg;                       //Image to make fade with Tweening
    //private float fadeSpeed = 10f;
    //private float unfadeSpeed = 10f;
    //private bool fadeIn = false;
    //public bool isFakeLoadingBar = false;
    //public float fakeIncrement = 0f;
    //public float fakeTiming = 0f;
    public RectTransform RTimage;               //To scale image
    public RectTransform RTtext;                //To scale text

    public GameObject loginCanvas;              //Canvas of logging
    public GameObject registerCanvas;           //Canvas of register
    public GameObject errorMessage;             //Canvas Error
    public GameObject levelsCanvas;
    public Text loggedUser;                     //Save the user in system, to show it in the txtbox 
    private EventSystem system;

    public GameObject Candado2;                 //Controla si esta el candado muestra el TIP de la mision
    public GameObject Candado3;

    void Start()
    {
        system = EventSystem.current;

        //if (Application.loadedLevelName == "MainMenu")
        if (SceneManager.GetActiveScene().name=="MainMenu")
        { 
            Destroy(GameObject.Find("GameManager"));    //Se destruye gamemanager para resetear datos juego
            if (UserData.isLogged())                    //Controlamos si el user esta validado, si lo está cambiamos scena y activamos script
            {
                loggedUser.text = UserData.getUsername();
                changeCanvas(loginCanvas, levelsCanvas);
                GameObject.Find("Canvas").GetComponent<prepareMenu>().enabled = true; //Activamos el gameObject de la Hierarchy
            }
        }
    }
    /*/// <summary>
     /// Carregar Scene desitjada. nlevel es el nom de la Scene.
     /// </summary>
     /// <param name="nlevel"></param>
     public void LoadLevel(string nlevel)
     {
         if (nlevel == "MainMenu") scriptCancion.Play();
         SceneManager.LoadScene(nlevel);
     }*/
    /// <summary>
    /// Desactivate Origin, and Activate end. reset GameObject errorMessage
    /// </summary>
    /// <param name="origin">GameObject to desactivate</param>
    /// <param name="end">GameObject to Activate</param>
    public void changeCanvas(GameObject origin, GameObject end)
    {
        origin.SetActive(false);
        end.SetActive(true);
        resetError();
    }

    ////Mensajes de Error/////
    public void showMessage(string error)
    {
        Text textMessage = errorMessage.GetComponentInChildren<Text>();
        errorMessage.GetComponent<Image>().DOFade(1f, 0.5f);
        errorMessage.GetComponent<RectTransform>().DOScale(1f, 0.5f);
        textMessage.text = string.Format(error, System.Environment.NewLine);
    }
    public void resetError()
    {
        errorMessage.GetComponentInChildren<Text>().text = "";
        errorMessage.GetComponent<Image>().DOFade(0.1f, 0.5f);
        errorMessage.GetComponent<RectTransform>().DOScale(0.1f, 0.5f);
    }
    ////
    /// <summary>
    /// To go from register to login Canvas
    /// </summary>
    public void Back()
    {
        changeCanvas(registerCanvas, loginCanvas);
        system.SetSelectedGameObject(GameObject.Find("InputUserLogin"));        //To make focus to GameObject finded
    }
    ////
    public void goRegister()
    {
        changeCanvas(loginCanvas, registerCanvas);
        system.SetSelectedGameObject(GameObject.Find("InputUserRegister"));
    }
    /// <summary>
    /// Login d'usuario introduït.
    /// </summary>
    public void Login()
    {
        //Text textMessage = GameObject.Find("Message").GetComponentInChildren<Text>();
        string user = GameObject.Find("UsernameL").GetComponentInChildren<InputField>().text; //user introduit a InputField.
        string password = GameObject.Find("PasswordL").GetComponentInChildren<InputField>().text; //pass introduida a InputField.
        string en_pswd = Utilities.Encrypt(password); // encriptacio de password.
        try
        {
            string LogginUserURL = "https://viodgamescom.000webhostapp.com/users.php";
            WWWForm form = new WWWForm();
            form.AddField("user", user);
            form.AddField("pswd", en_pswd);
            WWW phpRequest = new WWW(LogginUserURL, form);
            while (!phpRequest.isDone) { }  ///Posible try catch
            string phpText = phpRequest.text;
            if (phpText == "Correcte") // Si pswd no es null es que l'usuari existeix.
            {
                UserData.setUsername(user); //Donar nom d'usuari a variable username.    
                UserData.setLogged(true);
                loggedUser.text = user;
                UserData.loadUser();


                StartCoroutine(changeCanvas(loginCanvas, levelsCanvas, "Usuari loggejat correctament"));
            }
            else showMessage(phpText);
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
        }
    }
    /// <summary>
    /// Registrar nou usuari.
    /// </summary>
    public void Register()
    {
        string nom = GameObject.Find("Name").GetComponentInChildren<InputField>().text;
        string user = GameObject.Find("Username").GetComponentInChildren<InputField>().text;
        string password = GameObject.Find("Password").GetComponentInChildren<InputField>().text;
        string rpt_password = GameObject.Find("Rep.Passwd").GetComponentInChildren<InputField>().text;
        string email = GameObject.Find("Email").GetComponentInChildren<InputField>().text;


        //condicions inputs.
        if (!Utilities.containsLetters(user) && !Utilities.containsNumbers(user)) //comprovar que hagi escrit usuari.
        {
            showMessage("Nom d'usuari incorrecte.{0}Ha de contenir al menys una lletra o nombre");
            return;
        }
        if (!Utilities.containsLetters(password) && !Utilities.containsNumbers(password))
        {
            showMessage("No has introduït cap contrasenya");
            return;
        }
        if (!password.Equals(rpt_password)) // comprovar que hagi escrit correctament la contrasenya que vol.
        {
            showMessage("Les contrasenyes no són iguals");
            return;
        }
        if (!Utilities.IsEmail(email)) //comprovar que hagi introduït un email.
        {
            showMessage("Email incorrecte");
            return;
        }

        string CreateUserURL = "https://viodgamescom.000webhostapp.com/InsertUser.php";
        WWWForm form = new WWWForm();
        form.AddField("nom", nom);
        form.AddField("user", user);
        form.AddField("pswd", Utilities.Encrypt(password));
        form.AddField("email", email);
        WWW www = new WWW(CreateUserURL, form);
        while (!www.isDone) { }
        StartCoroutine(changeCanvas(registerCanvas, loginCanvas, "Usuari creat correctament"));
    }

    /// <summary>
    /// To load the scene, while show the screen load level(particles...)
    /// </summary>
    /// <param name="scene"> The name of the scene to charge</param>
    public void LoadLevel(string scene)
    {
        Debug.Log("Carrego escena aqui");
        if (loadingScreenBG != null)
        {
            loadingScreenBG.SetActive(true);
            fireLoad.gameObject.SetActive(true);
            loadingText.gameObject.SetActive(true);
            loadingText.text = "Loading...";
            exitButton.SetActive(false);
            tipText.gameObject.SetActive(true);
            aO = SceneManager.LoadSceneAsync(scene);
            StartCoroutine(LoadLevelWithRealProgress());

        }
        else SceneManager.LoadScene(scene);
               
        
    }

    // MIRAR V

    /// <summary>
    /// Function for control the charge time of the level, show how firte particles
    /// are desapareciendo conforme arrive to 100%
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadLevelWithRealProgress()
    {
        //Controlar particulas
        ParticleSystem ps = fireLoad.GetComponentInChildren<ParticleSystem>();
        var mp = ps.main;

        aO.allowSceneActivation = false;
        while (!aO.isDone)
        {
           // fireLoad.GetComponentInChildren<ParticleSystem>().maxParticles -= 5;
            mp.maxParticles -= 5; //sustituye a lo de arriva
        
            yield return new WaitForSeconds(0.1f);
            if (mp.maxParticles <= 0)
            {
                loadingText.text = "Ready!";
                var emission = ps.emission;   //Stop la emission
                emission.enabled = false;
                yield return new WaitForSeconds(1f);
                aO.allowSceneActivation = true;
            }
        }

    }

    IEnumerator changeCanvas(GameObject origin, GameObject end, string message)
    {
        showMessage(message);
        yield return new WaitForSeconds(1f);
        resetError();
        origin.SetActive(false);
        end.SetActive(true);
        GameObject.Find("Canvas").GetComponent<prepareMenu>().enabled = true;
    }
    /* IEnumerator LoadLevelWithFakeProgress()
     {
         yield return new WaitForSeconds(1);

         while(progBar.value != 1)
         {
             progBar.value += fakeIncrement;
             yield return new WaitForSeconds(fakeTiming);
         }
         Debug.Log(progBar.value);

         if(progBar.value == 1) SceneManager.LoadScene(2);
         yield return null;
     }*/

    public void ExitGame()
    {
        Application.Quit();
    }
    /// <summary>
    /// Function to show text missions
    /// </summary>
    /// <param name="textoBr">String with the tag of the mission to make</param>
    public void WriteTextOnEnter(string textoBr)
    {
        
            switch (textoBr)
            {
                case "1.contenidor":
                    {
                        textoBriefing.gameObject.SetActive(true);
                        FadeImg.DOFade(1f, 0.5f);
                        RTimage.DOScale(1f, 0.5f);
                        textoBriefing.DOFade(1f, 0.5f);
                        RTtext.DOScale(1f, 0.5f);
                        
                        textoBriefing.text = string.Format("- Nivell 1: Incendi de contenidor.{0}- Data: 17 de maig.{0}- Hora: 17:23{0}- Informació: Ens informen de que crema un contenidor al mig del carrer. La teva missió  es apagar l'incendi i garantir la seguretat de l'entorn.", System.Environment.NewLine);
                        break;
                    }
                case "1.casita":
                    {
                        try
                        {
                            if (!Candado2.activeSelf)
                            {
                                textoBriefing.gameObject.SetActive(true);
                                FadeImg.DOFade(1f, 0.5f);
                                RTimage.DOScale(1f, 0.5f);
                                textoBriefing.DOFade(1f, 0.5f);
                                RTtext.DOScale(1f, 0.5f);

                                textoBriefing.text = string.Format("- Nivell 2: Incendi d'habitatge.{0}- Data: 12 de Juny.{0}- Hora: 14:23{0} - Informació: Ens informen de que crema un cotxe i un contenidor al mig del carrer, però les flames s'han propagat al interior d'un habitatge. Hi ha una persona atrapada al interior de la vivenda. La teva missió es apagar l'incendi, buscar si hi ha persones afectades, i garantir la seguretat de tot personal ", System.Environment.NewLine);
                            }
                        }
                        catch(Exception e)
                        {
                            Debug.Log(e);
                        }
                        
                    break;
                    }
                case "1.planta2":
                    try
                    {
                        if (!Candado3.activeSelf)
                        {
                            textoBriefing.gameObject.SetActive(true);
                            FadeImg.DOFade(1f, 0.5f);
                            RTimage.DOScale(1f, 0.5f);
                            textoBriefing.DOFade(1f, 0.5f);
                            RTtext.DOScale(1f, 0.5f);
                            textoBriefing.text = string.Format("- Nivell 3: Incendi d'habitatge.{0}- Data: {0}- Hora: {0}- Informació: La teva missió es apagar l'incendi, buscar si hi ha persones afectades, i garantir la seguretat de tor personal ", System.Environment.NewLine);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                break;
            }
        
        
    }
 

    public void CleanTextOnExit()
    {
       // StopCoroutine(FadeToBlack());
        textoBriefing.text = "";
        FadeImg.DOFade(0f, 0.5f);
        RTimage.DOScale(0f, 0.5f);
        RTtext.DOScale(0f, 0.5f);
        textoBriefing.DOFade(0f, 0.5f);
        textoBriefing.gameObject.SetActive(false);
        //StartCoroutine(FadeToClear());
    }

    /*IEnumerator FadeToClear()
    {
        // Lerp the colour of the image between itself and transparent.
       while (FadeImg.color != Color.clear)
        {
            FadeImg.color = Color.Lerp(FadeImg.color, Color.clear, unfadeSpeed * Time.deltaTime);
            yield return null;
        }

    }
    IEnumerator FadeToBlack()
    {
        // Lerp the colour of the image between itself and black.
        while (FadeImg.color != Color.black)
        {
            FadeImg.color = Color.Lerp(FadeImg.color, Color.black, fadeSpeed * Time.deltaTime);
            yield return null;
        }
    }*/
}

