﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PjEscena2 : MonoBehaviour {
	
	public GameObject fumfoc;
	public GameObject foccocina;
	public Image termometre;
	private float distanciafum;
	private float distanciafoc;

	// Use this for initialization
	void Start () {
		foccocina = GameObject.Find ("foccuina");
		fumfoc = GameObject.FindGameObjectWithTag("colisiofum");
		distanciafoc = 0;
		distanciafum = 0;

	}
	
	// Update is called once per frame
	void Update () {
		distanciafoc = Vector3.Distance (this.transform.position, foccocina.transform.position);
		distanciafum = Vector3.Distance (this.transform.position, fumfoc.transform.position);

		if(distanciafoc < 2 && termometre.GetComponent<Image> ().fillAmount < 1.0f){
			termometre.GetComponent<Image>().fillAmount += 0.05f;
		}

		if(distanciafum < 20 && distanciafum > 5){
			termometre.GetComponent<Image>().fillAmount = (float)0.02*100 /distanciafum;
		}

	}
	
	void OnTriggerStay(Collider other) {
			
			if (other.gameObject.tag == "colisiofum" && distanciafoc > 2) {
				//Debug.Log (termometre.GetComponent<Image> ().fillAmount);
				if (!InterfaceManager.agachado) {
					if (Mathf.Abs (termometre.GetComponent<Image> ().fillAmount - 0.75f) <= 0.1f)
						termometre.GetComponent<Image> ().fillAmount = 0.75f;
					else {
						if (termometre.GetComponent<Image> ().fillAmount < 0.75f)
						termometre.GetComponent<Image> ().fillAmount += 0.05f;
						else
							termometre.GetComponent<Image> ().fillAmount -= 0.05f;	
					}
					
				} else {
					if (Mathf.Abs (termometre.GetComponent<Image> ().fillAmount - 0.50f) <= 0.1f)
						termometre.GetComponent<Image> ().fillAmount = 0.50f;
					else {
						if (termometre.GetComponent<Image> ().fillAmount > 0.50f)
							termometre.GetComponent<Image> ().fillAmount -= 0.05f;
						else
							termometre.GetComponent<Image> ().fillAmount += 0.05f;	
					}
				 }
			}
		
		}
		
}
