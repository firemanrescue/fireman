﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgreBarDoor : MonoBehaviour {

    public Transform LoadingBar;

    [SerializeField] private float currentAmount;
    [SerializeField] private float speed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(currentAmount<=30)
        {
            currentAmount += speed * Time.deltaTime;
        }
        LoadingBar.GetComponent<Image>().fillAmount = currentAmount/30;
	}
}
