﻿using UnityEngine;

public class BarricadeObjectiveBehaviour : MonoBehaviour {

    public bool trigger;
    public GameObject mine;

	// Use this for initialization
	void Start () {
        trigger = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (trigger)
        {
            transform.position = Vector3.MoveTowards(transform.position, mine.transform.position, 2 * Time.deltaTime);
            
        }
        if (transform.position == mine.transform.position)
        {
            trigger = false;
        }
	}
}
