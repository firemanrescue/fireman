﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Semafor : MonoBehaviour
{

    public enum Estats { verd, vermell, groc }; //declarem l'enumerador de colors
    public float tempsVermell = 5,
                  tempsGroc = 15f,
                  tempsVerd = 20f,
                  temps = 0f;   //temps actual
    public Estats estat;

    public GameObject wallCoche;
    public GameObject[] wallPeatons;


    public float getTemps()
    {
        return temps;
    }
    public void setTemps(float t)
    {
        temps = t;
    }


    void Update()
    {
        temps += Time.deltaTime;
    }

    public void CanviarAVerd()
    {
        transform.GetChild(4).gameObject.GetComponent<SpriteRenderer>().color = Color.green; 
        transform.GetChild(5).gameObject.GetComponent<SpriteRenderer>().color = Color.green; 

        transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.white;

        DesactivarWalls();
        ActivarZebra();
    }
    public void CanviarAVermell()
    {
        transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.red;

        transform.GetChild(2).gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        transform.GetChild(3).gameObject.GetComponent<SpriteRenderer>().color = Color.white;

        ActivarWalls();
        DesactivarZebra();
    }

    public void CanviarAGroc()
    {
        transform.GetChild(2).gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
        transform.GetChild(3).gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;

        transform.GetChild(4).gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        transform.GetChild(5).gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void ActivarWalls()
    {
        wallCoche.SetActive(true);
    }

    public void DesactivarWalls()
    {
        wallCoche.SetActive(false);
            foreach (GameObject car in wallCoche.GetComponent<stopCars>().cars){
                car.GetComponent<NavMeshAgent>().speed = 3f;
            }
            wallCoche.GetComponent<stopCars>().cars.Clear();
        }

    public void ActivarZebra()
    {
        for (int i = 0; i < wallPeatons.Length; ++i)
        {
            wallPeatons[i].SetActive(true);
            foreach (GameObject npc in wallPeatons[i].GetComponent<stopNpc>().npc)
            {
                npc.GetComponent<NavMeshAgent>().speed = npc.GetComponent<turnNPC>().navSpeed;
                if (npc.GetComponent<turnNPC>().navSpeed == 3.5f)
                {
                    npc.GetComponent<Animator>().SetBool("run", true);
                }
                else{
                    npc.GetComponent<Animator>().SetBool("walk", true);
                }
            }
        }
    }
    public void DesactivarZebra()
    {
        for (int i = 0; i < wallPeatons.Length; ++i) {
            wallPeatons[i].SetActive(false);
            foreach (GameObject npc in wallPeatons[i].GetComponent<stopNpc>().npc){
                npc.GetComponent<NavMeshAgent>().speed = npc.GetComponent<turnNPC>().navSpeed;
                if (npc.GetComponent<turnNPC>().navSpeed == 3.5f){
                    npc.GetComponent<Animator>().SetBool("run", true);
                }
                else{
                    npc.GetComponent<Animator>().SetBool("walk", true);
                }
            }
            wallPeatons[i].GetComponent<stopNpc>().npc.Clear();
        }
    }
}