﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveNPC : MonoBehaviour {

    private List<GameObject> npcs;
	void Start () {
        npcs = new List<GameObject>();
	}
	
    /*public void Movement()
    {
        for (int i = 0; i < npc.Length; ++i)
        {
            //npc[i].GetComponent<NavMeshAgent>().speed = GetComponent<stopNpc>().currentNavSpeed; 
            npc[i].GetComponent<NavMeshAgent>().speed = 1.4f;
        }
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "npc") 
        {
            other.gameObject.GetComponent<Animator>().Play("iddle");
            other.gameObject.GetComponent<NavMeshAgent>().speed = 0;
            npcs.Add(other.gameObject);
        }
    }

    public void RestartNPCs()
    {
        for (int i = 0; i < npcs.Count; ++i)
        {     
            npcs[i].GetComponent<NavMeshAgent>().speed = 1.4f;
            npcs[i].gameObject.GetComponent<Animator>().Play("run");
        }
        npcs.Clear();
    }
}
