"nivell1": [{
	"objectius": [{
			"nom": "acordonar_zona",
			"accio": ["parlar_poicia"],
			"ordre": 1,
			"text_completat": "Has acordonat la zona.{0}",
			"text_incomplet": "No has acordonat la zona.{0}",
			"text_regular": "No has acordonat la zona en el millor moment.{0}",
			"explicacio": "Abans d'intervenir,{0}hem de garantir la seguretat de la zona.",
			"type": "Objectiu",
			"max_time": 100
		}, {
			"nom": "apagar_contenidor",
			"accio": ["focContenidor"],
			"ordre": 2,
			"focs": ["focContenidor"],
			"text_completat": "Has apagat el contenidor.{0}",
			"text_incomplet": "No has apagat el contenidor.{0}",
			"text_regular": "No has apagat el contenidor en el millor moment.",
			"explicacio": "Eliminar els riscos ha de ser{0}una de les nostres prioritats.",
			"type": "FireObjective",
			"max_time": 220,
			"objective": 3
		}, {
			"nom": "apagar_cotxe",
			"accio": ["focCotxe"],
			"ordre": 3,
			"focs": ["focCotxe"],
			"text_completat": "Has apagat el cotxe.{0}",
			"text_incomplet": "No has apagat el cotxe.{0}",
			"text_regular": "No has apagat el cotxe en el millor moment.{0}",
			"explicacio": "Eliminar els riscos ha de ser{0}una de les nostres prioritats.",
			"type": "FireObjective",
			"max_time": 220,
			"objective": 2
		}, {
			"nom": "apagar_brosa",
			"accio": ["focBrossa1", "focBrossa2"],
			"ordre": 4,
			"focs": ["focBrossa1", "focBrossa2"],
			"text_completat": "Has apagat la brossa.{0}",
			"text_incomplet": "No has apagat la brossa.{0}",
			"text_regular": "No has apagat la brossa en el millor moment",
			"explicacio": "Hem de comprovar que l'incendi{0}est� totalment apagat.",
			"type": "FireObjective",
			"max_time": 230,
			"objective": 6
		}, {
			"nom": "desconnectar_bateria",
			"accio": ["desconnectar_bateria"],
			"ordre": 5,
			"text_completat": "Has desconnectat la bateria.{0}",
			"text_incomplet": "No has desconnectat la bateria.{0}",
			"text_regular": "No has desconnectat la bateria en el millor moment.{0}",
			"explicacio": "Hem de comprovar que no hi ha risc{0}en les bateries del vehicle.",
			"type": "Objectiu",
			"max_time": 230
		}, {
			"nom": "utilitzar_equips_proteccio",
			"accio": ["no_rebre_mal"],
			"ordre": -1,
			"text_completat": "Has utilitzat correctament l'equip de proteccio.{0}",
			"text_regular": "No t'has equipat l'equip de protecci� en el millor moment.{0}",
			"text_incomplet": "No has utilitzat correctament l'equip de proteccio",
			"explicacio": "La nostre seguretat es lo primer,{0}ens hem d'equipar correctament abans d'intervenir.",
			"type": "Objectiu",
			"max_time": 45
		}, {
			"nom": "comunicacio_control",
			"accio": ["comunicar_inici", "comunicar_meitat", "comunicar_final"],
			"ordre": -1,
			"text_completat": "Has comunicat correctament a la sala de control.{0}",
			"text_regular": "No has comunicat suficient a la sala de control",
			"text_incomplet": "No has comunicat a la sala de control",
			"explicacio": "La informaci� �s vital, i hem de comunicar{0}a la sala de control l'estat de l'incident",
			"type": "MultiNode",
			"max_time": 240
		}
	}], {
	"temps": [{
		"minuts": 4,
		"segons": 0,
		"minim": 120
	}]
}, {
	"objectes": [{
		"nom": "Policia",
		"accions": ["Parlar", "Bon Dia", "Acordonar Zona", "Marxa", "Cancelar"],
		"valorac": [3, 4, 6, 5, 0],
		"dependencies": [menu00, menu01, menu01, menu01, ninguna],
		"valordep": [13, 14, 14, 14, 0],
		"nodes": [-1, -1, 4, -1, -1]
	}, {
		"nom": "Manguera",
		"accions": ["Agafar", "Deixar", "Cancelar"],
		"valorac": [1, 2, 0],
		"dependencies": ["no_manguera", "manguera", "ninguna"],
		"valordep": [1, 2, 0],
		"nodes": [-1, -1, -1]
	}, {
		"nom": "ERA",
		"accions": ["Agafar", "Cancelar"],
		"valorac": [1, 0],
		"nodes": [-1, -1]
	}, {
		"nom": "Destral",
		"accions": ["Agafar", "Deixar", "Cancelar"],
		"valorac": [1, 2, 0],
		"dependencies": ["no_destral", "destral", "ninguna"],
		"valordep": [3, 4, 0],
		"nodes": [-1, -1, -1]
	}, {
		"nom": "Contenidor",
		"accions": ["Volcar", "Cancelar"],
		"valorac": [7, 0],
		"dependencies": ["multidep": ["bomber_acomp", "apagat"], "ninguna"],
		"valordep": ["multidep": [18, 17], 0],
		"nodes": [-1, -1]
	}, {
		"nom": "Capo",
		"accions": ["Utilitzar Destral", "Cancelar"],
		"valorac": [8, 0],
		"dependencies": ["destral", "ninguna"],
		"valordep": [4, 0],
		"node": [-1, -1]
	}, {
		"nom": "Bateria",
		"accions": ["Desconectar", "Cancelar"],
		"valorac": [10, 0],
		"dependencies": ["alicates", "ninguna"],
		"valordep": [12, 0],
		"nodes": [7, -1]
	}, {
		"nom": "Botiquin",
		"accions": ["Agafar", "Deixar", "Cancelar"],
		"valorac": [1, 2, 0],
		"dependencies": ["no_botiquin", "botiquin", "ninguna"],
		"valordep": [5, 6, 0],
		"nodes": [-1, -1, -1]
	}, {
		"nom": "Bomber",
		"accions": ["Parlar", "Acompanyam", "Cancelar"],
		"valorac": [3, 11, 0],
		"dependencies": ["menu00", "menu01", "ninguna"],
		"valordep": [13, 14, 0],
		"nodes": [-1, -1, -1]
	}, {
		"nom": "Radio",
		"accions": ["Comunicar a control", "Comunicar situaci� inicial", "Comunicar extinci� dels focs", "Comunicar fi de l'emerg�ncia", "Comunicar bombers", "Cancelar"],
		"valorac": [14, 15, 16, 17, 28, 0],
		"dependencies": ["menu00", "menu01", "multidep": ["menu01", "no_completat"], "menu01", "menu0", "ninguna"],
		"valordep": [13, "multidep": [14, 19], "multidep": [14, 19], 14, 13, 0],
		"nodes": [-1, 1, 5, 8, -1]
	}]
}]